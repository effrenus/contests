// https://www.codingame.com/ide/puzzle/power-of-thor-episode-2

// Currently pass 90% tests
// TODO: 100%

var inputs = readline().split(' ');
let tc = parseInt(inputs[0]);
let tr = parseInt(inputs[1]);

const dirs = [
    [-1,-1], [-1, 0], [-1,1],
    [0,-1], [0,1],
    [1,-1], [1,0], [1,1]]

while (true) {
    const [H, N] = readline().split(' ').map(Number)
    printErr(H, N)

    const enemies = []
    for (let i = 0; i < N; i++)
        enemies.push(readline().split` `.reverse().map(Number))

    const [br, bc] = getBoundRect(enemies)
    // printErr(tr, tc)

    if (findPotetial([0, 0], enemies) == enemies.length) {
        print('STRIKE')
        continue
    }
    // printErr('possib', findPotetial([0, 0], enemies), findPotetial([0, -1], enemies), enemies.length)

    printErr(isPossibleToEscape(enemies))

    if (inDanger([0, 0], enemies)) {
        const vars = findAlternatives(enemies)
        // printErr(vars.length, 'ttt')

        if (!vars.length) { printErr('WWW', vars); print('STRIKE'); continue }

        vars.sort((a, b) => findPotetial(b, enemies) - findPotetial(a, enemies))

        const [dr, dc] = vars[0]
        let mv = ''
        if (dr != 0) mv += dr < 0 ? 'N' : 'S'
        if (dc != 0) mv += dc < 0 ? 'W' : 'E'
        tr += dr
        tc += dc
        print(mv)
    } else {
        let mv = ''
        let dr = 0, dc = 0
        if (br != tr) mv += br < tr ? (dr = -1, 'N') : (dr = 1, 'S')
        if (bc != tc) mv += bc < tc ? (dc = -1, 'W') : (dc = 1, 'E')

        // printErr('brbrbr', inBounds([dr, dc]), isSafe([tr+dr, tc+dc], enemies))

        if (!mv || !inBounds([dr, dc]) || !isSafe([tr+dr, tc+dc], enemies)) {
            const vars = findAlternatives(enemies)
            // printErr(vars.length, 'zz')
            if (!vars.length) { printErr('END?', tr, tc, enemies); print('STRIKE'); continue }

            vars.sort((a, b) => findPotetial(b, enemies) - findPotetial(a, enemies))
            // printErr(vars)
            // printErr('@', vars.map(a => findPotetial(a, enemies)))

            const [dr, dc] = vars[0]
            let mv = ''
            if (dr != 0) mv += dr < 0 ? 'N' : 'S'
            if (dc != 0) mv += dc < 0 ? 'W' : 'E'
            tc += dc
            tr += dr
            print(mv)
        } else {
            // printErr('qqq')
            tc += dc
            tr += dr
            print(mv)
        }
    }
}

function isPossibleToEscape(enemies) {
    const q = [[[tr, tc], 0]]
    const vis = {}
    while (q.length) {
        const [[r, c], l] = q.shift()

        if (vis[`${r},${c}`]) continue
        vis[`${r},${c}`] = true

        if (l > 5) {
            printErr(vis)
            return true
        }

        for (const [dr, dc] of dirs) {
            const [nr, nc] = [r+dr, c+dc]
            if (!inBounds2([nr, nc]) || !isSafe([nr, nc], enemies)) continue
            q.unshift([[nr, nc], l+1])
        }
    }

    return false
}

function findPotetial([dr, dc], enemies) {
    const [nr, nc] = [tr+dr, tc+dc]
    let cnt = 0
    for (let i = nr-4; i <= nr+4; i++) {
        if (i < 0 || i >= 18) continue
        for (let j = nc-4; j <= nc+4; j++) {
            if (j < 0 || j >= 40) continue
            if (enemies.some(([r,c]) => r == i && c == j )) cnt++
        }
    }
    return cnt
}

function findAlternatives(enemies) {
    return dirs
        .filter(inBounds)
        .map(([dr, dc]) => [[dr, dc], [tr+dr, tc+dc]])
        .filter(p => isSafe(p[1], enemies))
        .map(v => v[0])
}

function inBounds([dr, dc]) {
    return tr+dr >= 0 && tr+dr < 18 && tc+dc >= 0 && tc+dc < 40
}

function inBounds2([r, c]) {
    return r >= 0 && r < 18 && c >= 0 && c < 40
}

function isSafe([nr, nc], enemies) {
    return enemies
        .every(([r, c]) => Math.abs(nr-r) + Math.abs(nc-c) > 2)
}

function inDanger([dr, dc], enemies) {
    return enemies
        .map(([r, c]) => [r+dr, c+dc])
        .some(([r,c]) => Math.abs(tr+dr-r) + Math.abs(tc+dc-c) < 2)
}

function getBoundRect(ps) {
    ps.sort((a,b) => a[0] - b[0])
    let [lr, rr] = [ps[0][0], ps[ps.length-1][0]]

    ps.sort((a,b) => a[1] - b[1])
    let [lc, rc] = [ps[0][1], ps[ps.length-1][1]]

    if (Math.abs(rc-lc) > 1.5*Math.abs(rr-lr)) {
        rr = Math.min(rr + 3 * Math.abs(lc-rc), 17)
    }

    return [(rr+lr)/2, (lc+rc)/2]
    // return [
    //     ps.reduce((s, v) => s+v[0], 0)/ps.length,
    //     ps.reduce((s, v) => s+v[1], 0)/ps.length,
    // ]
}

