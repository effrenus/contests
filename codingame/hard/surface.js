// https://www.codingame.com/ide/puzzle/surface

// BFS

const w = +readline()
const h = +readline()

const map = []
for (let i = 0; i < h; ++i) map.push(readline().split``)

let N = +readline()

const qry = []
while (N--) qry.push(readline().split` `.reverse().map(Number))

const bfs = ([r,c]) => {
    if (map[r][c] == '#') return 0
    if (map[r][c] != 'O') return map[r][c][0]

    const ans = [0]
    const q = [[r,c]]

    while (q.length) {
        const [r,c] = q.shift()

        if (map[r][c] != 'O') continue
        map[r][c] = ans
        ans[0] += 1

        for (const [dr,dc] of [[1,0],[-1,0],[0,1],[0,-1]]) {
            const [nr, nc] = [r+dr, c+dc]
            if (nr < 0 || nr >= h || nc < 0 || nc >= w || map[nr][nc] != 'O') continue
            q.push([nr, nc])
        }
    }
    return ans[0]
}

for (const q of qry) print(bfs(q))
