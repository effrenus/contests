// https://www.codingame.com/ide/puzzle/the-labyrinth

// BFS

const [rows, cols, alarmTime] = readline().split` `.map(Number)

let map = Array.from({length: rows}).map(_ => Array(cols).fill('?'))

function bfs(pos, ch='C') {
    const q = [[...pos, 0]]
    const visited = {}
    while (q.length) {
        const [r, c, cnt] = q.shift()
        if (map[r][c] == ch) return [[r,c], cnt+1]
        if (visited[`${r},${c}`]) continue
        visited[`${r},${c}`] = true

        for (const [dr,dc] of [[-1,0],[1,0],[0,1],[0,-1]]) {
            const [nr, nc] = [r+dr, c+dc]
            if (nr < 0 || nr >= rows || nc < 0 || nc >= cols || (ch != '?' && map[nr][nc] == '?') || map[nr][nc] == '#') continue
            q.push([nr, nc, cnt+1])
        }
    }
    return [[],-1]
}

const getDir = ([dr,dc]) => {
    if (dr == 0) return dc == 1 ? 'RIGHT' : 'LEFT'
    if (dc == 0) return dr == 1 ? 'DOWN' : 'UP'
}

function bfs2(pos, ch='C', t) {
    const q = [[...pos, []]]
    const visited = {}
    while (q.length) {
        const [r, c, path] = q.shift()
        if (map[r][c] == ch || (t && r == t[0] && c == t[1])) return path.join`\n`
        if (visited[`${r},${c}`]) continue
        visited[`${r},${c}`] = true

        for (const [dr,dc] of [[-1,0],[1,0],[0,1],[0,-1]]) {
            const [nr, nc] = [r+dr, c+dc]
            if (t && nr == t[0] && nc == t[1]) return path
            if (nr < 0 || nr >= rows || nc < 0 || nc >= cols || map[nr][nc] == '?' || map[nr][nc] == '#') continue
            q.push([nr, nc, path.slice().concat(getDir([dr,dc]))])
        }
    }
    return -1
}

function moveNearestUnknown([r, c]) {
    const [[rr, cc]] = bfs([r,c], '?')
    const d = Math.abs(r-rr)+Math.abs(c-cc)
    for (const [dr,dc] of [[-1,0],[1,0],[0,1],[0,-1]]) {
        const [nr, nc] = [r+dr, c+dc]
        if (nr < 0 || nr >= rows || nc < 0 || nc >= cols || map[nr][nc] == '?' || map[nr][nc] == '#') continue
        if (Math.abs(nr-rr)+Math.abs(nc-cc) > d) continue
        print(bfs2([r,c],'x',[rr,cc])[0])
        return
    }
    print(bfs2([r,c],'x',[rr,cc])[0])
}

while (true) {
    const pos = readline().split` `.map(Number)

    for (let i = 0; i < rows; ++i) {
        const rr = readline().split``
        for (let j = 0; j < cols; ++j) {
            if (map[i][j] == '?') map[i][j] = rr[j]
        }
    }

    let roomPos
    for (let r = 0; r < rows; ++r) for (let c = 0; c < cols; ++c)
        if (map[r][c] == 'C') roomPos = [r, c]
    
    if (roomPos) {
        const [[rr, cc], steps] = bfs(pos)
        printErr(steps)

        if (steps == -1) moveNearestUnknown(pos)
        else {
            if (steps < alarmTime) {
                print(bfs2(pos))
                print(bfs2([rr,cc], 'T'))
            }
        }
    } else {
        moveNearestUnknown(pos)
    }
}

