// https://www.codingame.com/ide/puzzle/the-holy-grail

// BFS. It's possible to optimize: DSU.

const [cols, rows] = readline().split` `.map(Number)
const mat = Array(rows).fill().map(_ => Array(cols).fill('#'))
mat[0][0] = '.'
mat[rows-1][cols-1] = '.'

let cell, tilesNeeded = 0
while (cell = readline()) {
    const [c, r] = cell.split` `.map(Number)
    mat[r][c] = '.'
    tilesNeeded++

    if (reachable()) break
    // view()
}

print(tilesNeeded)

function view() {
    print(mat.map(r => r.join``).join`\n`)
    print('~'.repeat(20))
}

function reachable() {
    const q = [[0,0]]
    const vis = new Map
    while (q.length) {
        const [r, c] = q.shift()
        if (r == rows-1 && c == cols-1) return true
        if (vis.has(''+[r,c])) continue
        vis.set(''+[r,c], true)

        for (const [dr,dc] of [[-1,0],[0,1],[1,0],[0,-1]]) {
            const [nr, nc] = [r+dr,c+dc]
            if (nr < 0 || nr >= rows || nc < 0 || nc >= cols || mat[nr][nc] == '#') continue
            q.push([nr,nc])
        }
    }
    return false
}

