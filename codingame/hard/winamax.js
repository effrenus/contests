// https://www.codingame.com/ide/puzzle/winamax-sponsored-contest

// Recursion, DFS

const [w, h] = readline().split` `.map(Number)

const grid = []
for (let i = 0; i < h; ++i) grid.push(readline().split``)

const mirror = grid.map(r => r.slice())

const balls = []
for (let r = 0; r < h; ++r) for (let c = 0; c < w; ++c)
    if (!isNaN(grid[r][c])) balls.push([[r, c], +grid[r][c]])

let isSuccess = false

const view = _ => (printErr(grid.map(r=>r.join``).join`\n`), printErr('~~~~~~~~~~'))

const dfs = ([r, c], dist, idx) => {
    if (isSuccess) return true

    if (grid[r][c] == 'H') {
        if (dist < 0) return

        if (idx == balls.length-1) {
            isSuccess = true
            print(mirror.map(r=>r.join``.replace(/X|H/g,'.')).join`\n`)
            return
        }

        grid[r][c] = -2
        dfs(...balls[idx+1], idx+1)
        grid[r][c] = 'H'
        return
    }

    if (dist <= 0) return

    for (const [dr, dc] of [[-1,0],[1,0],[0,1],[0,-1]]) {
        const [nr, nc] = [r+dr*dist, c+dc*dist]

        if (nr < 0 || nr >= grid.length || nc < 0 || nc >= grid[0].length || /\d+/.test(grid[nr][nc]) || grid[nr][nc] == 'X' || grid[nr][nc] < 0) continue
        if (!isValid([r, c], [nr, nc], [dr, dc])) continue

        mark([r, c], [nr, nc], [dr, dc])
        // view()
        dfs([nr, nc], dist-1, idx)
        unmark([r, c], [nr, nc], [dr, dc])
    }
}

dfs(...balls[0], 0)

function isValid([r, c], [nr, nc], [dr, dc]) {
    let i = r
    let j = c
    while (i != nr || j != nc) {
        if (grid[i][j] < 0)
            return false
        i += dr
        j += dc
    }
    return true
}

function mark([r, c], [nr, nc], [dr, dc]) {
    let ch = dr == 0 ? (dc > 0 ? '>' : '<') : (dr > 0 ? 'v' : '^')

    let i = r
    let j = c
    while (i != nr || j != nc) {
        grid[i][j] = /\d+/.test(grid[i][j]) ? -4 : (grid[i][j] == 'H' ? -2 : (grid[i][j] == 'X' ? -3 : -1))
        mirror[i][j] = ch
        i += dr
        j += dc
    }
}
function unmark([r, c], [nr, nc], [dr, dc]) {
    let i = r
    let j = c
    while (i != nr || j != nc) {
        mirror[i][j] = grid[i][j] == -2 ? 'H' : (grid[i][j] == -3 ? 'X' : '.')
        grid[i][j] = grid[i][j] == -4 ? '3' : (grid[i][j] == -2 ? 'H' : (grid[i][j] == -3 ? 'X' : '.'))
        i += dr
        j += dc
    }
}
