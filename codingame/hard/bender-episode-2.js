// https://www.codingame.com/ide/puzzle/bender-episode-2

// BFS, Dijkstra

let N = +readline()

const scores = new Map
const exitRooms = new Map
const rooms = []

while (N--) {
    const [id, score, to1, to2] = readline().split` `.map(v => v != 'E' ? +v : v)

    if (to1 == 'E' || to2 == 'E') exitRooms.set(id, true)
    rooms.push([id, score, to1, to2])
    scores.set(id, -score)
}

const graph = new Map
for (const [id, _, to1, to2] of rooms) {
    if (!graph.has(id)) graph.set(id, [])
    if (to1 != 'E') graph.get(id).push([to1, scores.get(to1)])
    if (to2 != 'E') graph.get(id).push([to2, scores.get(to2)])
}

function bfs() {
    const dist = Array(rooms.length).fill(Infinity)
    dist[0] = scores.get(0)
    const queue = [0]

    while (queue.length) {
        const id = queue.shift()

        for (const [nid, d] of (graph.get(id)||[])) {
            if (dist[id] + d < dist[nid]) {
                dist[nid] = dist[id] + d
                queue.push(nid)
            }    
        }
    }
    return -Math.min(
        ...[...exitRooms.keys()].map(k => dist[+k])
    )
}

print(bfs())

