// https://www.codingame.com/ide/puzzle/roller-coaster

// Simple

var inputs = readline().split(' ');
const L = parseInt(inputs[0]);
let C = parseInt(inputs[1]);
const N = parseInt(inputs[2]);
gr = []
for (let i = 0; i < N; i++) {
    gr.push(+readline())
}

s = 0
let i = 0
let mem = {}
while (C--) {
    if (mem[i]) {
        s += mem[i].sum
        i = mem[i].next
        continue
    }

    let capacity = gr[i]
    const start = i

    i = (i+1)%gr.length
    while (capacity+gr[i] <= L && i != start) {
        capacity += gr[i]
        i = (i+1)%gr.length
    }

    mem[start] = {
        sum: capacity,
        next: i
    }
    s += capacity
}

print(s)
