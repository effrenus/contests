// https://www.codingame.com/ide/puzzle/skynet-revolution-episode-2

// BFS

var inputs = readline().split(' ');
const N = parseInt(inputs[0]); // the total number of nodes in the level, including the gateways
const L = parseInt(inputs[1]); // the number of links
const E = parseInt(inputs[2]); // the number of exit gateways

const g = Array(N).fill(0).map(_=>[])
for (let i = 0; i < L; i++) {
    var inputs = readline().split(' ');
    const N1 = parseInt(inputs[0]); // N1 and N2 defines a link between these nodes
    const N2 = parseInt(inputs[1]);
    g[N1].push(N2)
    g[N2].push(N1)
}

const gw = new Map
for (let i = 0; i < E; i++) {
    gw.set(parseInt(readline()))
}

const ranks = {}
for (const n of gw.keys()) for (const nn of g[n]) ranks[nn] = (ranks[nn]||0)+1

const bfs = (n) => {
    const q = [[n, 0, 0]]
    const vis = {}
    while (q.length) {
        const [n, d, gg] = q.shift()

        if (vis[n]) continue
        vis[n] = true

        const hasGW = g[n].some(v=>gw.has(v))

        for (const nn of g[n]) {
            if (gw.has(nn)) {
                if (ranks[n]-d-1 == 0) {
                    ranks[n]--
                    g[n] = g[n].filter(v=>v!=nn)
                    g[nn] = g[nn].filter(v=>v!=n)
                    return [n, nn]
                }
                if (gg < ranks[n]) {
                    ranks[n]--
                    g[n] = g[n].filter(v=>v!=nn)
                    g[nn] = g[nn].filter(v=>v!=n)
                    return [n, nn]
                }
                continue
            }
            if (vis[nn]) continue
            q.push([nn, d+1, hasGW?gg:gg+1])
        }
    }
    const r = [...Object.entries(ranks)].sort((a,b)=>b[1]-a[1])[0][0]
    for (const n of g[r]) {
        if (gw.has(n)) {
            ranks[r]--
            g[n] = g[n].filter(v=>v!=r)
            g[r] = g[r].filter(v=>v!=n)
            return [r, n]
        }
    }
}

while (true) {
    const SI = parseInt(readline());
    print(bfs(SI).join` `)
}
