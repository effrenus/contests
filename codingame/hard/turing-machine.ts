// https://www.codingame.com/ide/puzzle/turing-machine

type WriteSymbol = number
type MoveDir = | 'R' | 'L'
type NextState = string
type Action = [WriteSymbol, MoveDir, NextState]

const END_STATE = 'HALT'

class TuringMachine {
    private tape: number[]
    private performedActionsCount = 0

    constructor(
        private state: string,
        private actions: Map<string, Action[]>,
        private tapePos: number, tapeLength: number
    ) {
        this.tape = Array(tapeLength).fill(0)
    }

    run() {
        while (this.state != END_STATE && this.positionInTapeBounds()) {
            const actionIdx = this.readTape()

            const [writeSym, direction, nextState] = this.actions.get(this.state)[actionIdx]

            this.writeTape(writeSym)
            this.moveTape(direction)
            this.state = nextState
            this.performedActionsCount += 1
        }
    }

    getStats(): [number, number, string] {
        return [this.performedActionsCount, this.tapePos, this.tape.join('')]
    }

    private readTape(): number {
        return this.tape[this.tapePos]
    }
    private writeTape(val: number) {
        this.tape[this.tapePos] = val
    }
    private moveTape(dir: MoveDir) {
        this.tapePos += dir == 'L' ? -1 : 1
    }
    private positionInTapeBounds(): boolean {
        return this.tapePos >= 0 && this.tapePos < this.tape.length
    }
}

const [, tapeLen, tapePos] = readline().split(` `).map(Number)

const machine = new TuringMachine(readline(), readActionsPerState(), tapePos, tapeLen)
machine.run()

console.log(
    machine.getStats().join('\n')
)



function readActionsPerState() {
    let statesNumber = Number(readline())
    const stateActions: Map<string, Action[]> = new Map
    while (statesNumber--) {
        const [state, actions] = readline().split(':')
        for (const act of actions.split(';')) {
            const [W, D, S] = act.split(' ')
            if (!stateActions.has(state)) stateActions.set(state, [])
            stateActions.get(state).push([Number(W), D as MoveDir, S])
        }
    }
    return stateActions
}

