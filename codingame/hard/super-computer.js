// https://www.codingame.com/ide/puzzle/super-computer

const N = parseInt(readline());

let works = []
for (let i = 0; i < N; i++) {
    var inputs = readline().split(' ');
    const J = parseInt(inputs[0]);
    const D = parseInt(inputs[1]);
    works.push([J, D])
}

works = works.map(([a,b])=>[a, a+b-1])
works.sort((a, b) => a[0] === b[0] ? a[1] - b[1] : a[0] - b[0])

let cnt = 0
for (let i = 0; i < works.length; i++) {
    let minRight = works[i][1]
    let j = i + 1
    while (j < works.length && works[j][0] <= minRight) {
        minRight = Math.min(minRight, works[j][1])
        j++
    }
    i = j - 1
    cnt += 1
}

print(cnt)
