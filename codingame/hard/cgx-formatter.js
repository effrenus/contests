// https://www.codingame.com/ide/puzzle/cgx-formatter

// RD

let N = +readline()

lines = []
while (N--) lines.push(readline())

function primitive({ txt, pos, indent }) {
    const r = txt.slice(pos).match(/(\d+|true|false|null|'.*?')/)
    
    if (!r) throw Error('PRIMITIVE')

    pos += r[0].length
    const val = ' '.repeat(indent) + r[0] + (txt[pos] == ';' ? ';' : '') + '\n'
    if (txt[pos] == ';') pos += 1
    
    return [pos, val]
}


function key_value({ txt, pos, indent }) {
    const r = txt.slice(pos).match(/'.+?'\s*=/)
    
    pos += r[0].length
    let val = ' '.repeat(indent) + r[0] + (txt[pos] == '(' ? '\n' : '')
    let newPos, s
    switch (txt[pos]) {
        case '(':
            [newPos, s] = block({ txt, pos, indent })
            break
        default:
            [newPos, s] = primitive({ txt, pos, indent: 0 })
    }
    return [newPos, val+s]
}


function block({ txt, pos, indent }) {
    let opened = 1
    let j = pos+1
    while (opened > 0) {
        if (txt[j] == '(') opened++
        if (txt[j] == ')') opened--
        j++
    }

    let val = [
        ' '.repeat(indent) + '(' + '\n',
        element({ txt: txt.slice(pos+1, j-1), pos: 0, indent: indent + 4 }),
        ' '.repeat(indent) + ')',
    ].join``

    if (txt[j] == ';') j++
    val += (txt[j-1]==';'?';':'') + '\n'

    return [j, val]
}


function element({ txt, formatted = '', pos = 0, indent }) {
    if (pos == txt.length) return formatted

    const ch = txt[pos]
    let newPos, s
    switch (ch) {
        case '(':
            [newPos, s] = block({ txt, formatted, pos, indent })
            break
        case '\'':
            [newPos, s] = !/^'.+?'\s*=/.test(txt.slice(pos)) 
                ? primitive({ txt, formatted, pos, indent }) 
                : key_value({ txt, formatted, pos, indent })
            break
        default:
            [newPos, s] = primitive({ txt, formatted, pos, indent })
            break
    }
    return element({ formatted: formatted + s, txt, pos: newPos, indent })
}


function format(txt) {
    txt = txt
        .replace(/^\s*|\n|\s*$/g, '')
    
    let insideStr = false
    nt = ''
    for (const ch of txt) {
        if (ch == '\'') insideStr = !insideStr
        if ((ch == ' ' || ch == '\t') && !insideStr) continue
        nt += ch
    }
    return element({ txt: nt, pos: 0, indent: 0 })
}

print(
    format(lines.filter(Boolean).join``)
)

