// https://www.codingame.com/ide/puzzle/tan-network

// Dijkstra's algorithm

const startId = readline()
const endId = readline()

let stopCount = +readline()

const stops = new Map
const nums = new Map
for (let i = 0; i < stopCount; ++i) {
    const [id,fullname,,lat,lng,,,type] = readline().split`,`
    stops.set(id, {num: i, fullname: fullname.slice(1,-1), lat: parseFloat(lat)*Math.PI/180, lng: parseFloat(lng)*Math.PI/180, type})
    nums.set(i, id)
}

const routeCount = +readline()
const routes = new Map
for (let i = 0; i < routeCount; ++i) {
    const [from, to] = readline().split` `
    if (!routes.has(from)) routes.set(from, [])
    routes.get(from).push(to)
}

(function() {
    if (startId == endId) {
        print(stops.get(startId).fullname)
        return
    }

    const vis = Array(stopCount).fill(false)
    const p = Array(stopCount).fill(-1)

    const dist = Array(stopCount).fill(Infinity)
    dist[stops.get(startId).num] = 0

    for (let i = 0; i < stopCount; ++i) {
        let v = -1
        for (let j = 0; j < dist.length; ++j) {
            if (!vis[j] && (v == -1 || dist[j] < dist[v])) v = j
        }

        if (dist[v] == Infinity) {
            break
        }

        vis[v] = true;
        for (const vv of (routes.get(nums.get(v))||[])) {
            const num = stops.get(vv).num
            const len = getDist(v, num)
            if (dist[v] + len < dist[num]) {
                dist[num] = dist[v] + len;
                p[num] = v;
            }
        }
    }

    if (p[stops.get(endId).num] == -1) {
        print('IMPOSSIBLE')
        return
    }

    const path = []
    for (let v = stops.get(endId).num; v != stops.get(startId).num; v = p[v])
        path.push(v)

    path.push(stops.get(startId).num)

    print(path.reverse().map(n => stops.get(nums.get(n)).fullname).join`\n`)
}())

function getDist(i, j) {
    const p1 = stops.get(nums.get(i))
    const p2 = stops.get(nums.get(j))

    const x = (p2.lng - p1.lng) * Math.cos((p1.lat+p2.lat)/2)
    const y = p2.lat - p1.lat
    return Math.sqrt(x**2 + y**2) * 6371
}
