// https://www.codingame.com/ide/puzzle/the-river-ii-

// Recursion

const meetNum = readline()

function find({ numSum = 0, num = 0, base = 1, remainNums = meetNum.length } = {}) {
    if (remainNums == 0) {
        return numSum + num == meetNum
    }

    for (let i = 0; i < 10; ++i) {
        if (
            find({ 
                numSum: numSum + i,
                num: num + base*i,
                base: base*10,
                remainNums: remainNums-1 
            })
        ) return true
    }
    return false
}

print(find() ? 'YES' : 'NO')

