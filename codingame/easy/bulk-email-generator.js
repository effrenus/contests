// https://www.codingame.com/ide/puzzle/bulk-email-generator

let i = +readline()

let msg = ''
while (i--) msg += readline()+'\n'

print (
    msg
        .replace(/\n/g, '・')
        .replace(/\((.+?)\)/gi, (_,s) => (a=s.split`|`, a[++i%a.length]))
        .replace(/・/g, '\n')
)

