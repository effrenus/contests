// https://www.codingame.com/ide/puzzle/brackets-extreme-edition
f=(s,rs=s.replace(/\[\]|\(\)|\{\}/g,''))=>s==rs?!s:f(rs)
print(f(readline().replace(/[^{}()\[\]]/g,'')))
