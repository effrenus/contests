// https://www.codingame.com/ide/puzzle/jack-silver-the-casino

let rounds = +readline()
let cash = +readline()

while (rounds--) {
    const [ball, call, num] = readline().split(' ') as [string, string, string | undefined]
    const bet = Math.ceil(cash/4)
    
    if (!({
        'PLAIN': () => num == ball,
        'ODD': () => +ball%2,
        'EVEN': () => !(+ball%2) && !!+ball,
    })[call]()) cash -= bet
    else cash += call == 'PLAIN' ? 35*bet : bet
}

console.log(cash)

