// https://www.codingame.com/ide/puzzle/rugby-score

// Recursion.

const targetScore = +readline()
const scores = [5, 2, 3]

const scoreCombinations = []
const curCombination = [0, 0, 0]

;(function find(remain = targetScore, pos = 0) {
    if (remain == 0) {
        scoreCombinations.push(curCombination.join` `)
        return
    }

    if (pos == 3) return

    for (let i = 0; i*scores[pos] <= targetScore; ++i) {
        if (pos == 1 && i > curCombination[0]) break
        curCombination[pos] = i
        find(remain - i*scores[pos], pos+1)
    }
    curCombination[pos] = 0
})()

print(scoreCombinations.join`\n`)

