// https://www.codingame.com/ide/puzzle/graffiti-on-the-fence

// Sorting.

const L = +readline()
const N = +readline()

const sections = []
for (let i = 0; i < N; i++) {
    sections.push(readline().split` `.map(Number))
}
sections.sort((a, b) => a[0] == b[0] ? b[1] - a[1] : a[0]-b[0])

let l = 0
const unpainted = []
for (let i = 0; i < N; ++i) {
    let [st, en] = sections[i]
    while (i < N-1 && sections[i+1][0] < en) {
        en = Math.max(en, sections[i+1][1])
        i += 1
    }
    if (l != st) {
        unpainted.push(`${l} ${st}`)
    }
    l = en
}

if (l != L) unpainted.push(`${l} ${L}`)

print(unpainted.length ? unpainted.join`\n` : 'All painted')

