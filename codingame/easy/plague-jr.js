// https://www.codingame.com/ide/puzzle/plague-jr

// BFS

const N = +readline()

const net = new Map
for (let i = 0; i < N; i++) {
    const [from , to] = readline().split` `.map(Number)
    if (!net.has(from)) net.set(from, [])
    if (!net.has(to)) net.set(to, [])
    net.get(from).push(to)
    net.get(to).push(from)
}

const spreadTime = (start) => {
    const q = [[start, 0]]
    const vis = new Map
    let daysToSpread = 0
    while (q.length) {
        const [id, day] = q.shift()
        if (vis.has(id)) continue
        vis.set(id, true)
        daysToSpread = Math.max(daysToSpread, day)
        for (const nId of (net.get(id)||[])) {
            q.push([nId, day+1])
        }
    }
    return daysToSpread
}

const start = [...net.entries()].sort((a,b)=>b[1].length-a[1].length)[0][0]

print(
    Math.min(...[...net.keys()].map(spreadTime))
)

