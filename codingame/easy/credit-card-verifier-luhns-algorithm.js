// https://www.codingame.com/ide/puzzle/credit-card-verifier-luhns-algorithm

readline()

let num = null
while ((num = (readline()||'').replace(/ /g, '').split``) && num.length) {
    const sum = num.reduceRight(
        (sum, n, i) => (sum[(num.length-i)%2] += (num.length-i)%2 ? +n : 2*n - (2*n>9 && 9), sum),
        [0, 0]
    ).reduce((a, b) => a+b)

    print(sum % 10 ? 'NO' : 'YES')
}

