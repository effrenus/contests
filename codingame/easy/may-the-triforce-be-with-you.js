// https://www.codingame.com/ide/puzzle/may-the-triforce-be-with-you

const N = +readline()

for (let i = 0, k = N*2-1; i < N; ++i, --k) {
    print((!i?'.':'') + ' '.repeat(k-!i) + '*'.repeat(i*2+1))
}

for (let i = 0, k = N*2-1, j = 1; i < N; ++i, k-=2, j+=2) {
    print(' '.repeat((k-1)/2) + '*'.repeat(j) + ' '.repeat(k) + '*'.repeat(j))
}

