// https://www.codingame.com/ide/puzzle/text-formatting

print(
    readline()
        .toLowerCase()
        .replace(/\s+/g, ' ')
        .replace(/\s*([^a-z0-9 ])(\s*[^a-z0-9 ])*\s*/g, '$1 ')
        .replace(/^[a-z]|\. [a-z]/g, (m) => m.toUpperCase())
        .trim()
)

