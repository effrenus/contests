// https://www.codingame.com/ide/puzzle/addem-up

readline()

const cardsVals = readline().split(' ').map(Number)

let totalCost = 0
while (cardsVals.length > 1) {
    cardsVals.sort((a, b) => a - b) // O(NlogN). Can use MinHeap O(logN) or linear search O(N)

    cardsVals.push(cardsVals.shift() + cardsVals.shift())
    totalCost += cardsVals[cardsVals.length-1]
}

console.log(totalCost)

