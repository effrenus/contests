// https://www.codingame.com/ide/puzzle/ghost-legs

const inputs: string[] = readline().split(' ');
const W: number = parseInt(inputs[0]);
const H: number = parseInt(inputs[1]);

const T: string[] = readline().split('  ')

const pos = [...T]
for (let r = 2; r < H; ++r) {
    const line = readline()
    for (let c = 0; c < W/3; ++c) {
        if (line[c*3+1] == '-') {
            [pos[c], pos[c+1]] = [pos[c+1], pos[c]]
        }
    }
}

const B: string[] = readline().split('  ')

console.log(
    T.map(t => `${t}${B[pos.indexOf(t)]}`).join('\n')
)

