// https://www.codingame.com/ide/puzzle/rectangular-block-spinner

const size = +readline()
let angle = +readline()
angle -= Math.floor(angle/360)*360

const mat = []
for (let i = 0; i < size; i++) mat.push(readline().split` `)

for (; angle >= 90; angle -= 90) rotateCCW()

view()

function rotateCCW() {
    for (let x = 0; x < size/2; x++) { 
        for (let y = x; y < size-x-1; y++) { 
            let tmp = mat[x][y]
            mat[x][y] = mat[y][size-1-x]
            mat[y][size-1-x] = mat[size-1-x][size-1-y]
            mat[size-1-x][size-1-y] = mat[size-1-y][x]
            mat[size-1-y][x] = tmp
        } 
    } 
}

function view() {
    for (let fromC = size-1; fromC >= 0; --fromC) {
        let s = ''
        let r = 0
        for (let c = fromC; c < size; ++c) {
            s += ' ' + mat[r++][c]
        }
        print(' '.repeat(fromC) + s.trim() + ' '.repeat(fromC))
    }

    let p = size-1
    let lines = []
    for (let fromC = 0; fromC < size-1; ++fromC) {
        let s = ''
        let r = size-1
        for (let c = fromC; c >= 0; --c) {
            s = mat[r--][c] + ' ' + s
        }
        lines.push(' '.repeat(p) + s.trim() + ' '.repeat(p))
        p--
    }
    print(lines.reverse().join`\n`)
}

