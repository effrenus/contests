// https://www.codingame.com/ide/puzzle/encryptiondecryption-of-enigma-machine

// Implementation

const alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

const operation = readline()
const startN = +readline()

const rots = []
for (let i = 0; i < 3; i++) {
    rots.push(readline())
}

print(
    operation == 'ENCODE'
        ? encode(readline().split``).join``
        : decode(readline().split``).join``
)

function encode(msg) {
    return rots.reduce(
        (acc, rotMap) => rot(acc, rotMap, alpha),
        shift(msg, startN)
    )
}

function decode(msg) {
    return unshift(
        rots.reverse().reduce((acc, srcMap) => rot(acc, alpha, srcMap), msg),
        startN
    )
}

function unshift(s, startN) {
    return s.map((ch, i) => {
        let nidx = (code(ch) - startN - i) % alpha.length
        return alpha[nidx >= 0 ? nidx : alpha.length + nidx]
    })
}

function shift(s, startN) {
    return s.map((ch, i) => alpha[(code(ch) + startN + i) % alpha.length])
}

function rot(s, rotMap, srcMap) {
    return s.map(ch => rotMap[srcMap.indexOf(ch)])
}

function code(ch) { return ch.toLowerCase().charCodeAt() - 97 }

