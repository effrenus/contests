// https://www.codingame.com/ide/demo/843666706aca704c45864a876591ea4513a206

// Brute-force, possible to optimize though.
// Possible TLE, but pass current tests.

readline()
const SEQ = readline()

const len = s => {
    const re = s.match(/(RM)+/g)
    return re ? Math.max(...re.map(v => v.length)) : 0
}

let maxLen = len(SEQ)
for (let i = 0; i < SEQ.length; ++i) {
    const [l, r] = [SEQ.substring(0, i), SEQ.substring(i)]

    maxLen = Math.max(
        maxLen,
        l.slice(-1) == 'R' ? 0 : len(l+'R'+r),
        l.slice(-1) == 'M' ? 0 : len(l+'M'+r)
    )
}

print(maxLen)

