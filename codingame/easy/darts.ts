// https://www.codingame.com/ide/puzzle/darts

type Point = { x: number; y: number }

const W = +readline()
const scores: Map<string, { score: number, order: number }> = new Map(Array.from({ length: +readline() }).map((_, i) => [readline(), { score: 0, order: i }]))

function isInTriangle(p: Point, p0: Point, p1: Point, p2: Point): boolean {
    const A = 1/2 * (-p1.y * p2.x + p0.y * (-p1.x + p2.x) + p0.x * (p1.y - p2.y) + p1.x * p2.y)
    const sign = A < 0 ? -1 : 1
    const s = (p0.y * p2.x - p0.x * p2.y + (p2.y - p0.y) * p.x + (p0.x - p2.x) * p.y) * sign
    const t = (p0.x * p1.y - p0.y * p1.x + (p0.y - p1.y) * p.x + (p1.x - p0.x) * p.y) * sign
    return s >= 0 && t >= 0 && (s + t) <= 2 * A * sign
}

const getScoreFromCoord = ([x, y]: [number, number]): number => {
    // Outside.
    if (Math.abs(x) > W/2 || Math.abs(y) > W/2) return 0

    // Inside diamond.
    if (
        isInTriangle(
            { x, y: Math.abs(y) },
            { x: -W/2, y: 0 },
            { x: 0, y: W/2 },
            { x: W/2, y: 0 }
        )
    ) return 15
    
    // Inside circle.
    if ((x**2 + y**2) <= (W/2)**2) return 10

    // Inside square.
    return 5
}

let N = +readline()
while (N--) {
    const [name, x, y] = readline().split(' ').map(v => Number(v) || v) as [string, number, number]
    scores.get(name).score += getScoreFromCoord([x, y])
}

console.log(
    [...scores.entries()].sort((a, b) => b[1].score == a[1].score ? a[1].order-b[1].order : b[1].score-a[1].score)
        .map(([name, { score }]) => `${name} ${score}`)
        .join('\n')
)

