// https://www.codingame.com/ide/puzzle/auto-pickup

readline()
print(
    function next(o,s='',t=o.slice(0,3),l=parseInt(o.slice(3,7),2)){
        return !o?s:next(o.slice(7+l),s+(t!='101'?'':`001${l.toString(2).padStart(4,'0')}${o.slice(7,7+l)}`))
    }(readline())
)

