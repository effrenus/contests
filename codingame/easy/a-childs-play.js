// https://www.codingame.com/ide/puzzle/a-childs-play

// Memo

const [w, h] = readline().split` `.map(Number)
let steps = +readline()

const map = []
let curPos = null
while (r = readline()) {
    map.push(r.split``.map((v, i) => {
        if (v == 'O') (curPos = [map.length, i], v = '.')
        return v
    }))
}

const dirs = [[-1,0], [0,1], [1,0], [0,-1]]
let dir = 0
const memo = {}
const path = []

while (steps--) {
    let nr, nc

    do {
        [nr, nc] = [curPos[0]+dirs[dir][0], curPos[1]+dirs[dir][1]]
    } while (map[nr][nc] == '#' && (dir = (dir+1) % 4, true)) 

    curPos = [nr, nc]

    if (memo[dir+'-'+curPos]) {
        break
    }

    memo[dir+'-'+curPos] = true
    path.push(curPos)
}

print(
    path[steps%path.length].reverse().join` `
)

