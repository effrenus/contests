// https://www.codingame.com/ide/puzzle/sudoku-validator

type Digit = | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
type Board = Digit[][]

const validSum: number = 45;

const validateRow = (b: Board) => {
  for (const row of b) {
    const sum = row.reduce((acc, v) => acc + v, 0);
    if (sum !== validSum) return false;
  }
  
  return true;
}

const validateCol = (b: Board) => {
  for (let c = 0; c < b[0].length; c++) {
    let sum = 0;
    for (let r = 0; r < b.length; r++) {
      sum += b[r][c];
    }
    if (sum !== validSum) return false;
  }
  
  return true;
}

const validate3x3Subgrid = (b: Board) => {
  const isValidSubgrid = (r: number, c: number) => {
    let sum = 0;
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        sum += b[r+i][c+j];
      }
    }
    return sum === validSum;
  }

  for (let r = 0; r + 2 < b.length; r += 3) {
    for (let c = 0; c + 2 < b[0].length; c += 3) {
      if (!isValidSubgrid(r, c)) {
        return false;
      }
    }
  }
  
  return true;
}

function validSolution(board: Board): boolean {
  return validateRow(board) &&
         validateCol(board) &&
         validate3x3Subgrid(board);
}

const board: Board = []
let row: null | string = null

while (row = readline()) board.push(row.split(' ').map(Number) as Digit[])

console.log(validSolution(board))

