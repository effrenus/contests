// https://www.codingame.com/ide/puzzle/rectangle-partition

// Brute-force + memo

const [w, h, cx, cy] = readline().split` `.map(Number)

let vert = readline().split` `.map(Number)
vert.unshift(0)
vert.push(w)

vert = vert.map((_, i) => !i?vert[i]:vert[i]-vert[i-1])
vert.shift()

let hor = readline().split` `.map(Number)
hor.unshift(0)
hor.push(h)

hor = hor.map((_, i) => !i?hor[i]:hor[i]-hor[i-1])
hor.shift()

let allCnt = 0
const memo = new Map
for (let i1 = 0; i1 < vert.length; ++i1) {
    let w = 0
    for (let i2 = i1; i2 < vert.length; ++i2) {
        w += vert[i2]

        if (memo.has(w)) {
            allCnt += memo.get(w)
            continue
        }
        let cnt = 0

        for (let j1 = 0; j1 < hor.length; ++j1) {
            let v = 0
            for (let j2 = j1; j2 < hor.length; ++j2) {
                v += hor[j2]
                if (w == v) cnt++
            }
        }

        allCnt += cnt
        memo.set(w, cnt)
    }
}

print(allCnt)

