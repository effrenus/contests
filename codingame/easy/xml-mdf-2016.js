// https://www.codingame.com/ide/puzzle/xml-mdf-2016

const weights = {}
let seq = readline()
while (seq.length) seq = seq.replace(/.-./, ([ch],pos) => (weights[ch]=(weights[ch]||0)+1/++pos,''))
print(Object.entries(weights).sort(([,a],[,b]) => b-a)[0][0])
