// https://www.codingame.com/ide/puzzle/lumen

// BFS

const N = parseInt(readline());
const L = parseInt(readline());

const map = []
const lights = []
for (let i = 0; i < N; i++) {
    const row = readline().split` `
    map.push(row)

    for (const [j, ch] of row.entries())
        if (ch =='C') {
            map[i][j] = L
            lights.push([i, j])
        }
}

for (const l of lights) bfs(l)
print(
    map.reduce((c, r) => r.reduce((c, v) => v == 'X'?c+1:c, c), 0)
)

function bfs(pos) {
    const q = [[pos, map[pos[0]][pos[1]]]] // :P

    while (q.length) {
        const [[r, c], w] = q.shift()

        if (w == 1) continue

        for (const [dr, dc] of [[-1,0], [1,0], [0,1], [0,-1], [-1,-1], [-1,1], [1,1], [1,-1]]) {
            const [nr, nc] = [r+dr, c+dc]
            if (
                nr < 0 || nr >= map.length ||
                nc < 0 || nc >= map[0].length ||
                (map[nr][nc] != 'X' && map[nr][nc] >= w-1)
            ) continue
            map[nr][nc] = w-1
            q.push([[nr,nc], w-1])
        }
    }
}

