// https://www.codingame.com/ide/puzzle/a-mountain-of-a-mole-hill

const field = []
for (let i = 0; i < 16; i++) {
    field.push(readline().split``)
}

let cnt = 0
let inside = false

field.forEach((r,i) => r.forEach((v,j) => {
    if (v == '|') inside = !inside
    else if (v == ' ' || (inside && v == 'o')) {
        inside = false
        bfs2([i,j])
    } else {
        field[i][j] = '🦧'
    }
}))

function bfs2(s) {
    const  q = [s]
    while (q.length) {
        const [r,c] = q.shift()
        if (field[r][c] != ' ' && field[r][c] != 'o') {
            field[r][c] = '🐣'
            continue
        }
        if (field[r][c] == 'o') cnt++
        field[r][c] = '🐣'
        for (const [dr,dc] of [[-1,0],[0,1],[1,0],[0,-1],[-1,-1],[-1,1],[1,1],[1,-1]]) {
            const [nr,nc] = [r+dr,c+dc]
            if (nr < 0 || nr > 15 || nc < 0 || nc > 15 || field[nr][nc] == '🐣') continue
            q.push([nr,nc])
        }
    }
}

print(cnt)

