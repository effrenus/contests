// https://www.codingame.com/ide/puzzle/happy-numbers

readline()

const sqDigSum = s => s.split('').map(Number).reduce((s,v) => s+v**2, 0)

const isHappy = (n, seen=new Map) => 
    seen.has(n) || n == 1 
        ? n == 1
        : (seen.set(n, true), isHappy(sqDigSum(''+n), seen))

let num: string | null = null
while (num = readline()) {
    console.log(`${num} ${isHappy(sqDigSum(num)) ? ':)' : ':('}`)
}

