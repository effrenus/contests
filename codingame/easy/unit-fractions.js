// https://www.codingame.com/ide/puzzle/unit-fractions

// Math, factorization.

const n = +readline()

function factorize(n) {
    const f = []

    if (n%2 == 0) {
        f.push(2)
        while(n%2 == 0) n /= 2
    }

    let i = 3
    while (n > 1) {
        if (n%i == 0) f.push(i)
        while (n%i == 0) n /= i
        i += 2
    }
    return f
}

const ans = []
const seen = new Map

function find(f) {
    let i = 2
    let x  = f*i
    while (x < f*70000000) {
        const y = x*n/(x-n)
        if (
            Number.isInteger(y) &&
            x >= y && y >= 0 &&
            !seen.has(`${x},${y}`)
        ) {
            seen.set(`${x},${y}`, true)
            ans.push([x, y])
        }
        x = f * ++i
    }
}

for (const i of factorize(n)) find(i)

print(
    ans
        .sort((a,b) => b[0] - a[0])
        .map(([x, y]) => `1/${n} = 1/${x} + 1/${y}`)
        .join`\n`
)

