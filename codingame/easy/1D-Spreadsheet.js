// https://www.codingame.com/ide/puzzle/1d-spreadsheet

// Think it's possible to use toposort, but for small constraints this solution is fine.

const N = parseInt(readline());
const cells = new Int32Array(N)

const get = arg => arg[0] == '$' ? cells[+arg.slice(1)] : +arg

const eval = (i, op, arg1, arg2) => {
    switch (op) {
        case 'VALUE':
            cells[i] = get(arg1); break
        case 'ADD':
            cells[i] = get(arg1) + get(arg2); break
        case 'SUB':
            cells[i] = get(arg1) - get(arg2); break
        case 'MULT':
            cells[i] = get(arg1) * get(arg2); break
    }
}

const ops = []
for (let i = 0; i < N; i++) {
    ops.push([i, ...readline().split` `])
}

const evaled = {}
const isEvaled = arg => arg[0] == '$' ? evaled[arg] : true

let cnt = ops.length
while (cnt) {
    for (let i = 0; i < ops.length; ++i) {
        if (ops[i] ==  null) continue

        const [k, op, a1, a2] = ops[i]

        if (
            (op != 'VALUE' && ![a1, a2].every(isEvaled)) ||
            (op == 'VALUE' && !isEvaled(a1))
        ) continue

        eval(...ops[i])
        ops[i] = null
        cnt--
        evaled['$'+i] = true
    }
}

print(cells.join`\n`)

