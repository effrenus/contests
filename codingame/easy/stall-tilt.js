// https://www.codingame.com/ide/puzzle/stall-tilt

let n = +readline()
let b = +readline()
const alpha = 'abcdefghijklmnopqrstuvwxyz'.split``

const cs = Array(n).fill().map(_ => [alpha.shift(), +readline()])
let bs = Array(b).fill().map(_ => Math.floor(Math.sqrt(Math.tan(Math.PI/3)*readline()*9.81)))

for (const c of cs) {
    let i = 1
    for (const b of bs) {
        if (c[1] > b) break
        i++
    }
    c.push(i)
}

print(Math.min(...bs))
print(['y', ...cs.sort((a,b) => a[2]==b[2] ? b[1]-a[1] : b[2]-a[2]  ).map(v=>v[0])].join`\n`)

