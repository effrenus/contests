// https://www.codingame.com/ide/puzzle/horse-racing-hyperduals

const hs = Array(+readline()).fill(null).map(_ => readline().split` `.map(Number))

let min = Infinity
for (let i = 0; i < hs.length-1; ++i) {
    for (let j = i+1; j < hs.length; ++j) {
        min = Math.min(Math.abs(hs[i][0]-hs[j][0])+Math.abs(hs[i][1]-hs[j][1]), min)
    }
}

print(min)

