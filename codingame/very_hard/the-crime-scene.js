// https://www.codingame.com/ide/puzzle/the-crime-scene

// Convex hull

const ROLL_LENGTH = 5
const MIN_DIST_FROM_CLUE = 3

const N = +readline()
const points = []

for (let i = 0; i < N; i++) {
    points.push(readline().split` `.map(Number))    
}

const seen = new Map(points.map(([x,y]) => [`${x},${y}`, true]))
const addPoints = []
for (const [x, y] of points) {
    [[-1,1], [0,1], [1,1], [1,0], [1,-1], [0,-1], [-1,-1], [-1,0]]
        .map(([dx, dy]) => [dx*MIN_DIST_FROM_CLUE, dy*MIN_DIST_FROM_CLUE])
        .forEach(([dx, dy]) => {
            if (seen.has(`${x+dx},${y+dy}`)) return
            addPoints.push([x+dx, y+dy])
            seen.set(`${x+dx},${y+dy}`, true)
        })
}

points.push(...addPoints)

const poly = convexHull(points)
let perim = 0
for (let i = 0; i < poly.length; ++i) {
    perim += getDist(poly[i], i == poly.length-1 ? poly[0] : poly[i+1])
}

print(perim/ROLL_LENGTH|0)



function getDist([x1, y1], [x2, y2]) {
    return Math.sqrt(Math.pow(x1-x2, 2)+Math.pow(y1-y2, 2))
}

function convexHull(points) {
   points.sort(function(a, b) {
      return a[0] == b[0] ? a[1] - b[1] : a[0] - b[0];
   });

   var lower = [];
   for (var i = 0; i < points.length; i++) {
      while (lower.length >= 2 && cross(lower[lower.length - 2], lower[lower.length - 1], points[i]) <= 0) {
         lower.pop();
      }
      lower.push(points[i]);
   }

   var upper = [];
   for (var i = points.length - 1; i >= 0; i--) {
      while (upper.length >= 2 && cross(upper[upper.length - 2], upper[upper.length - 1], points[i]) <= 0) {
         upper.pop();
      }
      upper.push(points[i]);
   }

   upper.pop();
   lower.pop();
   return lower.concat(upper);
}

function cross(a, b, o) {
   return (a[0] - o[0]) * (b[1] - o[1]) - (a[1] - o[1]) * (b[0] - o[0])
}

