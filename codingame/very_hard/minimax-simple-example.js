// https://www.codingame.com/ide/puzzle/minimax-simple-example

// Minimax, alpha-beta pruning

const Players = { First: 1, Second: 2 }

const [lettersCount, wordsCount] = readline().split` `.map(Number)
const letters = readline().split` `

const wordScore = new Map
for (let i = 0; i < wordsCount; ++i) {
    const [word, score] = readline().split` `
    wordScore.set(word.replace(/(.)\1*/g, '$1'), +score)
}


const bestOutcome = minimax()[1]
print(`${bestOutcome.ch} ${bestOutcome.p1}-${bestOutcome.p2}`)


function minimax(p1 = [], p2 = [], lettersToChoose = letters, currentPlayer = Players.First, α = -Infinity, β = Infinity) {
    if (!lettersToChoose.length) {
        const [sc1, sc2] = [getScore(p1), getScore(p2)]
        return [sc1 - sc2, { ch: p1[0], p1: sc1, p2: sc2 }]
    }
    
    let res = [currentPlayer == Players.First ? -Infinity : Infinity]
    const fn = currentPlayer == Players.First ? Math.max : Math.min

    const chooseLetters = [
        [lettersToChoose[0], lettersToChoose.slice(1)],
        [lettersToChoose[1], [lettersToChoose[0], ...lettersToChoose.slice(2)]],
    ]

    for (const [letter, nl] of chooseLetters) {
        if (!letter) continue

        if (currentPlayer == Players.First) p1.push(letter)
        if (currentPlayer == Players.Second) p2.push(letter)

        const [bestSoFar, finState] = minimax(p1, p2, nl, swapPlayers(currentPlayer), α, β)
        
        if (
            currentPlayer == Players.First && bestSoFar > res[0] ||
            currentPlayer == Players.Second && bestSoFar < res[0]
        ) {
            res = [bestSoFar, finState]
        }

        if (currentPlayer == Players.First) p1.pop()
        if (currentPlayer == Players.Second) p2.pop()

        if (currentPlayer == Players.First) {
            α = fn(res[0], α)
            if (α >= β) return res
        } else {
            β = fn(res[0], β)
            if (β <= α) return res
        }
    }

    return res
}

function swapPlayers(currenPlayer) {
    return currenPlayer == Players.First ? Players.Second : Players.First
}

function getScore(letters) {
    const sl = letters.slice().sort()
    let score = 0
    for (const [word, wScore] of wordScore.entries()) {
        if (word.split``.every(ch => sl.includes(ch))) score += wScore
    }
    return score
}

