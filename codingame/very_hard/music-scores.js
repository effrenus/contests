// https://www.codingame.com/ide/puzzle/music-scores

// Heuristic, BFS

const [w, h] = readline().split` `.map(Number)
const msg = readline().split` `

let mat = Array.from({length:h}).map(_=>Array.from({length:w}).fill(0))

let i = j = 0
while (msg.length) {
  const t = msg.shift()
  let len = +msg.shift()
  
  let k = 0
  while (k < len) {
    mat[i][j] = t=='B'?0:1
    if (j==w-1) i++
    j = (j+1)%w
    k++
  }
}

let pos = null
for (let j = 0; j < w && !pos; j++) {
  for (let i = 0; i < h; i++) {
    if (mat[i][j] == 0) { pos = [i, j]; break }
  } 
}

// Lines
let lineDetected = false
let lineHDetected = false
let lineH = 0
const lines = []
for (let i = pos[0]; i < h; i++) {
  if (mat[i][pos[1]] == 1) { // white space skip
    if (lineDetected && !lineHDetected) lineHDetected = true
    lineDetected = false
    continue
  }
  
  if (!lineDetected) { // First black is upper line pos
    lineDetected = true
    lines.push(i)
  }
  
  if (!lineHDetected) lineH++ // Detect line height
  
  for (let j = pos[1]; j < w; j++) { // Earse line
    mat[i][j] = 1
  }
}

let betweenLines = lines[1]-lines[0]-lineH // Space height between lines

for (let i = 0; i < lineH; i++) {
  const pos = lines[lines.length-1]+lineH+betweenLines+i
  for (let j = 0; j < w; j++) { // Earse line
    mat[pos][j] = 1
  }
}

lines.push(lines[lines.length-1]+lineH+betweenLines) // Add bottom line

const bfs = (pos, id, cl) => {
  const q = [pos]
  const vis = {}
  let maxRow = -Infinity
  let minRow = Infinity
  let minCol = Infinity
  let maxCol = -Infinity
  let color = cl || 2
  let c = 0
  const hs = new Map
  
  while (q.length) {
    const [i, j] = q.shift()
    const k = `${i},${j}`
    
    if (vis[k]) continue
    vis[k] = true
    
    mat[i][j] = id
    c++
    minRow = Math.min(minRow, i)
    maxRow = Math.max(maxRow, i)
    maxCol = Math.max(maxCol, j)
    minCol = Math.min(minCol, j)
    
    hs.set(i, (hs.get(i)||0)+1)
    
    for (const [di,dj] of [[1,0],[-1,0],[0,1],[0,-1],[-1,1],[1,1]]) {
      const [ni, nj] = [i+di, j+dj]
      if (ni <0 || ni >= h || nj < 0 || nj >= w || mat[ni][nj] == 1) continue
      q.push([ni, nj])
    }
  }
  
  return [minRow, maxCol, c, maxRow, hs, minCol]
}

const notes = []

const bfs2 = (pos, cl) => {
  const q = [pos]
  const vis = {}
  let maxRow = -Infinity
  let minRow = Infinity
  let maxCol = -Infinity
  let color = cl || 2
  let c = 0
  
  while (q.length) {
    const [i, j] = q.shift()
    const k = `${i},${j}`
    
    if (vis[k]) continue
    vis[k] = true
    
    if (mat[i][j] > 1) {
      const note = notes[mat[i][j]-3]
      if (note[4].has(i)) {
        note[4].set(i, note[4].get(i)-1)
        if(note[4].get(i) == 0) note[4].delete(i)
      }
    }
    mat[i][j] = color
    c++
    minRow = Math.min(minRow, i)
    maxRow = Math.max(maxRow, i)
    maxCol = Math.max(maxCol, j)
    
    for (const [di,dj] of [[1,0]]) {
      const [ni, nj] = [i+di, j+dj]
      if (ni <0 || ni >= h || nj < 0 || nj >= w) continue
      q.push([ni, nj])
    }
  }
  
  return [minRow, maxCol, c, maxRow]
}

for (let j = 0; j < w; j++) {
  for (let i = 0; i < h; i++) {
    if (mat[i][j] == 0) {
      const [r, maxCol, cc, mr, hs, minCol] = bfs([i,j], notes.length+3)
      notes.push([r, maxCol, cc, mr, hs, minCol])
      j = maxCol
      break
    }
  } 
}

for (let j = 0; j < w; j++) {
  for (let i = 0; i < h; i++) {
    if (mat[i][j] == 0) {
      bfs2([i,j], 1)
      break
    }
  } 
}

notes.forEach(note=>{
  const l = [...note[4].keys()].sort((a,b)=>a-b)
  for (let j = 0; j < w; j++) {
    mat[l[0]][j] = 0
    mat[l[l.length-1]][j] = 0
  }
})

lines.forEach(line=>{
  for (let j = 0; j < w; j++) {
    mat[line][j] = 3
  }
})

const FN = [,'E','C','A','F','D']
const HN = [,'D','B','G','E','C']

// [minRow, maxCol, c, maxRow, hs, minCol]
const getT = ([,maxCol,count,,,minCol],[minRow,maxRow]) => {
  return count >= (maxRow-minRow)*(maxCol-minCol)/1.5 ? 'Q' : 'H'
}

console.log(notes.map(note=>{
  const l = [...note[4].keys()].sort((a,b)=>a-b)
  const [min, max] = [l[0], l[l.length-1]]
  
  for (let i = 0; i < lines.length; i++) {
    if (min > lines[i]) continue
    
    if (i == 0) return ((max-min>betweenLines/2)?'G':'F')+getT(note, [min,max])
    if (min <= lines[i-1]+lineH+3) return FN[i]+getT(note, [min,max])
    else return HN[i]+getT(note, [min,max])
  }
}).join` `)

