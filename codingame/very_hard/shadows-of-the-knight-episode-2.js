// https://www.codingame.com/ide/puzzle/shadows-of-the-knight-episode-2

// TODO: Pass 100% tests

const [w, h] = readline().split` `.map(Number)
const steps = +readline()
let pos = readline().split` `.map(Number)

let xl = 0
let xr = w-1

let dir

while (true) {
    let exit = false
    const state = readline() // COLDER | WARMER | SAME | UNKNOWN
    let nx

    if (xr - xl == 1) {
        if (state == 'WARMER') break

        if (pos[0] == xl) pos[0] = xr
        else pos[0] = xl
        print(pos.join` `)
        break
    }

    switch (state) {
        case 'UNKNOWN':
            nx = (xl+xr) >> 1
            dir = nx-pos[0]
            pos[0] = nx
            break
        case 'COLDER':
            if (dir < 0) xl = pos[0]; else xr = pos[0]
            nx = (xl+xr) >> 1
            dir = nx-pos[0]
            pos[0] = nx
            break
        case 'WARMER':
            if (dir < 0) xr = pos[0]; else xl = pos[0]
            nx = (xl+xr) >> 1
            dir = nx-pos[0]
            pos[0] = nx
            break
        case 'SAME':
            exit = true
            break
    }
    if (exit) break
    print(pos.join` `)
}

let yl = 0
let yr = h-1

let prev
function check(pos) {
    prev = pos[1]
    print(pos.join` `)
    while (true) {
        return readline()
    }
}

let cl = 0
function vert(yl, yr) {
    let ny
    let part
    let m = 16
    if (pos[1]-yl > yr-pos[1]) {
        part = 'l'
        cl++
        ny = (pos[1]+yl)/m|0
    } else {
        cl = 0
        part = 'r'
        ny = (pos[1]+yr)>>1
    }
    print(`${pos[0]} ${ny}`)
    console.error(pos[1], ny, yl, yr)

    while (true) {
        const state = readline()
        console.error(state)
        switch (state) {
            case 'COLDER':
                if(part == 'l') yl = pos[1]; else yr = pos[1];
                break
            case 'WARMER':
                if(part == 'l') yr = pos[1]; else yl = pos[1];
                break
        }
        break
    }
    pos[1] = ny
    vert(yl, yr)
}

vert(0, h-1)

