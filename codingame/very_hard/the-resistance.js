// https://www.codingame.com/ide/puzzle/the-resistance

// Memo, recursion

const code = readline()
const len = code.length
let N = +readline()

const rootTrie = Array(26)

function add(trie, word) {
    if (!word) {
        trie.isWord = true
        if (!trie.count) trie.count = 1
        else trie.count++
        return
    }
    const k = word[0].charCodeAt()-97

    if (!trie[k]) trie[k] = Array(26)

    add(trie[k], word.slice(1))
}

while (N--) add(rootTrie, readline().toLowerCase())

const tbl = {
    '.-': 0, '-...': 1, '-.-.': 2, '-..': 3, '.': 4, '..-.': 5,
    '--.': 6, '....': 7, '..': 8, '.---': 9, '-.-': 10, '.-..': 11, '--': 12, '-.': 13, '---': 14,
    '.--.': 15, '--.-': 16, '.-.': 17, '...': 18, '-': 19, '..-': 20, '...-': 21, '.--': 22, '-..-': 23,
    '-.--': 24, '--..': 25
}

const memo = new Map
const enc = new Map

const decode = (trie, pos) => {
    if (!memo.has(trie)) memo.set(trie, {})
    if (memo.get(trie)[pos]) return memo.get(trie)[pos]

    if (pos == code.length) return trie.isWord ? 1 : 0
    if (pos > code.length) return 0

    let w = 0
    for (let i = 1; pos+i <= len && i <= 4; ++i) {
        if (!trie[tbl[code.substr(pos, i)]]) continue
        w += decode(trie[tbl[code.substr(pos, i)]], pos + i)
    }

    if (!trie.isWord) {
        memo.get(trie)[pos] = w
        return w
    }

    for (let i = 1; pos+i <= len && i <= 4; ++i) {
        if (!rootTrie[tbl[code.substr(pos, i)]]) continue
        w += decode(rootTrie[tbl[code.substr(pos, i)]], pos + i)
    }

    memo.get(trie)[pos] = w

    return w
}

print(decode(rootTrie, 0))
