// https://www.codingame.com/ide/puzzle/recurring-decimals

let num = 1
let denom = +readline()

let n = Math.trunc(num/denom)
num -= denom*n

let d = ''
const enc = new Map
let pos = 0
let start = -1

while (true) {
    if (!num) break

    if (enc.has(num)) {
        start = enc.get(num)
        break
    }
    
    enc.set(num, pos++)
    const m = num*10
    d += Math.trunc(m/denom)
    num = m%denom
}

print(`0.${start==-1 ? d : d.slice(0,start) + `(${d.substr(start)})`}`)
