const segs = {
  0:[1,1,1,1,1,1,0],
  1:[0,1,1,0,0,0,0],
  2:[1,1,0,1,1,0,1],
  3:[1,1,1,1,0,0,1],
  4:[0,1,1,0,0,1,1],
  5:[1,0,1,1,0,1,1],
  6:[1,0,1,1,1,1,0],
  7:[1,1,1,0,0,0,0],
  8:[1,1,1,1,1,1,1],
  9:[1,1,1,1,0,1,1],
}

const findDiff = (a, b) => Array(7).fill(0).reduce((d,_,i) => d + Math.abs(segs[a][i]-segs[b][i]), 0)
const timeAfterMinute = (time) => {
  let [h, m] = time.split`:`.map(v => parseInt(v, 10))
  m = String(m+1 == 60 ? '00' : '0'+(m+1)).slice(-2)
  h = String(m != '00' ? '0'+h : h+1 == 24 ? '00' : '0'+(h+1)).slice(-2)
  return h+':'+m
}

const countSegsDiff = (time) => {
 let diff = 0
 const nextTime = timeAfterMinute(time)
 for (let i = 0; i < time.length; ++i) {
    if (time[i] == ':') continue
    diff += findDiff(time[i], nextTime[i])
 }
 return diff
}

console.log(countSegsDiff('12:34'))
console.log(countSegsDiff('15:10'))
console.log(countSegsDiff('01:59'))

