// https://www.codingame.com/ide/puzzle/short-accounts-make-long-friends

const target = +readline()
const originalValues = readline().split` `.map(Number)

const ops = {
    '+': (a, b) => a+b,
    '/': (a, b) => Number.isInteger(a/b) ? a/b : null,
    '*': (a, b) => a*b,
    '-': (a, b) => a-b >= 0 ? a-b : null,
}
const copyWithoutIdx = (arr, idxs) => arr.filter((_,i) => !idxs.includes(i))

let minOps = Infinity
let minDiff = Math.min(...originalValues.map(v => Math.abs(target-v)))

;(function solve(curValues, opsSoFar) {
    if (curValues.includes(target)) minOps = opsSoFar
    if (opsSoFar >= minOps || curValues.length == 1) return

    for (let i = 0; i < curValues.length; ++i) {
        for (let j = i+1; j < curValues.length; ++j) {
            for (const op of ['+','-','/','*']) {
                const [r1, r2] = [
                    ops[op](curValues[i], curValues[j]),
                    ops[op](curValues[j], curValues[i])
                ]

                if (Number.isInteger(r1)) solve([r1, ...copyWithoutIdx(curValues,[i,j])], opsSoFar+1)
                if (r2 !== r1 && Number.isInteger(r2)) solve([r2, ...copyWithoutIdx(curValues,[i,j])], opsSoFar+1)

                minDiff = Math.min(minDiff, Math.abs(target-(r1||0)), Math.abs(target-(r2||0)))
            }
        }
    }
})(originalValues, 0)

print(minOps == Infinity ? `IMPOSSIBLE\n${minDiff}` : `POSSIBLE\n${minOps}`)

