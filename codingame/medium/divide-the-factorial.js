// https://www.codingame.com/ide/puzzle/divide-the-factorial

// Legendre’s formula :)

const len = 1000000
const ns = Array.from({length: len}).fill(true)
const done = new Map
const primes = []

let pos = 2
    
while (primes.length < 2000) {
    while(ns[pos] == false) pos++
    
    primes.push(pos)

    let i = pos*pos
    while(i < len) {
        ns[i] = false
        i += pos
    }
    
    pos++
}

let [a, b] = readline().split` `.map(Number)
// a = BigInt(a)
// b = BigInt(b)

// let x = 0
// let p = 1n
// let i = 2n
// while (i <= b) {
//     p *= (i%a)||1n
//     i++
// }

// print(p/a)

const memo = new Map
function factorize(n, mx=Infinity) {
    if (memo.has(n)) return memo.get(n)

    let k = 0
    let i = primes[k]
    const f = {}
    while (n != 1 && i <= mx) {
        if (memo.has(n)) {
            for (const [w, v] of memo.get(n)) {
                f[w] = (f[w]||0)+v
            }
            break
        }
        while (n%i == 0) {
            if (!f[i]) f[i] = 0
            f[i]++
            n /= i
        }
        i = primes[k++]
    }

    memo.set(n, Object.entries(f))

    return memo.get(n)
}

// const mx = Math.max(...factorize(a).map(([v])=>v))

// const ff = new Map
// let j = 2
// while(j <= b) {
//     if (j==2000000) print(j)
//     for (const [n, c] of factorize(j++, mx)) {
//         ff.set(n, (ff.get(n)||0)+c)
//     }
// }

// function find() {
//     let x = 0
//     let ff = factorize(a).map(([n, c]) => Math.pow(n, c))
//     while(b > 1) {
//         let i = 0
//         while (i < ff.length) {
//             if (b%ff[i]) return x
//             b = b/ff[i]
//             i++
//         }
//         if (i != ff.length) return x
//         x++
//     }
//     return x
// }

function max_pow(n) {
    let v = b
    let x = 0
    while (v >= 1) {
        const d = Math.trunc(v/n)
        x += d
        v = d
    }

    return x
}

print(
    Math.min(
        ...factorize(a).map(([n, c]) => max_pow(+n)/c|0)
    )
)

// 14 2*7
// 13 13
// 12 2*2*3
// 11 11
// 10 2*5
// 9  3*3
// 8  2*2*2
// 7  7
// 6  2*3
// 5  5
// 4  2*2
// 3  3
// 2  2
// ...
