// https://www.codingame.com/ide/puzzle/create-the-longest-sequence-of-1s

const b:string = readline()

let prevOnesCount = 0
let prevZero = 0
let max = b.length ? 1 : 0

for (let i = 0; i < b.length; ++i) {
    let cntOne = 0
    while (b[i] === '1') {
        cntOne++
        i++
    }

    max = Math.max(
        max,
        prevZero == 1 ? cntOne + prevOnesCount + 1: -Infinity,
        prevZero ? cntOne+1 : cntOne
    )

    prevOnesCount = cntOne

    prevZero = 0
    while (b[i] == '0') {
        prevZero++
        i++
    }
    i--
}

console.log(
    String(max)
)
