// https://www.codingame.com/ide/puzzle/network-cabling

// Median

let N = +readline()
const houses = []

while (N--) houses.push(readline().split` `.map(Number))

houses.sort((a, b) => a[1] - b[1])

const len = houses.length
const m = houses.length % 2 ? houses[len/2|0][1] : (houses[len/2][1]+houses[len/2-1][1])/2

let l = 0
houses.forEach(h => l += Math.abs(h[1]-m))

houses.sort((a, b) => a[0] - b[0])
l += Math.abs(houses[len-1][0]-houses[0][0])

print(l)
