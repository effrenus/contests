// https://www.codingame.com/ide/puzzle/depot-organization

// Backtracking, brute-force.

const TILES_COUNT = 7
const TILE_SIDES_COUNT = 6
const tiles = []

for (let i = 0; i < TILES_COUNT; i++) {
    tiles.push(readline().split` `)
}

const usedTiles = new Uint8Array(TILES_COUNT).fill(0)
const slots = new Int8Array(TILES_COUNT).fill(-1)

let validConfig = null

function isValid(slotNum) {
    switch (slotNum) {
        case 0:
            return tiles[slots[slotNum]][1] == tiles[slots[3]][4]
        case 1:
            return tiles[slots[slotNum]][3] == tiles[slots[0]][0] &&
                   tiles[slots[slotNum]][2] == tiles[slots[3]][5]
        case 2:
            return tiles[slots[slotNum]][5] == tiles[slots[0]][2] &&
                   tiles[slots[slotNum]][0] == tiles[slots[3]][3]
        case 4:
            return tiles[slots[slotNum]][4] == tiles[slots[1]][1] &&
                   tiles[slots[slotNum]][3] == tiles[slots[3]][0]
        case 5:
            return tiles[slots[slotNum]][4] == tiles[slots[2]][1] &&
                   tiles[slots[slotNum]][5] == tiles[slots[3]][2]
        case 6:
            return tiles[slots[slotNum]][4] == tiles[slots[3]][1] &&
                   tiles[slots[slotNum]][5] == tiles[slots[4]][2] &&
                   tiles[slots[slotNum]][3] == tiles[slots[5]][0]
    }
}

function shiftTile(num) {
    const tile = tiles[num]
    tiles[num] = [tile[tile.length-1], ...tile.slice(0, -1)]
}

function findConfiguration(slotNum) {
    if (slotNum == 7) {
        printErr('SUCCESS')
        printErr([...slots].map(i => tiles[i].join` `).join`\n`)

        print([...slots].map(i => i+tiles[i][0]).join` `)

        validConfig = slots.slice()
        return
    }

    for (let i = 0; i < TILES_COUNT && !validConfig; ++i) {
        if (usedTiles[i]) continue

        usedTiles[i] = 1
        slots[slotNum] = i

        for (let j = 0; j < TILE_SIDES_COUNT; ++j) {
            shiftTile(i)
            if (!isValid(slotNum)) continue
            findConfiguration(slotNum == 2 ? 4 : slotNum+1)
        }

        usedTiles[i] = 0
        slots[slotNum] = -1
    }
}

for (let i = 0; i < TILES_COUNT; ++i) {
    usedTiles[i] = 1
    slots[3] = i

    const lowestChar = tiles[i].slice().sort()[0]

    while (tiles[i][0] != lowestChar) shiftTile(i)
    findConfiguration(0)
    usedTiles[i] = 0

    if (validConfig) break
}

