// https://www.codingame.com/ide/puzzle/cgfunge-interpreter

// Implementation

const N = +readline()
const code = []
for (let i = 0; i < N; i++) {
    code.push(readline().split``)
}

const stack = []
const dirs = [[0,1], [0,-1], [1,0], [-1,0]]

let r = 0
let c = 0
let dir = dirs[0]

let isProgramEnd = false
let out = ''

while (!isProgramEnd) {
    const ch = code[r][c]

    switch (ch) {
        case '>': dir = dirs[0]; break
        case '<': dir = dirs[1]; break
        case '^': dir = dirs[3]; break
        case 'v': dir = dirs[2]; break
        case 'S': [r, c] = [r+dir[0], c+dir[1]]; break
        case 'E': isProgramEnd = true; break
        case '"':
            {   
                while (code[r+dir[0]][c+dir[1]] != '"') {
                    r += dir[0]
                    c += dir[1]
                    stack.unshift(code[r][c].charCodeAt())
                }
                r += dir[0]
                c += dir[1]
            }
            break
        case 'P': stack.shift(); break
        case 'X': [stack[0], stack[1]] = [stack[1], stack[0]]; break
        case 'D': stack.unshift(stack[0]); break
        case '_': dir = stack.shift() == 0 ? dirs[0] : dirs[1]; break
        case '|': dir = stack.shift() == 0 ? dirs[2] : dirs[3]; break
        case 'I': out += stack.shift(); break
        case 'C': out += String.fromCharCode(stack.shift()); break
        case '+': stack.unshift(stack.shift()+stack.shift()); break
        case '*': stack.unshift(stack.shift()*stack.shift()); break
        case '-': stack.unshift(-stack.shift()+stack.shift()); break
        default: if (/\d/.test(ch)) stack.unshift(+ch)
    }
    r += dir[0]
    c += dir[1]
}

print(out)
