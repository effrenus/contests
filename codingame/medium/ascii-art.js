// https://www.codingame.com/ide/puzzle/ascii-art

const L = parseInt(readline());
const H = parseInt(readline());

const chs = readline().toLowerCase().split``;
const alpha = Array(27).fill().map(_=>[])

for (let i = 0; i < H; i++) {
    const row = readline();
    for (let l = 0; l < 27; l++) {
        alpha[l].push(row.substr(l*L, L))
    }
}

for (let i = 0; i < H; i++) {
    let r = ''
    for (let j = 0; j < chs.length; j++) {
        let k = chs[j].charCodeAt()-97
        if (k < 0 || k > 25) k = 26
        r += alpha[k][i]
    }
    print(r)
}

