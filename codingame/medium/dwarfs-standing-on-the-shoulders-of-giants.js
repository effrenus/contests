// https://www.codingame.com/ide/puzzle/dwarfs-standing-on-the-shoulders-of-giants

// DFS. Trivial solution, better toposort.

let N = +readline()

const g = []
while (N--) {
    const [f, t] = readline().split` `.map(Number)
    if (!g[f]) g[f] = []
    g[f].push(t)
}

let maxDepth = -Infinity
for (let i = 0; i < g.length; ++i) dfs(i)

print(maxDepth)

function dfs(i) {
    const q = [[i, 1]]
    while (q.length) {
        const [n, d] = q.shift()
        maxDepth = Math.max(maxDepth, d)
        for (const nn of (g[n]||[])) {
            q.unshift([nn, d+1])
        }
    }
}
