// https://www.codingame.com/ide/puzzle/mayan-calculation

// Implementation

const [w, h] = readline().split` `.map(Number)

const dict = []
for (let i = 0; i < h; ++i) {
    dict.push(readline().split``)
}

const nums = new Map
const rev = new Map
const frev = new Map
let k = 0
while (k < 20) {
    let n = ''
    let fn = ''
    for (let r = 0; r < h; ++r) {
        for (let c = k*w; c < k*w+w; ++c) {
            n += dict[r][c]
            fn += dict[r][c]
        }
        fn += '\n'
    }
    nums.set(n, k)
    rev.set(k, n)
    frev.set(k, fn.slice(0,-1))
    k++
}

const n1 = decode(+readline())
const n2 = decode(+readline())
const op = readline()

print(encode(eval(`${n1}${op}${n2}`)))

function encode(num) {
    if (num == 0) return frev.get(0)
    const ans = []
    while (num) {
        const r = num%20
        ans.unshift(frev.get(r))
        num = Math.trunc(num/20)
    }
    return ans.join`\n`
}

function decode(lineCnt) {
    let st = lineCnt/h-1
    let n = 0
    while (lineCnt) {
        lineCnt -= h
        let d = h
        let ss = ''
        while (d--) ss += readline()
        n += nums.get(ss)*Math.pow(20, st--)
    }
    return n
}
