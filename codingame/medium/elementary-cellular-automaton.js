// https://www.codingame.com/ide/puzzle/elementary-cellular-automaton

const R = +readline()
let N = +readline()
const startPattern = readline()

const states = new Map
const bin = R.toString(2).padStart(8, '0')

for (let i = bin.length-1, j = 0; i >= 0; --i, ++j) states.set(j, bin[i])

let pattern = startPattern
while (N--) {
    print(pattern)
    pattern = next(pattern)
}

function next(p) {
    let nextPattern = ''

    for (let i = 0; i < p.length; ++i) {
        let s = (i > 0?p[i-1]:p[p.length-1]) + p[i] + (i < p.length-1?p[i+1]:p[0])
        nextPattern += states.get(parseInt(s.replace(/\./g, '0').replace(/@/g, '1'), 2))
    }

    return nextPattern.replace(/1/g, '@').replace(/0/g, '.')
}

/**
 * [ . . @ . @ ]
 * [ @ . . ] - 1
 * [ . . @ ] - 2
 * [ . @ . ] - 3
 */

