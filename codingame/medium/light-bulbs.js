// https://www.codingame.com/ide/puzzle/light-bulbs

// Recursion. Possible to use precomputed counts after first toggle.

const s = readline().split``.map(Number)
const p = readline().split``.map(Number)

let cnt = 0

for (let i = 0; i < s.length; ++i) {
    if (s[i] == p[i]) continue
    toggle(i, p[i])
    cnt++
}

print(cnt)

function toggle(i, val) {
    if (i == s.length-1) {
        if (s[i] == val) return
        s[i] = val
        return
    }

    s[i] = val

    if (s[i+1] != 1) {
        toggle(i+1, 1)
        s[i+1] = 1
        cnt++
    }

    for (let k = i+2; k < s.length; ++k) {
        if (s[k] == 0) continue
        toggle(k, 0)
        s[k] = 0
        cnt++
    }
}

