// https://www.codingame.com/ide/puzzle/micro-assembly

type AddCommand = ['ADD', string, string, string]
type MovCommand = ['MOV', string, string]
type SubCommand = ['SUB', string, string, string]
type JneCommand = ['JNE', string, string, string]

type Command = 
    | AddCommand
    | MovCommand
    | SubCommand
    | JneCommand

type Register =
    | 'a' | 'b' | 'c' | 'd'
type Registers = { [k in Register]: number }

class CPU {
    private registers: Registers = { a: 0, b: 0, c: 0, d: 0 }
    private ipointer = 0

    constructor(registers: [[Register, number]]) {
        registers.forEach(([r, val]) => this.registers[r] = val)
    }

    run(code: string) {
        const commands = this.parseCode(code)
        while (this.ipointer < commands.length) {
            this.execute(commands[this.ipointer++])
        }
        return this
    }
    get allRegisters(): number[] {
        const {a,b,c,d} = this.registers
        return [a, b, c, d]
    }

    private execute(command: Command) {
        switch (command[0]) {
            case 'ADD':
                this.saveVal(command[1], this.getValue(command[2]) + this.getValue(command[3])); break
            case 'SUB':
                this.saveVal(command[1], this.getValue(command[2]) - this.getValue(command[3])); break
            case 'MOV':
                this.saveVal(command[1], this.getValue(command[2])); break
            case 'JNE':
                if (this.getValue(command[2]) !== this.getValue(command[3])) {
                    this.ipointer = this.getValue(command[1])
                }
                break
        }
    }
    private parseCode(code: string): Command[] {
        return code.split('\n').map(val => val.split(' ') as Command)
    }
    private getValue(param: string): number {
        return /[a-z]/.test(param) ? this.registers[param] : Number(param)
    }
    private saveVal(register: string, value: number) {
        this.registers[register] = value
    }
}

console.log(
    new CPU(retrieveRegisters()).run(retrieveProgram()).allRegisters.join(' ')
)



function retrieveRegisters(): [[Register, number]] {
    return readline()
        .split(' ').map(Number)
        .reduce((acc, v, i) => 
            (acc[i][1] = v, acc), [['a', 0], ['b', 0], ['c', 0], ['d', 0]]) as unknown as [[Register, number]]
}

function retrieveProgram(): string {
    let N = Number(readline())
    let commands = ''
    while (N--) commands += readline() + '\n'
    return commands.trim()
}

