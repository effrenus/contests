// https://www.codingame.com/ide/puzzle/dynamic-sorting

const readRecord = () => readline().split`,`.map(f => f.split`:`).reduce((acc, [name, val]) => ({ ...acc, [name]: val }), {})

const cmps = {
    int: { asc: (a, b) => a - b, desc: (a, b) => b - a },
    string: { asc: (a, b) => a < b ? -1 : (a > b ? 1 : 0), desc: (a, b) => -cmps.string.asc(a, b) }
}
const orderBySym = { '+': 'asc', '-': 'desc' }

const orderFields = readline().match(/([+-][a-z]+)/g).map(f => ({ order: orderBySym[ f[0] ], name: f.slice(1) }))
const fieldTypes = readline().split`,`
const records = Array.from({ length: +readline() }).map(_ => readRecord())

records.sort((a, b) => {
    let result = 0
    for (let i = 0; i < orderFields.length && !result; ++i) {
        const { order, name } = orderFields[i]
        result = cmps [fieldTypes[i]] [order] (a[name], b[name])
    }
    return result
})

print(records.map(({ id }) => id).join`\n`)

