// https://www.codingame.com/ide/puzzle/van-ecks-sequence

let a = parseInt(readline());
let N = parseInt(readline());

const seen = new Map
let k = 0
while (--N) {
    let n
    if (!seen.has(a)) {
        n = 0
    } else {
        n = k - seen.get(a)
    }
    seen.set(a, k)
    a = n
    k++
}

print(a)
