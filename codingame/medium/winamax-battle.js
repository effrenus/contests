// https://www.codingame.com/ide/puzzle/winamax-battle

const stack1 = []
const stack2 = []

const PRIO = {2:1,3:2,4:3,5:4,6:5,7:6,8:7,9:8,10:9,J:10,Q:11,K:12,A:13}

let n =+readline()
for (let i = 0; i < n; i++) {
    const s = readline()
    stack1.push([s.slice(0, -1), s.slice(-1)])
}

const m = +readline()
for (let i = 0; i < m; i++) {
    const s = readline()
    stack2.push([s.slice(0, -1), s.slice(-1)])
}

let state = 'F' // F-fight, W-war
const l = []
const r = []

let winner = 'PAT'
let cnt = 0

while (true) {

    if (state == 'F') {
        if (stack1.length == 0) { winner = 2; break }
        if (stack2.length == 0) { winner = 1; break }
    }

    if (l.length > 3 && (stack1.length == 0 || stack2.length == 0)) {
        break
    }

    if (state == 'F') {
        if (l.length < 3) cnt++
        l.push(stack1.shift())
        r.push(stack2.shift())

        const d = PRIO[l[l.length-1][0]] - PRIO[r[r.length-1][0]]

        if (d == 0) {
            state = 'W'
            continue
        } else if (d > 0) {
            stack1.push(...l, ...r)
            l.length = 0
            r.length = 0
            state = 'F'
        } else {
            stack2.push(...l, ...r)
            l.length = 0
            r.length = 0
            state = 'F'
        }
    } else {
        if (stack1.length < 3 || stack2.length < 3) {
            break
        }
        state = 'F'

        l.push(...stack1.splice(0, 3))
        r.push(...stack2.splice(0, 3))
    }
}

print(winner == 'PAT' ? winner : `${winner} ${cnt}`)

