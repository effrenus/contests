// https://www.codingame.com/ide/puzzle/what-the-brainfuck

try {
    const { code, inp, memSize } = readData()
    
    validateSyntax(code)

    const mem = new Uint8Array(memSize)
    let ptr = 0
    let ip = 0

    const out = []
    while (ip < code.length) {
        switch (code[ip]) {
            case 62/* > */: ptr++; break;
            case 60/* < */: ptr--; break;
            case 43/* + */: mem[ptr]++; break;
            case 45/* - */: mem[ptr]--; break;
            case 46/* . */: out.push(String.fromCharCode(mem[ptr])); break;
            case 44/* , */: mem[ptr] = inp.shift(); break;
            case 91/* [ */: if (mem[ptr] === 0) ip = matchPosFor({ a: 91, b: 93, dir: 1, ip, code }); break;
            case 93/* ] */: if (mem[ptr] !== 0) ip = matchPosFor({ a: 93, b: 91, dir: -1, ip, code }); break;
        }
        if (ptr >= mem.length || ptr < 0) throw new Error('POINTER OUT OF BOUNDS')
        if (mem[ptr] < 0 || mem[ptr] >= 255) throw new Error('INCORRECT VALUE')
        ip++
    }
    print(out.join``)
} catch (err) {
    print(err.message)
}

function readData() {
    let [L, S, I] = readline().split` `.map(Number)

    let code = ''
    while (L--) {
        code += readline()
    }
    code = code.replace(/[^><+-.,\[\]]/g, '')

    const inp = []
    while (I--) inp.push(Number(readline()))

    return { code: Buffer.from(code), inp, memSize: S }
}

function matchPosFor({ a, b, dir, ip, code }) {
    if (!matchPosFor.memo) matchPosFor.memo = new Map

    const k = `${a}${b}${ip}`
    if (matchPosFor.memo.has(k)) matchPosFor.memo.get(k) // Simple memo

    let mismatchedCnt = 1
    let pos = ip + dir
    while (mismatchedCnt) {
        if (code[pos] == a) mismatchedCnt++
        if (code[pos] == b) mismatchedCnt--
        pos += dir   
    }

    matchPosFor.memo.set(k, pos - dir)
    
    return pos - dir
}

function validateSyntax(code) {
    let opened = 0
    for (const ch of code) {
        opened += ch == 91 ? 1 : ch == 93 ? -1 : 0
        if (opened < 0) throw new Error('SYNTAX ERROR')
    }
    if (opened) throw new Error('SYNTAX ERROR')
}

