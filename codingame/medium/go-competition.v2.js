// https://www.codingame.com/ide/puzzle/go-competition

const L = +readline()
let W = 6.5
let B = 0

const mat = []
for (let i = 0; i < L; i++) {
    mat.push(readline().split``)
}

for (let r = 0; r < mat.length; ++r) {
    for (let c = 0; c < mat[0].length; ++c) {
        if (mat[r][c] == '.') mark([r, c])
    }
}

const flatBoard = mat.map(r => r.join``).join``
W += flatBoard.replace(/[^W]/g, '').length
B += flatBoard.replace(/[^B]/g, '').length

print(`BLACK : ${B}
WHITE : ${W}
${W>B?'WHITE':'BLACK'} WINS`)

function mark(pos) {
    const q = [pos]
    const ps = []
    let boundSym = null
    while (q.length) {
        const [r, c] = q.shift()

        if (mat[r][c] != '.') continue
        mat[r][c] = '_'
        ps.push([r,c])

        for (const [dr, dc] of [[-1,0], [1,0], [0,-1], [0,1]]) {
            const [nr, nc] = [r+dr, c+dc]
            if (nr < 0 || nr >= mat.length || nc < 0 || nc >= mat[0].length) continue
            if (mat[nr][nc] == 'B' || mat[nr][nc] == 'W') {
                boundSym = !boundSym ? mat[nr][nc] : (mat[nr][nc] != boundSym ? '$' : boundSym)
                continue
            }
            q.push([nr, nc])
        }
    }
    for (const [r, c] of ps) mat[r][c] = boundSym
}

