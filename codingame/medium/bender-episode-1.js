// https://www.codingame.com/ide/puzzle/bender-episode-1

// Implementation

let [L, C] = readline().split` `.map(Number)

let mat = []
while (L--) mat.push(readline().split``.slice(1, -1))
mat = mat.slice(1, -1)

const rows = mat.length
const cols = mat[0].length

const MODES = {NORMAL: 1, BREAKER: 2}
const DIRS = [[1,0,'SOUTH'],[0,1,'EAST'],[-1,0,'NORTH'],[0,-1,'WEST']] // SOUTH, EAST, NORTH, WEST

let mode = MODES.NORMAL
let dirT = 1 // INVERSE: -1
let dir = 0

let pos
let teleports = []
for (let r = 0; r < mat.length; ++r) {
    for (let c = 0; c < mat[0].length; ++c) {
        const ch = mat[r][c]
        switch (ch) {
            case '@': pos = [r, c]; break
            case 'T': teleports.push([r, c]); break
        }
    }
}

let isTeleported = false
const visited = new Map
let logs = []

while (1) {
    const [r, c] = pos

    if (mat[r][c] == '$') break

    const k = `${r},${c},${dir},${dirT}`
    if (visited.has(k) && visited.get(k) > 3) {
        logs = ['LOOP']
        break
    }

    visited.set(k, (visited.get(k)||0)+1)

    const ch = mat[r][c]

    switch (ch) {
        case 'I': dirT *= -1; break
        case 'S': dir = 0; break
        case 'E': dir = 1; break
        case 'N': dir = 2; break
        case 'W': dir = 3; break
        case 'B': mode = mode == MODES.BREAKER ? MODES.NORMAL : MODES.BREAKER; break
        case 'T':
            if (isTeleported) {
                isTeleported = false
                break
            }
            isTeleported = true
            pos = teleports[(teleports.findIndex(([rr, cc])=>rr==r&&cc==c)+1)%2]
            continue
    }

    let isTry = false
    while (true) {
        const [nr, nc] = [r+DIRS[dir][0], c+DIRS[dir][1]]

        if (
            nr < 0 || nr >= rows ||
            nc < 0 || nc >= cols ||
            mat[nr][nc] == '#' ||
            (mat[nr][nc] == 'X' && mode != MODES.BREAKER)
        ) {
            dir = !isTry ? (dirT > 0 ? 0 : DIRS.length-1) : (dir+dirT < 0 ? DIRS.length-1 : (dir+dirT)%DIRS.length)
            if (!isTry) isTry = true
            continue
        }

        if (mat[nr][nc] == 'X') mat[nr][nc] = ' '

        pos = [nr, nc]
        logs.push(DIRS[dir][2])
        break
    }
}

print(logs.join`\n`)
