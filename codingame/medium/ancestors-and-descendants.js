// https://www.codingame.com/ide/puzzle/ancestors-&-descendants

let N = +readline()

const tree = []
while (N--) {
    const rel = readline()

    const name = rel.replace(/\./g, '')
    const depth = rel.length - name.length

    if (tree.length && tree[tree.length-1].depth >= depth) {
        printTree()
        while (tree.length && tree[tree.length-1].depth >= depth) tree.pop()
    }

    tree.push({ name, depth })
}

if (tree.length) printTree()

function printTree() {
    print(tree.map(({ name }) => name).join(' > '))
}

