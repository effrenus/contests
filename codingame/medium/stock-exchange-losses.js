// https://www.codingame.com/ide/puzzle/stock-exchange-losses

// Pre-processing

+readline()
const prices = readline().split` `.map(Number)
const minPrices = []

for (let i = prices.length-1; i >= 0; --i) {
    minPrices[i] = i == prices.length-1 ? prices[i] : Math.min(prices[i], minPrices[i+1])
}

const maxLoss = Math.max(...prices.map((v, i) => i==prices.length-1 ? -Infinity : prices[i]-minPrices[i+1]), 0)

print(maxLoss <= 0 ? 0 : -maxLoss)

