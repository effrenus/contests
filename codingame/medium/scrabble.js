// https://www.codingame.com/ide/puzzle/scrabble

// Could be shorter

let n = +readline()
const scores = []

const sc = {
    'e, a, i, o, n, r, t, l, s, u': 1,
    'd, g':2,
    'b, c, m, p':3,
    'f, h, v, w, y':4,
    'k':5,
    'j, x':6,
    'q, z':7
}

for (const [ks, v] of Object.entries(sc))
    for (const k of ks.split`, `) scores[k.charCodeAt()-97] = v

const rootTrie = []

let i = 1
function add(trie, word, score) {
    if (!word) {
        trie.isWord = true
        trie.score = score
        trie.order = i++
        return
    }
    const k = word[0].charCodeAt()-97
    if (!trie[k]) trie[k] = []
    add(trie[k], word.slice(1), score + scores[k])
}

while (n--) add(rootTrie, readline(), 0)

const avail = new Map
for (const ch of readline()) avail.set(ch, (avail.get(ch)||0)+1)
const chars = [...avail.keys()]

let max = -Infinity
let order = Infinity
let r = ''

function find(trie, w='') {
    if (trie.isWord) {
        if (trie.score > max || (trie.score == max && trie.order < order)) {
            max = trie.score
            order = trie.order
            r = w
        }
    }

    for (const ch of chars) {
        if (avail.get(ch) == 0) continue

        const k = ch.charCodeAt() - 97
        if (!trie[k]) continue
        
        avail.set(ch, avail.get(ch)-1)
        find(trie[k], w + ch)
        avail.set(ch, avail.get(ch)+1)
    }
}

find(rootTrie)

print(r)
