// https://www.codingame.com/ide/puzzle/forest-fire

// Messy, not optimal solution
// TODO: Redo maybe?

const L = +readline()
let water = +readline()

const map = Array.from({length: L}).map(_ => Array(L).fill(0))
let fires = []

// C=Canadair (3x3/2100), H=Helicopter (2x2/1200), J=SmokeJumpers (1/600)
const findCost = ([x,y], limit, waterNeed, type) => {
    let cnt = 0
    for (let i = 0; i < limit; ++i) {
        for (let j = 0; j < limit; ++j) {
            if (y+i >= L || x+j >= L) return false
            if (map[y+i][x+j] == 0) continue
            cnt++
        }
    }
    return [L>6 ? cnt*cnt/(limit*limit)*waterNeed/(limit*limit) : -cnt, [x,y], type, waterNeed, limit]
}
const costForJ = pos => findCost(pos, 1, 600, 'J')
const costForH = pos => findCost(pos, 2, 1200, 'H')
const costForC = pos => findCost(pos, 3, 2100, 'C')

const extinguish = ([x,y], type) => {
    const limit = type == 'J' ? 1 : (type == 'H' ? 2 : 3)
    for (let i = 0; i < limit; ++i) {
        for (let j = 0; j < limit; ++j) {
            if (y+i >= L || x+j >= L) continue
            map[y+i][x+j] = 0
            fires = fires.filter(([ax, ay]) => ax != x+j || ay != y+i)
        }
    }
}

while (true) {
    let N = +readline()
    while (N--) {
        const [x, y] = readline().split` `.map(Number)

        map[y][x] = 1
        fires.push([x, y])
    }

    let costs = []
    for (let y = 0; y < L; ++y) {
        for (let x = 0; x < L; ++x) {
            costs.push(costForJ([x,y]), costForH([x,y]), costForC([x,y]))
        }
    }
    
    costs = costs.filter(v => v ? !!v[0] : v)
    
    if (!costs.length) throw Error('Empty')

    costs.sort((a, b) => L>6?b[0]-a[0]:(a[0]==b[0] ? a[3]-b[3] : a[0]-b[0]))

    const [p, pos, type, ww] = (costs[0] || [])
    extinguish(pos, type)
    water -= ww

    print(`${type} ${pos[0]} ${pos[1]}`);
}

