// https://www.codingame.com/ide/puzzle/target-firing

type Enemy = {
    type: 'FIGHTER' | 'CRUISER'
    hp: number
    armor: number
    damage: number
}

const fleet: Enemy[] = Array(+readline()).fill(null)
    .map(_ => readline().split(' ') as [string, string, string, string])
    .map(([type, hp, armor, damage]) => ({type, hp: +hp, armor: +armor, damage: +damage} as Enemy))

let HP = 5000

while (HP > 0 && fleet.length) {
    HP -= fleet.reduce((acc, enemy) => acc + enemy.damage, 0)

    fleet.sort((a,b) => getScore(b) - getScore(a))
    
    const nextTarget = fleet[0]
    nextTarget.hp -= damageToEnemy(nextTarget)
    
    if(nextTarget.hp <= 0) fleet.shift()
}

console.log(HP < 0 ? 'FLEE' : HP)

function getScore(enemy: Enemy): number {
    let {hp, damage} = enemy
    let turnsToKill = 0
    while (hp > 0) {
        turnsToKill++
        hp -= damageToEnemy(enemy)
    }
    return damage/turnsToKill
}

function damageToEnemy(enemy: Enemy): number {
    return Math.max(1, (enemy.type == 'FIGHTER' ? 20 : 10) - enemy.armor)
}

