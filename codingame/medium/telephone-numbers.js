// https://www.codingame.com/ide/puzzle/telephone-numbers

const N = +readline()

const prefix = new Map
let cnt = 0
for (let i = 0; i < N; i++) {
    const digits = readline()
    for(let i = digits.length; i > 0; --i) {
        if (prefix.has(digits.slice(0, i))) break
        cnt++
        prefix.set(digits.slice(0, i), true)
    }
}

print(cnt)

