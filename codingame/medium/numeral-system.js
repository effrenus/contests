// https://www.codingame.com/ide/puzzle/numeral-system

const al = '0123456789abcdefghijklmnopqrstuvwxyz'
const eq = readline().toLowerCase()

const [operads, result] = eq.split`=`
const [l, r] = operads.split`+`

const convertToDec = (val, base) => val.split``.reduceRight((acc,c,i) => acc + al.indexOf(c) * base**(val.length-1-i), 0)

for (let i = 2; i <= 36; ++i) {
    if (
        !eq.split``.every(ch => al.indexOf(ch)+1 <= i) ||
        convertToDec(l,i) + convertToDec(r,i) != convertToDec(result,i)
    ) continue

    print(i)

    break
}

