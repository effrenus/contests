// https://www.codingame.com/ide/puzzle/conway-sequence

// Compact array, simulation

const R = +readline()
let L = +readline()

let seq = [[1, R]]

while (--L) {
    const nseq = []

    for (const [cnt, n] of seq) {
        if (cnt == n) {
            nseq.push([2, n])
        } else {
            if (nseq.length && nseq[nseq.length-1][1] == cnt) {
                nseq[nseq.length-1][0] += 1
                nseq.push([1, n])
            } else {
                nseq.push([1, cnt])
                nseq.push([1, n])
            }
        }
    }

    seq = nseq
}

print(seq.reduce((s, [cnt, n]) => s + (n+' ').repeat(cnt), '').trim())

