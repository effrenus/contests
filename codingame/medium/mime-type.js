// https://www.codingame.com/ide/puzzle/mime-type

const N = parseInt(readline())
const Q = parseInt(readline())

const tbl = new Map

for (let i = 0; i < N; i++) {
    const [ext, type] = readline().split` `
    tbl.set(ext.toLowerCase(), type)
}

for (let i = 0; i < Q; i++) {
    const fileParts = readline().toLowerCase().split`.`
    const ext = [
        tbl.get(fileParts[fileParts.length-1].toLowerCase()),
        tbl.get(fileParts[fileParts.length-1].toUpperCase())
    ].filter(Boolean)


    print(fileParts.length <= 1 || !ext.length  ? 'UNKNOWN' : ext.pop())
}
