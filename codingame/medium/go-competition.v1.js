// https://www.codingame.com/ide/puzzle/go-competition

const L = +readline()
let W = 6.5
let B = 0

const lines = []
for (let i = 0; i < L; i++) {
    lines.push(readline())
}

let prevLine = ''
for (let i = 0; i < L; i++) {
    let r = lines[i]
        .replace(/W(\.+)B/g, (_,d) => 'W'+d.replace(/\./g, '_')+'B')
        .replace(/B(\.+)W/g, (_,d) => 'B'+d.replace(/\./g, '_')+'W')
        .replace(/(\.+)([WB])/g, (_,d,s) => d.replace(/\./g, s)+s)
        .replace(/([WB])(\.+)/g, (_,s,d) => d.replace(/\./g, s)+s)
    
    if (/^\.+$/.test(r)) {
        if (!prevLine) {
            lines[i] = r.replace(/\./g, '$')
            continue
        }
        else if (prevLine.split``.every(ch => ch == 'B')) r = r.replace(/\./g, 'B')
        else if (prevLine.split``.every(ch => ch == 'W')) r = r.replace(/\./g, 'W')
        else r = r.replace(/\./g, '_')
    }

    for (const ch of r) if (ch == 'W') W++; else if (ch == 'B') B++
    prevLine = r
    lines[i] = r
}

// printErr(lines)

for (let i = 0; i < L; i++) {
    let cnt = 0
    if (lines[i][0] == '$') while (lines[i][0] == '$') {
        cnt += lines[i].length
        i++
    }
    if (lines[i].split``.every(ch => ch == 'B')) B += cnt
    if (lines[i].split``.every(ch => ch == 'W')) W += cnt
}

print(`BLACK : ${B}
WHITE : ${W}
${W>B?'WHITE':'BLACK'} WINS`)

