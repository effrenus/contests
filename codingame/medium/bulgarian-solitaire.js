// https://www.codingame.com/ide/puzzle/bulgarian-solitaire

readline()
let piles = readline().split` `.flatMap(v => +v?+v:[]).sort((a,b) => a-b)

const seen = {}
let i = 0
while (!seen[piles]) {
    seen[piles] = i++
    piles = piles.flatMap(v => --v?v:[]).concat(piles.length).sort((a,b) => a-b)
}
print(i - seen[piles])

