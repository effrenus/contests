const input = require('../read-input');

const OPCODE_TYPES = {
    SUM: 1,
    MULT: 2,
    READ: 3,
    WRITE: 4,
    JMP_T: 5,
    JMP_F: 6,
    LT: 7,
    EQ: 8,
    HALT: 99
};

const INPUT_VAL = 5;

const parseCommandFromNumber = (command) => {
    command = String(command);
    const pos = command.length - 2;

    return {
        opcode: Number(command.substr(pos)),
        paramTypes: [
            Number(command[pos - 1] || 0),
            Number(command[pos - 2] || 0),
            Number(command[pos - 3] || 0),
        ]
    };
};

const evaluate = async (pos, mem) => {
    const { opcode, paramTypes } = parseCommandFromNumber(mem[pos]);

    const getVal = (pos, type) => {
        if (type === 0) {
            return mem[pos];
        } else {
            return pos;
        }
    }

    const saveVal = (val, pos) => {
        mem[pos] = val;
    }

    let val;
    switch (opcode) {
        case OPCODE_TYPES.SUM:
            val = getVal(mem[pos+1], paramTypes[0]) + getVal(mem[pos+2], paramTypes[1]);
            saveVal(val, mem[pos+3], paramTypes[2]);
            return pos + 4;

        case OPCODE_TYPES.MULT:
            val = getVal(mem[pos+1], paramTypes[0]) * getVal(mem[pos+2], paramTypes[1]);
            saveVal(val, mem[pos+3], paramTypes[2]);
            return pos + 4;

        case OPCODE_TYPES.WRITE:
            console.log(getVal(mem[pos+1], 0));
            return pos + 2;

        case OPCODE_TYPES.READ:
            val = INPUT_VAL;
            saveVal(val, mem[pos+1], 0);
            return pos + 2;
        
        case OPCODE_TYPES.JMP_T:
          if (getVal(mem[pos+1], paramTypes[0]) > 0) {
            return getVal(mem[pos+2], paramTypes[1]);
          } else {
            return pos + 3;
          }
        
        case OPCODE_TYPES.JMP_F:
          if (getVal(mem[pos+1], paramTypes[0]) === 0) {
            return getVal(mem[pos+2], paramTypes[1]);
          } else {
            return pos + 3;
          }
        
        case OPCODE_TYPES.LT:
          if (getVal(mem[pos+1], paramTypes[0]) < getVal(mem[pos+2], paramTypes[1])) {
            saveVal(1, mem[pos+3], paramTypes[3]);
          } else {
            saveVal(0, mem[pos+3], paramTypes[3]);
          }
          return pos + 4;
        
        case OPCODE_TYPES.EQ:
          if (getVal(mem[pos+1], paramTypes[0]) === getVal(mem[pos+2], paramTypes[1])) {
            saveVal(1, mem[pos+3], paramTypes[3]);
          } else {
            saveVal(0, mem[pos+3], paramTypes[3]);
          }
          return pos + 4;
    }

    throw new Error(`Unknown opcode: ${opcode}`);
}

async function part1(mem) {
    let pos = 0;

    while (mem[pos] !== OPCODE_TYPES.HALT) {
        pos = await evaluate(pos, mem);
    }
}

(async () => {
    const origArr = await input((str) => str.split(',').map(Number));

    await part1(origArr.slice());
})();