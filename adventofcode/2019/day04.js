function valid(val) {
    const nums = String(val).split('').map(v => Number(v));
    
    let count = 1;
    const m = {};
    for (let i = 1; i < nums.length; ++i) {
      if (nums[i] === nums[i-1]) {
        existEq = true;
        count += 1;
        continue;
      }
      
      if (nums[i] < nums[i-1]) return false;
      else {
        m[nums[i-1]] = count;
        count = 1;
      }
    }
    m[nums[nums.length-1]] = count;

    for (const k of Object.keys(m)) {
      if (m[k] === 2) return true;
    }
    
    return false;
  }
  
  
  function t() {
    let count = 0;
    for (let i = 307237; i <= 769058; ++i) {
      if (valid(i)) {
        count++;
      }
    }
    
    console.log(count)
  }
  
  t();