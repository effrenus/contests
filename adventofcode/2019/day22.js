const input = require('../read-input');

const CMDS = {
  incrN(len, n, watchedPos) {
    let i = 0;
    let pos = 0;
    let f = false;
    let cc = Math.ceil(len / n);
    while (i < len) {
      if (i + cc < watchedPos) {
        i += cc;
        pos = (pos + (cc * n)) % len;
        f++;
        continue;
      }
      if (!f) {
        const gg = Math.floor((watchedPos - i) / n);
        i += gg;
        pos += gg * n;
        f = true;
        continue;
      }
      if (i === watchedPos) {
        return pos;
      }
      pos = (pos + n) % len;
      i += 1;
    }

    throw new Error('incrN: skipped watchedPos');
  },
  cutN(len, n, watchedPos) {
    if (n < 0) {
      n = len + n;
    }
    return watchedPos < n ? len + watchedPos - n : watchedPos - n;
  },
  dealNewStack(len, val, watchedPos) {
    return len - watchedPos - 1;
  }
};

(async () => {
  const commands = await input((str) => {
    return str.split('\n').map(line => {
      let cmd, val;
      if (line.indexOf('deal with increment') > -1) {
        [cmd, val] = [CMDS.incrN, Number(line.split('deal with increment ').pop())];
      } else if (line.indexOf('deal into new stack') > -1) {
        [cmd, val] = [CMDS.dealNewStack]
      } else if (line.indexOf('cut') > -1) {
        [cmd, val] = [CMDS.cutN, Number(line.split('cut ').pop())];
      } else {
        throw new Error(`Unknown command: ${line}`);
      }

      return { cmd, val };
    });
  });

  const len = 119315717514047;
  console.log(commands.reduce(
    (watchedPos, act) => {
      console.log('step');
      return act.cmd(len, act.val, watchedPos)
    },
    2020
  ));
  // console.log(CMDS.incrN(20000, 65, 1001))
})();