const input = require('../read-input');

(async () => {
  const LAYER_W = 25;
  const LAYER_H = 6;

  const layers = await input((str) => {
    const len = LAYER_W * LAYER_H;
    const layerCount = str.length / len;

    const layers = [];
    let i = 0;

    while (i < layerCount) {
      const layer = str.substr(i*len, len);
      let j = 0;
      const arr = [];
      while (j < LAYER_H) {
        arr.push(layer.substr(j*LAYER_W, LAYER_W).split(''));
        j += 1;
      }
      layers.push(arr);
      i += 1;
    }

    return layers;
  });

  let minZero = Infinity;
  let checkVal = -1;

  for (const layer of layers) {
    const chArr = layer.reduce((acc, v) => acc.concat(v), []);
    let zCount = chArr.filter(ch => ch === '0').length;

    if (zCount < minZero) {
       minZero = zCount;
       checkVal = chArr.filter(ch => ch === '1').length * chArr.filter(ch => ch === '2').length
    }
  }

  console.log('Part 1:', checkVal);

  // Part 2
  const getPixel = ([row, col]) => {
    for (const layer of layers) {
      if (layer[row][col] === '2') continue;
      return layer[row][col];
    }

    return 0;
  }

  const image = Array.from({ length: 6 }).map(() => Array.from({ length: 25 }));

  console.log('Part 2:');
  for (let row = 0; row < image.length; row++) {
    for (let col = 0; col < image[0].length; col++) {
      image[row][col] = getPixel([row, col]);
      if (image[row][col] === '0') process.stdout.write('■');
      else process.stdout.write('□')
    }
    console.log('');
  }

})();