let f = `
..#.#
.#.##
...#.
...##
#.###`.split('\n').filter(Boolean).map(line => line.split('').map(ch => ch === '.' ? 0 : 1));
const layouts = new Map();

const rows = f.length;
const cols = f[0].length;

const pos = [[-1, 0], [0, 1], [1, 0], [0, -1]];

const isDead = (i, j) => f[i][j] === 0;
const toReborn = (i, j) => {
  let c = 0;
  for (const [dr, dc] of pos) {
    const rr = i + dr;
    const cc = j + dc;
    if (rr >= 0 && rr < rows && cc >= 0 && cc < cols && f[rr][cc] === 1) c += 1;
  }
  if (c === 1 || c === 2) return true;
  return false;
}
const mustDie = (i, j) => {
  let c = 0;
  for (const [dr, dc] of pos) {
    const rr = i + dr;
    const cc = j + dc;
    if (rr >= 0 && rr < rows && cc >= 0 && cc < cols && f[rr][cc] === 1) c += 1;
  }
  if (c !== 1) return true;
  return false;
}

layouts.set(f.map(r => r.join('')).join(''), true);

while (true) {
  let comb = '';
  const copy = f.map(r => r.slice());
  for (let i = 0; i < rows; ++i) {
    for (let j = 0; j < cols; ++j) {
      if (isDead(i, j) && toReborn(i, j)) copy[i][j] = 1;
      else if (!isDead(i,j) && mustDie(i, j)) copy[i][j] = 0;
      comb += copy[i][j];
    }
  }
  f = copy;
  if (layouts.has(comb)) {
    console.log(comb.split('').reduce((acc, v, i) => v === '0' ? acc : acc + Math.pow(2, i), 0));
    return;
  }
  layouts.set(comb, true);
}
