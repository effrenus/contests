const input = require('../read-input');

const gcd = (a, b) => {
  if (b === 0) return a; 
  return gcd(b, a % b);
} 

const calcEnergy = ({ pos, vel }) => (Math.abs(pos.x)+Math.abs(pos.y)+Math.abs(pos.z)) * (Math.abs(vel.x)+Math.abs(vel.y)+Math.abs(vel.z));

const possibleToSkip = (points) => {
  const len = points.length;

  let diffX = Infinity;
  let diffY = Infinity;
  let diffZ = Infinity;

  for (let i = 0; i < len; ++i) {
    const point = points[i];
    for (let j = 0; j < len; ++j) {
      if (i === j) continue;
      const cmpP = points[j];
      diffX = Math.min(diffX, Math.abs(point.pos.x - cmpP.pos.x));
      diffY = Math.min(diffY, Math.abs(point.pos.y - cmpP.pos.y));
      diffZ = Math.min(diffZ, Math.abs(point.pos.z - cmpP.pos.z));
    }
  }

  return Math.min(diffX, diffY, diffZ)
}

const update = (points) => {
  let len = points.length;

  for (let i = 0; i < len; ++i) {
    const point = points[i];
    for (let j = 0; j < len; ++j) {
      if (i === j) continue;
      const cmpP = points[j];
      point.vel.x += point.pos.x > cmpP.pos.x ? -1 : (point.pos.x === cmpP.pos.x ? 0 : 1);
      point.vel.y += point.pos.y > cmpP.pos.y ? -1 : (point.pos.y === cmpP.pos.y ? 0 : 1);
      point.vel.z += point.pos.z > cmpP.pos.z ? -1 : (point.pos.z === cmpP.pos.z ? 0 : 1);
    }
  }

  for (const point of points) {
    point.pos.x += point.vel.x;
    point.pos.y += point.vel.y;
    point.pos.z += point.vel.z;
  }
}

(async () => {
  const points = await input((str) => {
    // <x=3, y=2, z=-6>
    return str.split('\n').map(val => {
      const [, x, y, z] = val.match(/<x=(-?\d+), y=(-?\d+), z=(-?\d+)>/);
      return { pos: { x: +x, y: +y, z: +z }, vel: { x: 0, y: 0, z: 0 } };
    })
  });

  const makeCopy = () => points.map(val => ({ pos: {...val.pos}, vel: {...val.vel} }));

  const original = makeCopy();

  const isEq = (ps, f) => {
    let i = 0;
    for (const point of ps) {
      const cmpPos = original[i].pos;
      const { pos, vel } = point;
      const eq = pos[f] === cmpPos[f] && vel[f] === 0;
      if (!eq) return false;
      i += 1;
    }

    return true;
  };

  const step = (points, f) => {
    let steps = 0;
    do {
      steps += 1;
      update(points);
    } while (!isEq(points, f));

    return steps;
  }


  const cx = step(makeCopy(), 'x');
  const cy = step(makeCopy(), 'y');
  const cz = step(makeCopy(), 'z');

  const cxy = cx * cy / gcd(cx, cy);

  console.log(cxy * cz / gcd(cxy, cz));

  let energy = 0;
  for (const point of points) {
    energy += calcEnergy(point);
  }

  console.log(energy);
})();