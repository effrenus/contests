const input = require('../read-input');

(async () => {
  const points = await input((str) => {
    const points = [];
    
    str.split('\n').forEach((line, i) => 
      line.split('').forEach((val, j) => {
        if (val === '#') points.push([j, i]);
      })
    );

    return points;
  });

  let max = -Infinity;
  let maxPointIndex;
  for (let i = 0; i < points.length; ++i) {
    let c = 0;
    const found = new Map();

    for (let j = 0; j < points.length; ++j) {
      if (i === j) continue;
      const angle = Math.atan2(points[j][1] - points[i][1], points[j][0] - points[i][0]);
      if (found.has(angle)) continue;
      found.set(angle, true);
      c++;
    }

    if (c > max) {
      max = c;
      maxPointIndex = i;
    }
  }

  console.log('Part 1:', max, points[maxPointIndex]);

  const dist = (p) => Math.abs(p[1] - points[maxPointIndex][1]) + Math.abs(p[0] - points[maxPointIndex][0]);

  const angleMap = new Map;
  for (let j = 0; j < points.length; ++j) {
    if (maxPointIndex === j) continue;
    let angle = 180/Math.PI*Math.atan2(points[j][0] - points[maxPointIndex][0], points[maxPointIndex][1] - points[j][1]);
    if (angle < 0) angle += 360;
    if (!angleMap.has(angle)) angleMap.set(angle, []);
    angleMap.get(angle).push(points[j]);
    angleMap.get(angle).sort((a, b) => dist(a) - dist(b))
  }

  let steps = 200;
  const angles = Array.from(angleMap.keys()).sort((a, b) => a - b);
  let i = angles.indexOf(0);
  while (true) {
    const angle = angles[i];
    if (!angleMap.get(angle).length) {
      i = (i + 1) % angles.length;
      continue;
    }
    const p = angleMap.get(angle).shift();
    steps--;
    if (steps === 0) {
      console.log('Part 2:', p[0]*100+p[1], p);
      break;
    }
    i = (i + 1) % angles.length;
  }
})();