const input = require('../read-input');

async function part1(ingredients) {
  const freeRes = {};

  const computeOreCount = (ingredient, needCount) => {
    if (ingredient === 'ORE') {
      return needCount;
    }

    const production = ingredients[ingredient];
    if (freeRes[ingredient]) {
      needCount -= freeRes[ingredient];
    }
    const mult = Math.ceil(needCount / production.count);
    freeRes[ingredient] = (production.count * mult) - needCount;

    let oreCount = 0;
    for (const part of production.ingredients) {
      if (part.type === 'ORE') {
        oreCount += mult * part.count;
        continue;
      }

      oreCount += computeOreCount(part.type, mult * part.count);
    }

    return oreCount;
  }

  let prev = 6000000;
  let current = prev;
  const MAX = 1000000000000;
  while (MAX - computeOreCount('FUEL', current) > 0) {
    prev = current;
    current += 1;
  }
  console.log(prev);
}

(async () => {
    const ingredients = await input((str) => {
      return str.split('\n').reduce((acc, val) => {
        const [ingredients, type] = val.split('=>').map(v => v.trim());
        const [count, name] = type.split(' ');
        const ingredientsArr = ingredients.split(', ').map(val => {
          const [count, type] = val.split(' ');
          return { count, type };
        });

        acc[name] = {
          count,
          ingredients: ingredientsArr
        };

        return acc;
      }, {});
    });

    await part1(ingredients);

    // const ORE_SUPP = 1000000000000;
    // let count = Math.floor(1000000000000 / 301997);
    // count += Math.floor((124 * count + (ORE_SUPP - count * 301997)) / 301997);

    // console.log(count);
})();