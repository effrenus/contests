const input = require('../read-input');

const calc = (opcode, l, r) => {
    switch (opcode) {
        case 1:
            return l + r;
        case 2:
            return l * r;
    }

    throw new Error(`Unknown opcode: ${opcode}`);
}

function part1(arr) {
    arr[1] = 12;
    arr[2] = 2;

    let i = 0;
    while (true) {
        if (arr[i] === 99) {
            break;
        }

        const res = calc(arr[i], arr[arr[i+1]], arr[arr[i+2]]);
        arr[arr[i+3]] = res;

        i += 4;
    }

    console.log(arr[0]);
}

function part2(origArr) {
    const VAL = 19690720;

    const test = (v1, v2) => {
        const arr = origArr.slice();
        arr[1] = v1;
        arr[2] = v2;

        let i = 0;
        while (true) {
            if (arr[i] === 99) {
                break;
            }

            const res = calc(arr[i], arr[arr[i+1]], arr[arr[i+2]]);
            arr[arr[i+3]] = res;

            i += 4;
        }

        return arr[0] === VAL;
    }

    for (let i = 0; i < 100; ++i) {
        for (let j = 0; j < 100; ++j) {
            try {
                if (test(i, j)) {
                    console.log(100 * i + j);
                    return;
                }
            } catch (e) {}
        }
    }

    throw new Error('Can\'t find input params.')
}

(async () => {
    const origArr = await input((str) => str.split(',').map(Number));

    part1(origArr.slice());
    part2(origArr.slice());
});