const input = require('../read-input');

const OPCODE_TYPES = {
    SUM: 1,
    MULT: 2,
    READ: 3,
    WRITE: 4,
    JMP_T: 5,
    JMP_F: 6,
    LT: 7,
    EQ: 8,
    HALT: 99
};

const parseCommandFromNumber = (command) => {
    command = String(command);
    const pos = command.length - 2;

    return {
        opcode: Number(command.substr(pos)),
        paramTypes: [
            Number(command[pos - 1] || 0),
            Number(command[pos - 2] || 0),
            Number(command[pos - 3] || 0),
        ]
    };
};

const evaluate = (pos, inputs, mem, output) => {
    const { opcode, paramTypes } = parseCommandFromNumber(mem[pos]);

    const getVal = (pos, type) => {
        if (type === 0) {
            return mem[pos];
        } else {
            return pos;
        }
    }

    const saveVal = (val, pos) => {
        mem[pos] = val;
    }

    let val;
    switch (opcode) {
        case OPCODE_TYPES.SUM:
          val = getVal(mem[pos+1], paramTypes[0]) + getVal(mem[pos+2], paramTypes[1]);
          saveVal(val, mem[pos+3], paramTypes[2]);
          return pos + 4;

        case OPCODE_TYPES.MULT:
          val = getVal(mem[pos+1], paramTypes[0]) * getVal(mem[pos+2], paramTypes[1]);
          saveVal(val, mem[pos+3], paramTypes[2]);
          return pos + 4;

        case OPCODE_TYPES.WRITE:
          output.push(getVal(mem[pos+1], 0));
          return pos + 2;

        case OPCODE_TYPES.READ:
          val = inputs.shift();
          saveVal(val, mem[pos+1], 0);
          return pos + 2;
        
        case OPCODE_TYPES.JMP_T:
          if (getVal(mem[pos+1], paramTypes[0]) > 0) {
            return getVal(mem[pos+2], paramTypes[1]);
          } else {
            return pos + 3;
          }
        
        case OPCODE_TYPES.JMP_F:
          if (getVal(mem[pos+1], paramTypes[0]) === 0) {
            return getVal(mem[pos+2], paramTypes[1]);
          } else {
            return pos + 3;
          }
        
        case OPCODE_TYPES.LT:
          if (getVal(mem[pos+1], paramTypes[0]) < getVal(mem[pos+2], paramTypes[1])) {
            saveVal(1, mem[pos+3], paramTypes[3]);
          } else {
            saveVal(0, mem[pos+3], paramTypes[3]);
          }
          return pos + 4;
        
        case OPCODE_TYPES.EQ:
          if (getVal(mem[pos+1], paramTypes[0]) === getVal(mem[pos+2], paramTypes[1])) {
            saveVal(1, mem[pos+3], paramTypes[3]);
          } else {
            saveVal(0, mem[pos+3], paramTypes[3]);
          }
          return pos + 4;
    }

    throw new Error(`Unknown opcode: ${opcode}`);
}

function* run(mem, { phase, input }) {
  let pos = 0;
  const output = [];
  const inputs = [phase, input]

  while (mem[pos] !== OPCODE_TYPES.HALT) {
      while (!output.length && mem[pos] !== OPCODE_TYPES.HALT) {
        pos = evaluate(pos, inputs, mem, output);
      }
      inputs[0] = yield output.pop();
      output.length = 0;
  }
  yield -1;
}

function initThrusters(mem, phases) {
  let input = 0;
  const inited = {};
  while (true) {
    for (let i = 0; i < phases.length; i++) {
      const phase = phases[i];
      if (!inited[phase]) {
        inited[phase] = run(mem.slice(), { phase, input });
        const { value } = inited[phase].next();
        if (value === undefined) return 0;
        if (Number.isNaN(value)) {
          return 0;
        }
        input = value;
      } else {
        const { value } = inited[phase].next(input);
        if (value === undefined || value === -1) {
          return input;
        }
        input = value;
      }
    }
  }
}

function getAllPermutations(len, start = 0) {
  const res = [];

  const get = (arr) => {
    if (arr.length === len) {
      res.push(arr.slice());
      return;
    }

    for (let i = start; i < start + len; ++i) {
      if (arr.indexOf(i) > -1) continue;
      arr.push(i);
      get(arr);
      arr.pop();
    }
  }

  get([]);

  return res;
}

function part1(mem) {
  const perms = getAllPermutations(5);
  let max = -Infinity;

  for (const perm of perms) {
    let prevOutput = 0;
    const output = [];
    for (const phase of perm) {
      run([phase, prevOutput], mem.slice(), output);
      prevOutput = output.pop();
    }
    max = Math.max(max, prevOutput);
  }

  console.log(max);
}

function part2(mem) {
  const perms = getAllPermutations(5, 5);
  let max = -Infinity;

  for (const perm of perms) {
    max = Math.max(max, initThrusters(mem, perm));
  }

  console.log(max);
}

(async () => {
    const origArr = await input((str) => str.split(',').map(Number));

    // part1(origArr.slice());
    part2(origArr.slice());
})();