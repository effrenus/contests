const input = require('../read-input');

function part1(graph) {
  let sum = 0;
  const dfs = (n, l) => {
    sum += l;
    if (!graph[n]) return;
    for (const nn of graph[n]) {
      dfs(nn, l + 1);
    }
  }

  dfs("COM", 0);

  console.log(sum);
}

(async () => {
  const graph = await input((str) => {
    const rels = str.split('\n').map(v => v.split(')'));
    const edges = {};
    for(const [p, o] of rels) {
      if (!edges[p]) edges[p] = [];
      edges[p].push(o);
    }

    return edges;
  });

  console.log(graph)
  part1(graph);
})();