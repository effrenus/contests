const input = require('../read-input');

const OPCODE_TYPES = {
    SUM: 1,
    MULT: 2,
    READ: 3,
    WRITE: 4,
    JMP_T: 5,
    JMP_F: 6,
    LT: 7,
    EQ: 8,
    ADJ_BASE: 9,
    HALT: 99
};

const parseCommandFromNumber = (command) => {
    command = String(command);
    const pos = command.length - 2;

    return {
        opcode: Number(command.substr(pos)),
        paramTypes: [
            Number(command[pos - 1] || 0),
            Number(command[pos - 2] || 0),
            Number(command[pos - 3] || 0),
        ]
    };
};

const evaluate = (pos, mem, data, handlers) => {
    const { opcode, paramTypes } = parseCommandFromNumber(mem[pos]);

    const getVal = (pos, type) => {
        if (type === 0) {
            return mem[pos] || 0;
        } else if (type === 1) {
            return pos;
        } else {
            return mem[data.base + pos] || 0;
        }
    }

    const saveVal = (val, pos, type) => {
        if (type === 2) {
          mem[data.base + pos] = val;
        } else {
          mem[pos] = val;
        }
    }

    let val;
    switch (opcode) {
        case OPCODE_TYPES.SUM:
          val = getVal(mem[pos+1], paramTypes[0]) + getVal(mem[pos+2], paramTypes[1]);
          saveVal(val, mem[pos+3], paramTypes[2]);
          return pos + 4;

        case OPCODE_TYPES.MULT:
          val = getVal(mem[pos+1], paramTypes[0]) * getVal(mem[pos+2], paramTypes[1]);
          saveVal(val, mem[pos+3], paramTypes[2]);
          return pos + 4;

        case OPCODE_TYPES.WRITE:
          handlers.onWrite(getVal(mem[pos+1], paramTypes[0]));
          return pos + 2;

        case OPCODE_TYPES.READ:
          val = handlers.onInput();
          saveVal(val, mem[pos+1], paramTypes[0]);
          return pos + 2;
        
        case OPCODE_TYPES.JMP_T:
          if (getVal(mem[pos+1], paramTypes[0]) > 0) {
            return getVal(mem[pos+2], paramTypes[1]);
          } else {
            return pos + 3;
          }
        
        case OPCODE_TYPES.JMP_F:
          if (getVal(mem[pos+1], paramTypes[0]) === 0) {
            return getVal(mem[pos+2], paramTypes[1]);
          } else {
            return pos + 3;
          }
        
        case OPCODE_TYPES.LT:
          if (getVal(mem[pos+1], paramTypes[0]) < getVal(mem[pos+2], paramTypes[1])) {
            saveVal(1, mem[pos+3], paramTypes[3]);
          } else {
            saveVal(0, mem[pos+3], paramTypes[3]);
          }
          return pos + 4;
        
        case OPCODE_TYPES.EQ:
          if (getVal(mem[pos+1], paramTypes[0]) === getVal(mem[pos+2], paramTypes[1])) {
            saveVal(1, mem[pos+3], paramTypes[3]);
          } else {
            saveVal(0, mem[pos+3], paramTypes[3]);
          }
          return pos + 4;
        
        case OPCODE_TYPES.ADJ_BASE:
          data.base += getVal(mem[pos+1], paramTypes[0]);
          return pos + 2;
    }

    throw new Error(`Unknown opcode: ${opcode}`);
}

async function run(mem) {
  let pos = 0;
  const data = {
    base: 0
  };
  const ps = [[0, 1], [-1, 0], [0, -1], [1, 0]];

  const state = {
    pos: [0, 0],
    dir: 0,
    action: 'paint'
  };
  const paintMap = {};
  while (mem[pos] !== OPCODE_TYPES.HALT) {
      pos = evaluate(pos, mem, data, {
        onInput: () => {
          return paintMap[state.pos.join(',')] || 0;
        },
        onWrite: (val) => {
          if (state.action === 'paint') {
            paintMap[state.pos.join(',')] = val;
            state.action = 'move';
          } else {
            if (val === 0) {
              state.dir = (state.dir + 1) % ps.length;
            } else {
              state.dir = state.dir > 0 ? state.dir - 1 : ps.length - 1;
            }
            const dir = ps[state.dir];
            state.pos = [state.pos[0]+dir[0], state.pos[1]+dir[1]];
            state.action = 'paint';
          }
        }
      });
  }
  console.log(Object.keys(paintMap).length);
}

async function part1(mem) {
  run(mem.slice());
}

(async () => {
    const origArr = await input((str) => str.split(',').map(Number));

    await part1(origArr.slice());
})();