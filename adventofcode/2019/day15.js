const input = require('../read-input');

const OPCODE_TYPES = {
    SUM: 1,
    MULT: 2,
    READ: 3,
    WRITE: 4,
    JMP_T: 5,
    JMP_F: 6,
    LT: 7,
    EQ: 8,
    ADJ_BASE: 9,
    HALT: 99
};

const parseCommandFromNumber = (command) => {
    command = String(command);
    const pos = command.length - 2;

    return {
        opcode: Number(command.substr(pos)),
        paramTypes: [
            Number(command[pos - 1] || 0),
            Number(command[pos - 2] || 0),
            Number(command[pos - 3] || 0),
        ]
    };
};

const evaluate = (pos, mem, data, handlers) => {
    const { opcode, paramTypes } = parseCommandFromNumber(mem[pos]);

    const getVal = (pos, type) => {
        if (type === 0) {
            return mem[pos] || 0;
        } else if (type === 1) {
            return pos;
        } else {
            return mem[data.base + pos] || 0;
        }
    }

    const saveVal = (val, pos, type) => {
        if (type === 2) {
          mem[data.base + pos] = val;
        } else {
          mem[pos] = val;
        }
    }

    let val;
    switch (opcode) {
        case OPCODE_TYPES.SUM:
          val = getVal(mem[pos+1], paramTypes[0]) + getVal(mem[pos+2], paramTypes[1]);
          saveVal(val, mem[pos+3], paramTypes[2]);
          return pos + 4;

        case OPCODE_TYPES.MULT:
          val = getVal(mem[pos+1], paramTypes[0]) * getVal(mem[pos+2], paramTypes[1]);
          saveVal(val, mem[pos+3], paramTypes[2]);
          return pos + 4;

        case OPCODE_TYPES.WRITE:
          handlers.onWrite(getVal(mem[pos+1], paramTypes[0]));
          return pos + 2;

        case OPCODE_TYPES.READ:
          val = handlers.onInput();
          saveVal(val, mem[pos+1], paramTypes[0]);
          return pos + 2;
        
        case OPCODE_TYPES.JMP_T:
          if (getVal(mem[pos+1], paramTypes[0]) > 0) {
            return getVal(mem[pos+2], paramTypes[1]);
          } else {
            return pos + 3;
          }
        
        case OPCODE_TYPES.JMP_F:
          if (getVal(mem[pos+1], paramTypes[0]) === 0) {
            return getVal(mem[pos+2], paramTypes[1]);
          } else {
            return pos + 3;
          }
        
        case OPCODE_TYPES.LT:
          if (getVal(mem[pos+1], paramTypes[0]) < getVal(mem[pos+2], paramTypes[1])) {
            saveVal(1, mem[pos+3], paramTypes[3]);
          } else {
            saveVal(0, mem[pos+3], paramTypes[3]);
          }
          return pos + 4;
        
        case OPCODE_TYPES.EQ:
          if (getVal(mem[pos+1], paramTypes[0]) === getVal(mem[pos+2], paramTypes[1])) {
            saveVal(1, mem[pos+3], paramTypes[3]);
          } else {
            saveVal(0, mem[pos+3], paramTypes[3]);
          }
          return pos + 4;
        
        case OPCODE_TYPES.ADJ_BASE:
          data.base += getVal(mem[pos+1], paramTypes[0]);
          return pos + 2;
    }

    throw new Error(`Unknown opcode: ${opcode}`);
}

function run({ mem, dir, data, pos }) {
  return new Promise(resolve => {
    let isOut = false;
    while (!isOut) {
        pos = evaluate(pos, mem, data, {
          onInput: () => {
            return dir;
          },
          onWrite: (val) => {
            resolve({ status: val, pos });
            isOut = true;
          }
        });
    }
  });
}

async function part1(mem) {
  const queue = [];
  const data = { base: 0 };

  for (let i = 1; i < 5; ++i) {
    queue.push({ pos: 0, coord: [0, 0], memo: mem.slice(), steps: 1, dir: i });
  }

  const visited = {};

  while (queue.length) {
      let { pos, coord, steps, memo, dir } = queue.shift();

      if (visited[coord.join(',')+','+dir]) continue;

      visited[coord.join(',')] = 1;

      const res = await run({ mem: memo, data, dir, pos });

      if (res.status === 0) {
        console.log('wall')
        continue; // wall
      }
      if (res.status === 2) {
        console.log(steps);
        return;
      }

      const dirs = [[-1, 0], [1, 0], [0, -1], [0, 1]];
      for (let i = 0; i < 4; ++i) {
        const cc = [coord[0]+dirs[i][0], coord[1]+dirs[i][1]]
        queue.push({ pos: res.pos, coord: cc, memo: memo.slice(), steps: steps + 1, dir: i })
      }
  }
}

(async () => {
    const origArr = await input((str) => str.split(',').map(Number));

    await part1(origArr.slice());
})();