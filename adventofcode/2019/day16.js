const input = require('../read-input');

function * gen(repeatCount = 1) {
  const vals = [0, 1, 0, -1];
  let pos = 0;
  let step = 1;
  
  while (true) {
    while (step++ < repeatCount) {
      yield vals[pos];
    }
    step = 0;
    pos = (pos + 1) % vals.length;
  }
}

(async () => {
  const originalNums = await input((str) => str);

  const len = nums.length;

  // Part 1.
  let iter = 1;
  let nums = originalNums;

  while (iter <= 100) {
    let i = 1;
    let next = '';

    while (i <= len) {
      let sum = 0;
      const g = gen(i);
      for (const ch of nums) {
        const { value } = g.next();
        sum += value*Number(ch);
      }
      i += 1;
      next += Math.abs(sum % 10);
    }
    
    nums = next;
    iter += 1;
  }

  console.log(nums);

  // Part 2.
  const offset = parseInt(nums.substr(0, 7), 10);
  nums = originalNums.repeat(10000).substr(offset);

  for (let iter = 0; iter < 100; ++iter) {
    let sum = 0;
    let nn = '';
    for (let i = nums.length - 1; i >= 0; --i) {
      sum += Number(nums[i]);
      nn = (sum % 10) + nn;
    }
    nums = nn;
  }

  console.log(nums.substr(0, 8));
})();
