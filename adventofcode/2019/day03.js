const readInput = require('../read-input');

const ORIENT_TYPE = {
    HORIZONTAL: 1,
    VERTICAL: 2,
};

const DIRECTION = {
    U: 1,
    R: 2,
    D: 3,
    L: 4
};

function prepare(cmds) {
    let prevPoint = [0, 0];
    const lines = [];

    for (let command of cmds) {
        const cmd = command[0];
        const coord = Number(command.substr(1));

        let nextPoint;

        switch (cmd) {
            case 'R':
                nextPoint = [prevPoint[0] + coord, prevPoint[1]];
                lines.push([prevPoint, nextPoint, ORIENT_TYPE.HORIZONTAL, DIRECTION.R]);
                break;
            case 'U':
                nextPoint = [prevPoint[0], prevPoint[1] + coord];
                lines.push([prevPoint, nextPoint, ORIENT_TYPE.VERTICAL, DIRECTION.U]);
                break;
            case 'D':
                nextPoint = [prevPoint[0], prevPoint[1] - coord];
                lines.push([nextPoint, prevPoint, ORIENT_TYPE.VERTICAL, DIRECTION.D]);
                break;
            case 'L':
                nextPoint = [prevPoint[0] - coord, prevPoint[1]];
                lines.push([nextPoint, prevPoint, ORIENT_TYPE.HORIZONTAL, DIRECTION.L]);
                break;
        }
        prevPoint = nextPoint;
    }

    return lines;
}

function getDist([p1, p2, type, dir], toPoint) {
    if (!toPoint) {
        return (type === ORIENT_TYPE.HORIZONTAL ? Math.abs(p1[0] - p2[0]) : Math.abs(p1[1] - p2[1]));
    } else {
        return (type === ORIENT_TYPE.HORIZONTAL 
            ? Math.abs((dir === DIRECTION.R ? p1[0] : p2[0]) - toPoint[0]) 
            : Math.abs((dir === DIRECTION.U ? p1[1] : p2[1]) - toPoint[1]));
    }
}

function solution(lines1, lines2) {
    const intersections = [];
    const distances = [];

    let dist1 = 0;
    let dist2 = 0;
    for (const l1 of lines1) {
        dist2 = 0;

        for (const l2 of lines2) {
            if (l1[2] === l2[2]) {
                dist2 += getDist(l2);
                continue;
            }

            let point;

            if (l1[2] === ORIENT_TYPE.HORIZONTAL) {
                point = [l2[0][0], l1[0][1]];
            } else {
                point = [l1[0][0], l2[0][1]];
            }

            if (
                point[0] >= l1[0][0] && point[0] <= l1[1][0]
                && point[1] >= l1[0][1] && point[1] <= l1[1][1]
                && point[0] >= l2[0][0] && point[0] <= l2[1][0]
                && point[1] >= l2[0][1] && point[1] <= l2[1][1]
            ) {
                distances.push(dist1 + getDist(l1, point) + dist2 + getDist(l2, point));
                intersections.push(point);
            }
            dist2 += getDist(l2);
        }

        dist1 += getDist(l1);
    }

    intersections.shift();
    distances.shift();

    console.log('Part1:', Math.min(...intersections.map(p => Math.abs(p[0]) + Math.abs(p[1]))));

    console.log('Part2:', Math.min(...distances));
}

(async () => {
    const [wire1, wire2] = await readInput(str => {
        return str.split('\n').map(line => line.split(',')).map(prepare);
    });

    solution(wire1, wire2);
})();