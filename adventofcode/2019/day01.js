const getInput = require('../read-input');

const calcFuel = (mass) => {
    return Math.floor(mass / 3) - 2;
}

const calcFuelRec = (mass) => {
    let fuel = Math.floor(mass / 3) - 2;
    let totalFuel = 0;

    while (fuel > 0) {
        totalFuel += fuel;
        fuel = Math.floor(fuel / 3) - 2
    }

    return totalFuel;
}

function part1(masses) {
    const fuels = masses.map(calcFuel);

    console.log(fuels.reduce((acc, val) => acc + val, 0));
}

function part2(masses) {
    const fuels = masses.map(calcFuelRec);

    console.log(fuels.reduce((acc, val) => acc + val, 0));
}

(async function main(){
    const masses = await getInput((str) => str.split('\n').map(Number));

    part1(masses);
    part2(masses);
})();