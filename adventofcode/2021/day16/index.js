const read = require("../../read-input");

(async () => {
  const input = await read(
    (i) =>
      i.split("").map((r) => parseInt(r, 16).toString(2).padStart(4, "0"))
        .join``
  );

  const exec = (type, ...operands) => {
    switch (type) {
      case 0:
        return operands.reduce((acc, v) => acc + v);
      case 1:
        return operands.reduce((acc, v) => acc * v);
      case 2:
        return Math.min(...operands);
      case 3:
        return Math.max(...operands);
      case 5:
        return operands[0] > operands[1] ? 1 : 0;
      case 6:
        return operands[0] < operands[1] ? 1 : 0;
      case 7:
        return operands[0] == operands[1] ? 1 : 0;
    }
  };

  let pos = 0;
  let ans = 0;

  const read_packet = () => {
    const version = parseInt(input.slice(pos, (pos += 3)), 2);
    const id = parseInt(input.slice(pos, (pos += 3)), 2);

    if (pos >= input.length) throw new Error("Invalid packet");

    ans += version;

    switch (id) {
      case 4: {
        // literal
        let num_bits = "";

        while (true) {
          const bits = input.slice(pos, (pos += 5));
          num_bits += bits.slice(1);
          if (bits[0] === "0") break;
        }

        return parseInt(num_bits, 2);
      }
      default: {
        // operator
        const mode = input[pos++];
        const packets = [];

        if (mode === "0") {
          let len = parseInt(input.slice(pos, (pos += 15)), 2);
          const start = pos;
          while (pos < start + len) {
            packets.push(read_packet());
          }
        } else {
          let cnt = parseInt(input.slice(pos, (pos += 11)), 2);
          while (cnt--) {
            packets.push(read_packet());
          }
        }

        return exec(id, ...packets);
      }
    }
  };

  let ans2;

  while (pos < input.length) {
    try {
      ans2 = read_packet();
    } catch (_err) {}
  }

  console.log("Part1:", ans);
  console.log("Part2:", ans2);
})();
