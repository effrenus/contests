const read = require("../../read-input");

(async () => {
  const input = await read((i) =>
    i.split("\n").map((r) => r.split``.map(Number))
  );

  const m = input.length;
  const n = input[0].length;

  const q = [[0, [0, 0]]];
  const visited = {};
  while (q.length) {
    q.sort((a, b) => a[0] - b[0]);

    const [d, [r, c]] = q.shift();

    if (r === 5 * input.length - 1 && c === 5 * input[0].length - 1) {
      console.log(d);
      break;
    }

    if (visited[[r, c]]) continue;
    visited[[r, c]] = 1;

    for (const [dr, dc] of [
      [0, 1],
      [0, -1],
      [1, 0],
      [-1, 0],
    ]) {
      const [nr, nc] = [r + dr, c + dc];
      if (
        nr < 0 ||
        nr >= 5 * input.length ||
        nc < 0 ||
        nc >= 5 * input[0].length
      )
        continue;
      let cost =
        Math.floor(nr / input.length) +
        Math.floor(nc / input[0].length) +
        input[nr % m][nc % n];
      if (cost >= 10) cost = cost % 9;
      q.push([d + cost, [nr, nc]]);
    }
  }
})();
