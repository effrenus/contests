const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n").map((r) => JSON.parse(r)));
  const input2 = JSON.parse(JSON.stringify(input));

  const split = (pair) => {
    let splitOrder;
    let updateVals;
    let order = 0;

    const update = (pair) => {
      if (typeof pair === "number") {
        const ord = order++;
        if (ord === splitOrder - 1) return pair + updateVals[0];
        if (ord === splitOrder + 1) return pair + updateVals[1];
        return pair;
      }
      const [p1, p2] = pair;
      pair = [update(p1), update(p2)];
      return pair;
    };

    const walk = (pair, lvl = 0) => {
      if (typeof pair === "number") {
        order++;
        return pair;
      }

      if (lvl === 4 && !updateVals) {
        splitOrder = order++;
        updateVals = pair;
        return 0;
      }

      const [p1, p2] = pair;
      pair = [walk(p1, lvl + 1), walk(p2, lvl + 1)];

      return pair;
    };

    pair = walk(pair, splitOrder);
    if (updateVals) {
      order = 0;
      pair = update(pair);
    }

    return pair;
  };

  const explode = (pair) => {
    let isExploded = false;

    const walk = (pair) => {
      if (typeof pair === "number") {
        if (pair >= 10 && !isExploded) {
          isExploded = true;
          return [Math.floor(pair / 2), Math.ceil(pair / 2)];
        } else return pair;
      }

      pair = [walk(pair[0]), walk(pair[1])];

      return pair;
    };

    return walk(pair);
  };

  let val = input.shift();

  const magnitude = (node) => {
    if (typeof node === "number") return node;

    const m = magnitude(node[0]) * 3 + magnitude(node[1]) * 2;

    return m;
  };

  while (true) {
    let newVal = val;

    while (true) {
      do {
        val = newVal;
        newVal = split(val);
      } while (JSON.stringify(val) !== JSON.stringify(newVal));

      newVal = explode(val);

      if (JSON.stringify(val) === JSON.stringify(newVal)) break;

      val = newVal;
    }

    if (!input.length) break;

    val = [val, input.shift()];
  }

  console.log("Part1:", magnitude(val));

  let max = -Infinity;

  function sum(a, b) {
    const input = [b];
    let val = a;

    while (true) {
      let newVal = val;

      while (true) {
        do {
          val = newVal;
          newVal = split(val);
        } while (JSON.stringify(val) !== JSON.stringify(newVal));

        newVal = explode(val);

        if (JSON.stringify(val) === JSON.stringify(newVal)) break;

        val = newVal;
      }

      if (!input.length) break;

      val = [val, input.shift()];
    }
    return val;
  }

  for (const [i, a] of input2.entries()) {
    for (const b of input2.slice(i + 1)) {
      max = Math.max(magnitude(sum(a, b)), max);
      max = Math.max(magnitude(sum(b, a)), max);
    }
  }

  console.log("Part2:", max);
})();
