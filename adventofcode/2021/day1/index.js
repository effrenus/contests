const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n").map(parseFloat));

  let cnt = 0;
  for (let i = 1; i < input.length; i++) {
    if (input[i] > input[i - 1]) cnt++;
  }

  console.log(cnt);
})();

(async () => {
  const input = await read((i) => i.split("\n").map(parseFloat));

  let cnt = 0;
  let sum = input.slice(0, 3).reduce((acc, v) => acc + v);

  for (let i = 3; i < input.length; i++) {
    let nextSum = input[i] + sum - input[i - 3];

    if (nextSum > sum) cnt++;

    sum = nextSum;
  }

  console.log(cnt);
})();
