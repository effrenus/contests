const read = require("../../read-input");

(async () => {
  const input = await read((i) =>
    i.split("\n").map((l) => l.split``.map(Number))
  );

  let ans = 0;
  const points = [];

  for (let r = 0; r < input.length; r++) {
    for (let c = 0; c < input[0].length; c++) {
      let isOk = true;

      for (const [dr, dc] of [
        [0, 1],
        [0, -1],
        [1, 0],
        [-1, 0],
      ]) {
        const [nr, nc] = [r + dr, c + dc];
        if (nr < 0 || nr >= input.length || nc < 0 || nc >= input[0].length)
          continue;
        if (input[r][c] >= input[nr][nc]) {
          isOk = false;
          break;
        }
      }

      if (isOk) {
        points.push([r, c]);
        ans += input[r][c] + 1;
      }
    }
  }

  console.log("Part1:", ans);

  const bfs = ([r, c]) => {
    const q = [[r, c]];
    let size = 0;

    while (q.length) {
      const [r, c] = q.shift();

      if (input[r][c] === 9) continue;

      input[r][c] = 9;
      size += 1;

      for (const [dr, dc] of [
        [0, 1],
        [0, -1],
        [1, 0],
        [-1, 0],
      ]) {
        const [nr, nc] = [r + dr, c + dc];
        if (
          nr < 0 ||
          nr >= input.length ||
          nc < 0 ||
          nc >= input[0].length ||
          input[nr][nc] === 9 ||
          input[r][c] <= input[nr][nc]
        )
          continue;

        q.push([nr, nc]);
      }
    }

    return size;
  };

  let sizes = [];
  for (const p of points) sizes.push(bfs(p));

  console.log(
    "Part2",
    sizes
      .sort((a, b) => b - a)
      .slice(0, 3)
      .reduce((acc, v) => acc * v)
  );
})();
