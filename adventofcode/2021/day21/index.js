const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n"));
  let pos1 = input[0].split(" ").pop() - 1;
  let pos2 = input[1].split(" ").pop() - 1;

  const possVals = [
    [1, 1, 1],
    [1, 1, 2],
    [1, 1, 3],
    [1, 2, 1],
    [1, 2, 2],
    [1, 2, 3],
    [1, 3, 1],
    [1, 3, 2],
    [1, 3, 3],
    [2, 1, 1],
    [2, 1, 2],
    [2, 1, 3],
    [2, 2, 1],
    [2, 2, 2],
    [2, 2, 3],
    [2, 3, 1],
    [2, 3, 2],
    [2, 3, 3],
    [3, 1, 1],
    [3, 1, 2],
    [3, 1, 3],
    [3, 2, 1],
    [3, 2, 2],
    [3, 2, 3],
    [3, 3, 1],
    [3, 3, 2],
    [3, 3, 3],
  ];

  const memo = {};
  const findOutcomes = ({ pos1, pos2, score1, score2 }) => {
    // if (memo[[pos1, pos2, score1, score2]]) {
    //   return memo[[pos1, pos2, score1, score2]];
    // }

    const wins = [0, 0];

    for (const vals1 of possVals) {
      let nextPos1 = (pos1 + vals1[0]) % 10;
      nextPos1 = (nextPos1 + vals1[1]) % 10;
      nextPos1 = (nextPos1 + vals1[2]) % 10;
      nextScore1 = score1 + nextPos1 + 1;

      if (nextScore1 >= 21) {
        ww1 += 1;
        continue;
      }

      for (const vals2 of possVals) {
        let nextPos2 = (pos2 + vals2[0]) % 10;
        nextPos2 = (nextPos2 + vals2[1]) % 10;
        nextPos2 = (nextPos2 + vals2[2]) % 10;
        nextScore2 = score2 + nextPos2 + 1;

        if (nextScore2 >= 21) {
          ww2 += 1;
          continue;
        }

        findOutcomes({
          pos1: nextPos1,
          pos2: nextPos2,
          score1: nextScore1,
          score2: nextScore2,
        });
        // wins[0] += w1;
        // wins[1] += w2;
      }
    }

    // ww1 += wins[0];
    // ww2 += wins[1];

    // return (memo[[pos1, pos2, score1, score2]] = wins);
  };

  const dumb = ({ pos1, steps1, score1, pos2, steps2, score2 }) => {
    if (memo[[pos1, steps1, score1, pos2, steps2, score2]])
      return memo[[pos1, steps1, score1, pos2, steps2, score2]];

    if (steps1 === 0 && score1 >= 21) {
      return [1, 0];
    }
    if (steps2 === 0 && score2 >= 21) {
      return [0, 1];
    }

    let w1 = 0,
      w2 = 0;

    if (steps1 > 0) {
      for (const v of [1, 2, 3]) {
        const [ww1, ww2] = dumb({
          pos1: (pos1 + v) % 10,
          steps1: steps1 - 1,
          score1: steps1 === 1 ? score1 + ((pos1 + v) % 10) + 1 : score1,
          pos2,
          steps2,
          score2,
        });
        w1 += ww1;
        w2 += ww2;
      }
      return (memo[[pos1, steps1, score1, pos2, steps2, score2]] = [w1, w2]);
    } else if (steps2 > 0) {
      for (const v of [1, 2, 3]) {
        const [ww1, ww2] = dumb({
          pos1,
          steps1,
          score1,
          pos2: (pos2 + v) % 10,
          steps2: steps2 - 1,
          score2: steps2 === 1 ? score2 + ((pos2 + v) % 10) + 1 : score2,
        });
        w1 += ww1;
        w2 += ww2;
      }
      return (memo[[pos1, steps1, score1, pos2, steps2, score2]] = [w1, w2]);
    }

    return (memo[[pos1, steps1, score1, pos2, steps2, score2]] = dumb({
      pos1,
      steps1: 3,
      score1,
      pos2,
      steps2: 3,
      score2,
    }));
  };

  console.log(dumb({ pos1, pos2, score1: 0, score2: 0, steps1: 3, steps2: 3 }));
})();
