const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n"));

  let grid = [];
  let maxX = 0;
  let maxY = 0;
  for (let l of input) {
    if (l === "") break;
    const [x, y] = l.split(",").map(Number);
    if (!grid[y]) grid[y] = [];
    grid[y][x] = "#";
    maxX = Math.max(maxX, x);
    maxY = Math.max(maxY, y);
  }

  for (let r = 0; r <= maxY; r++)
    for (let c = 0; c <= maxX; c++) {
      if (!grid[r]) grid[r] = [];
      if (grid[r][c] !== "#") grid[r][c] = ".";
    }

  const print = () => console.log(grid.map((r) => r.join``).join`\n` + "\n");
  // print();

  const folds = [];
  for (const l of input) {
    if (!l.startsWith("fold alon")) continue;
    const [_, c, v] = l.match(/fold along (x|y)=(\d+)/);
    folds.push([c, v]);
  }

  const up = (y) => {
    let up = y - 1;
    let down = +y + 1;
    while (up >= 0 && down < grid.length) {
      for (let i = 0; i < grid[0].length; i++) {
        if (grid[up][i] !== "#") grid[up][i] = grid[down][i];
      }
      up--;
      down++;
    }
    grid = grid.slice(0, y);
  };

  const left = (x) => {
    let left = x - 1;
    let right = x + 1;
    while (left >= 0 && right < grid[0].length) {
      for (let r = 0; r < grid.length; r++) {
        if (grid[r][left] !== "#") grid[r][left] = grid[r][right];
      }
      left--;
      right++;
    }
    grid = grid.map((r) => r.slice(0, x));
  };

  for (const [f, coord] of folds) {
    if (f === "y") up(+coord);
    if (f === "x") left(+coord);
  }

  let cnt = 0;
  for (let r = 0; r < grid.length; r++)
    for (let c = 0; c < grid[0].length; c++) {
      if (grid[r][c] === "#") cnt++;
    }

  print();
})();
