const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n"));

  const stat = Array(input[0].length)
    .fill(null)
    .map((_) => [0, 0]);

  for (const r of input) {
    for (let i = 0; i < r.length; i++) {
      stat[i][r[i]]++;
    }
  }

  let gam = "";
  let eps = "";
  for (const [z, o] of stat)
    if (z > o) {
      gam += "0";
      eps += "1";
    } else {
      gam += "1";
      eps += "0";
    }

  console.log(parseInt(gam, 2) * parseInt(eps, 2));
})();

(async () => {
  const input = await read((i) => i.split("\n"));

  let stat;

  const updateStats = (inp) => {
    stat = Array(inp[0].length)
      .fill(null)
      .map((_) => [0, 0]);

    for (const r of inp) {
      for (let i = 0; i < r.length; i++) {
        stat[i][r[i]]++;
      }
    }
  };

  let filtered = input;
  let i = 0;
  while (i < input[0].length && filtered.length > 1) {
    updateStats(filtered);
    const cmp = stat[i][0] > stat[i][1] ? "0" : "1";
    filtered = filtered.filter((v) => v[i] === cmp);
    i++;
  }
  const oxy = parseInt(filtered[0], 2);

  filtered = input;
  i = 0;
  while (i < input[0].length && filtered.length > 1) {
    updateStats(filtered);
    const cmp = stat[i][1] >= stat[i][0] ? "0" : "1";
    filtered = filtered.filter((v) => v[i] === cmp);
    i++;
  }
  console.log(input.length);
  const co2 = parseInt(filtered[0], 2);

  console.log(oxy * co2);
})();
