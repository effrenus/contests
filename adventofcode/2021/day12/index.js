const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n").map((l) => l.split`-`));

  const g = {};
  for (const [a, b] of input) {
    if (!g[a]) g[a] = [];
    if (!g[b]) g[b] = [];
    g[a].push(b);
    g[b].push(a);
  }

  const paths = new Set();
  const dfs = (node, path, visited) => {
    if (node === "end") {
      paths.add(path);
      return;
    }

    for (const nn of g[node] || []) {
      if (
        nn === "start" ||
        (/^[a-z]+$/.test(nn) &&
          visited[nn] &&
          Object.entries(visited).filter(
            ([k, v]) => /^[a-z]+$/.test(k) && v > 1
          ).length)
      ) {
        continue;
      }
      visited[nn] = (visited[nn] || 0) + 1;
      dfs(nn, path + "," + nn, visited);
      visited[nn] -= 1;
    }
  };

  dfs("start", "start", { start: 1 });

  console.log(paths.size);
})();
