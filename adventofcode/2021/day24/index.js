const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n").map((l) => l.split` `));

  const coeffs = [];

  for (let i = 0; i < 14; i++) {
    const j = i * 18;
    coeffs.push(
      [input[j + 4][2], input[j + 5][2], input[j + 15][2]].map(Number)
    );
  }

  const memo = new Map();
  const ans = [];

  const solve = (step, z) => {
    if (step === 14) return z;

    const k = String([step, z]);

    if (memo.has(k)) return memo.get(k);

    for (let n = 9; n > 0; n--) {
      const x = (z % 26) + coeffs[step][1] === n ? 0 : 1;
      const nz =
        Math.floor(z / coeffs[step][0]) * (25 * x + 1) +
        (n + coeffs[step][2]) * x;

      const r = solve(step + 1, nz);

      memo.set(k, r);

      if (r === 0) {
        ans[step] = n;
        return r;
      }
    }

    return memo.get(k);
  };

  solve(0, 0);

  console.log("Part1:", ans.join``);
  console.log("Part2:", ans.join``); // Opposite for (n 1..9)
})();
