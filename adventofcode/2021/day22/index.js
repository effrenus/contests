const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n"));

  const switches = [];
  for (const line of input) {
    const [_, state, x1, x2, y1, y2, z1, z2] = line.match(
      /(on|off) x=(-?\d+)..(-?\d+),y=(-?\d+)..(-?\d+),z=(-?\d+)..(-?\d+)/
    );
    switches.push([state, [+x1, +x2], [+y1, +y2], [+z1, +z2]]);
  }

  const overlap = (a, b) => {
    const [[x, X], [y, Y], [z, Z]] = a;
    const [[u, U], [v, V], [w, W]] = b;
    return !(x > U || X < u || y > V || Y < v || z > W || Z < w);
  };

  const split = (a, b) => {
    const [[x, X], [y, Y], [z, Z]] = a;
    const [[u, U], [v, V], [w, W]] = b;

    if (!overlap(a, b)) return [b];

    const ans = [];
    if (x > u)
      ans.push([
        [u, x - 1],
        [v, V],
        [w, W],
      ]);
    if (X < U)
      ans.push([
        [X + 1, U],
        [v, V],
        [w, W],
      ]);
    if (y > v)
      ans.push([
        [Math.max(u, x), Math.min(U, X)],
        [v, y - 1],
        [w, W],
      ]);
    if (Y < V)
      ans.push([
        [Math.max(u, x), Math.min(U, X)],
        [Y + 1, V],
        [w, W],
      ]);
    if (z > w)
      ans.push([
        [Math.max(u, x), Math.min(U, X)],
        [Math.max(v, y), Math.min(V, Y)],
        [w, z - 1],
      ]);
    if (Z < W)
      ans.push([
        [Math.max(u, x), Math.min(U, X)],
        [Math.max(v, y), Math.min(V, Y)],
        [Z + 1, W],
      ]);
    return ans;
  };

  let cubes = [];
  for (const [state, ...cube1] of switches) {
    const newCubes = [];

    for (const cube2 of cubes) {
      newCubes.push(...split(cube1, cube2));
    }

    if (state === "on") newCubes.push(cube1);
    cubes = newCubes;
  }

  let ans = 0;
  for (const [xx, yy, zz] of cubes) {
    ans += (xx[1] - xx[0] + 1) * (yy[1] - yy[0] + 1) * (zz[1] - zz[0] + 1);
  }

  console.log(ans);
})();
