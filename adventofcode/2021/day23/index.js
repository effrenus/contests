const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n").map((l) => l.split``));

  let pos = [];
  for (let r = 0; r < input.length; r++) {
    for (let c = 0; c < input[0].length; c++) {
      if (input[r][c] === "." || input[r][c] === "#") continue;
      pos.push([r, c]);
    }
  }

  const dest = {
    A: 2,
    B: 4,
    C: 6,
    D: 8,
  };

  const cost = {
    A: 1,
    B: 10,
    C: 100,
    D: 1000,
  };

  const nextPos = (grid, pos) => {
    const ch = grid[pos[0]][pos[1]];
    const q = [pos];
    const visited = {};
    let ans = [];

    while (q.length) {
      const [r, c] = q.shift();

      if (visited[[r, c]]) continue;
      visited[[r, c]] = 1;

      if (r !== pos[0] && c !== pos[1]) {
        if (r === 0 && c !== pos[1]) ans.push([r, c]);

        if (r === 4 && c === dest[ch]) return [[r, c]];
        if (r === 3 && c === dest[ch] && grid[4][dest[ch]] === ch)
          return [[r, c]];
        if (
          r === 2 &&
          c === dest[ch] &&
          grid[4][dest[ch]] === ch &&
          grid[3][dest[ch]] === ch
        )
          return [[r, c]];
        if (
          r === 1 &&
          c === dest[ch] &&
          grid[4][dest[ch]] === ch &&
          grid[3][dest[ch]] === ch &&
          grid[2][dest[ch]] === ch
        )
          return [[r, c]];
      }

      // prettier-ignore
      for (const [dr, dc] of [[1, 0],[-1, 0],[0, 1],[0, -1]]) {
        const [nr, nc] = [r + dr, c + dc];
        if (
          nr < 0 ||
          nr >= grid.length ||
          nc < 0 ||
          nc >= grid[0].length ||
          grid[nr][nc] !== "."
        )
          continue;

        q.push([nr, nc]);
      }
    }

    return ans;
  };

  const possible = (grid, from, to) => {
    const q = [from];
    const visited = {};

    while (q.length) {
      const [r, c] = q.shift();

      if (visited[[r, c]]) continue;
      visited[[r, c]] = 1;
      if (r === to[0] && c === to[1]) return true;

      // prettier-ignore
      for (const [dr, dc] of [[1, 0],[-1, 0],[0, 1],[0, -1]]) {
        const [nr, nc] = [r + dr, c + dc];
        if (
          nr < 0 ||
          nr >= grid.length ||
          nc < 0 ||
          nc >= grid[0].length ||
          grid[nr][nc] !== "."
        )
          continue;

        q.push([nr, nc]);
      }
    }

    return false;
  };

  const move = (state) => {
    const { grid, active, locked } = state;
    for (const [i, [r, c]] of locked.entries()) {
      if (r !== 0) continue;
      const ch = grid[r][c];
      if (possible(grid, [r, c], [4, dest[ch]])) {
        grid[r][c] = ".";
        grid[4][dest[ch]] = ch;
        locked.splice(i, 1);
        state.energy += (4 + Math.abs(c - dest[ch])) * cost[ch];
        return true;
      } else if (
        grid[4][dest[ch]] === ch &&
        possible(grid, [r, c], [3, dest[ch]])
      ) {
        grid[r][c] = ".";
        grid[3][dest[ch]] = ch;
        locked.splice(i, 1);
        state.energy += (3 + Math.abs(c - dest[ch])) * cost[ch];
        return true;
      } else if (
        grid[4][dest[ch]] === ch &&
        grid[3][dest[ch]] === ch &&
        possible(grid, [r, c], [2, dest[ch]])
      ) {
        grid[r][c] = ".";
        grid[2][dest[ch]] = ch;
        locked.splice(i, 1);
        state.energy += (2 + Math.abs(c - dest[ch])) * cost[ch];
        return true;
      } else if (
        grid[4][dest[ch]] === ch &&
        grid[3][dest[ch]] === ch &&
        grid[2][dest[ch]] === ch &&
        possible(grid, [r, c], [1, dest[ch]])
      ) {
        grid[r][c] = ".";
        grid[1][dest[ch]] = ch;
        locked.splice(i, 1);
        state.energy += (1 + Math.abs(c - dest[ch])) * cost[ch];
        return true;
      }
    }
    for (const [i, [r, c]] of active.entries()) {
      const ch = grid[r][c];
      if (c === dest[ch]) continue;
      if (possible(grid, [r, c], [4, dest[ch]])) {
        grid[r][c] = ".";
        grid[4][dest[ch]] = ch;
        active.splice(i, 1);
        state.energy +=
          (4 + Math.abs(r - 0) + Math.abs(c - dest[ch])) * cost[ch];
        return true;
      } else if (
        grid[4][dest[ch]] === ch &&
        possible(grid, [r, c], [3, dest[ch]])
      ) {
        grid[r][c] = ".";
        grid[3][dest[ch]] = ch;
        active.splice(i, 1);
        state.energy +=
          (3 + Math.abs(r - 0) + Math.abs(c - dest[ch])) * cost[ch];
        return true;
      } else if (
        grid[4][dest[ch]] === ch &&
        grid[3][dest[ch]] === ch &&
        possible(grid, [r, c], [2, dest[ch]])
      ) {
        grid[r][c] = ".";
        grid[2][dest[ch]] = ch;
        active.splice(i, 1);
        state.energy +=
          (2 + Math.abs(r - 0) + Math.abs(c - dest[ch])) * cost[ch];
        return true;
      } else if (
        grid[4][dest[ch]] === ch &&
        grid[3][dest[ch]] === ch &&
        grid[2][dest[ch]] === ch &&
        possible(grid, [r, c], [1, dest[ch]])
      ) {
        grid[r][c] = ".";
        grid[1][dest[ch]] = ch;
        active.splice(i, 1);
        state.energy +=
          (1 + Math.abs(r - 0) + Math.abs(c - dest[ch])) * cost[ch];
        return true;
      }
    }

    return false;
  };

  const print = (arr) => console.log(arr.map((r) => r.join``).join`\n` + "\n");
  const copy = (arr) => arr.slice().map((r) => r.slice());

  let min = 50_000;
  let iter = 0;
  const visited = {};
  const seen = {};

  const find = () => {
    const q = [{ grid: input, active: pos, locked: [], energy: 0 }];

    while (q.length) {
      const state = q.shift();
      const { grid, locked, active, energy } = state;

      if (energy >= min) continue;
      if (visited[JSON.stringify(grid)]) continue;

      visited[JSON.stringify(state)] = 1;

      if (
        //prettier-ignore
        grid[1][2] === "A" && grid[2][2] === "A" && grid[3][2] === "A" && grid[4][2] === "A" &&
        grid[1][4] === "B" && grid[2][4] === "B" && grid[3][4] === "B" && grid[4][4] === "B" &&
        grid[1][6] === "C" && grid[2][6] === "C" && grid[3][6] === "C" && grid[4][6] === "C" &&
        grid[1][8] === "D" && grid[2][8] === "D" && grid[3][8] === "D" && grid[4][8] === "D"
      ) {
        min = Math.min(min, energy);
        console.log(min);
        continue;
      }

      for (let i = 0; i < active.length; i++) {
        const [r, c] = active[i];
        const ch = grid[r][c];

        for (const [nr, nc] of nextPos(grid, [r, c])) {
          const newGrid = copy(grid);

          newGrid[r][c] = ".";
          newGrid[nr][nc] = ch;

          const newState = {
            grid: newGrid,
            active: active.filter((v) => v !== active[i]),
            locked: [...locked, [nr, nc]],
            energy:
              state.energy + (Math.abs(nr - r) + Math.abs(nc - c)) * cost[ch],
          };

          while (move(newState));

          if (visited[JSON.stringify(newGrid)]) continue;

          q.push(newState);
        }
      }
    }
  };

  find();

  console.log(min);

  // for (const arr of minHistory) print(arr);
})();
