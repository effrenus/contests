const read = require("../../read-input");

(async () => {
  const input = await read((i) =>
    i
      .split("\n")
      .map((l) => l.split(" -> ").map((v) => v.split(",").map(Number)))
  );

  const cnt = {};

  for (let [[x1, y1], [x2, y2]] of input) {
    const dx = Math.abs(x2 - x1) / (x2 - x1 || 1);
    const dy = Math.abs(y2 - y1) / (y2 - y1 || 1);

    while (x2 !== x1 || y2 !== y1) {
      cnt[`${x1},${y1}`] = (cnt[`${x1},${y1}`] || 0) + 1;

      x1 += dx;
      y1 += dy;
    }
    cnt[`${x2},${y2}`] = (cnt[`${x2},${y2}`] || 0) + 1;
  }

  console.log(Object.values(cnt).filter((v) => v >= 2).length);
})();
