const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n"));

  const formulas = {};
  for (const l of input.slice(2)) {
    const [_, p1, p2] = l.match(/(.+) -> (.+)/);
    formulas[p1] = [p2];
  }

  let stats = {};
  for (let i = 0; i < input[0].length - 1; i++) {
    stats[input[0].slice(i, i + 2)] =
      (stats[input[0].slice(i, i + 2)] || 0) + 1;
  }

  let step = 1;

  while (step++ < 41) {
    const newStat = {};
    for (const k of Object.keys(stats)) {
      const cnt = stats[k];

      const newPart = formulas[k];
      newStat[k[0] + newPart] = (newStat[k[0] + newPart] || 0) + cnt;
      newStat[newPart + k[1]] = (newStat[newPart + k[1]] || 0) + cnt;
    }
    stats = newStat;
  }

  const st = {};
  for (const k of Object.keys(stats)) {
    for (const ch of k[0]) st[ch] = (st[ch] || 0) + stats[k];
  }
  st[input[0].slice(-1)[0]] += 1;
  const cnts = Object.values(st).sort((a, b) => a - b);

  console.log(cnts.pop() - cnts[0]);
})();
