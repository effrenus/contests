const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n"));
  const enhance = input[0];

  const convert = (val) => (val === "." ? "0" : "1");

  let outsideColor = "0";
  const image = input.slice(2).map((r) => r.split``.map(convert));

  const countPixels = () => {
    let cnt = 0;
    for (let r = 0; r < image.length; r++) {
      for (let c = 0; c < image[0].length; c++) {
        if (image[r][c] === "1") cnt++;
      }
    }
    return cnt;
  };

  let step = 0;

  while (++step < 51) {
    const m = image.length;
    const n = image[0].length;

    for (const r of image) {
      r.push(outsideColor);
      r.unshift(outsideColor);
    }
    image.unshift(Array(n + 2).fill(outsideColor));
    image.push(Array(n + 2).fill(outsideColor));

    const getAt = (r, c) => {
      if (r < 0 || r >= m + 2 || c < 0 || c >= n + 2) return outsideColor;
      return image[r][c].split`|`[0];
    };

    for (let r = 0; r < m + 2; r++) {
      for (let c = 0; c < n + 2; c++) {
        const n =
          getAt(r - 1, c - 1) +
          getAt(r - 1, c) +
          getAt(r - 1, c + 1) +
          getAt(r, c - 1) +
          getAt(r, c) +
          getAt(r, c + 1) +
          getAt(r + 1, c - 1) +
          getAt(r + 1, c) +
          getAt(r + 1, c + 1);
        const val = convert(enhance[parseInt(n, 2)]);
        image[r][c] = image[r][c] + "|" + val;
      }
    }

    for (let r = 0; r < m + 2; r++) {
      for (let c = 0; c < n + 2; c++) {
        image[r][c] = image[r][c].split`|`[1];
      }
    }

    outsideColor = convert(
      outsideColor === "0" ? enhance[0] : enhance[enhance.length - 1]
    );

    if (step === 2 || step === 50) console.log(countPixels());
  }
})();
