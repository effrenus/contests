const read = require("../../read-input");

(async () => {
  const input = await read((i) => i);
  const [_, xmin, xmax, ymin, ymax] = input.match(
    /target area: x=(-?\d+)\.\.(-?\d+), y=(-?\d+)\.\.(-?\d+)/
  );

  const cnt = new Set();
  let overall_max = -Infinity;

  const check = ({ xv, yv }) => {
    let x = 0;
    let y = 0;
    const [_xv, _yv] = [xv, yv];
    let local_max = -Infinity;

    while (true) {
      x += xv;
      y += yv;

      local_max = Math.max(local_max, y);

      xv = xv === 0 ? 0 : xv > 0 ? xv - 1 : xv + 1;
      yv -= 1;

      if (yv < 0 && y < ymin) break;
      if ((xv === 0 && x < xmin) || x > xmax) break;

      if (x >= xmin && x <= xmax && y >= ymin && y <= ymax) {
        cnt.add(`${_xv},${_yv}`);
        overall_max = Math.max(overall_max, local_max);
      }
    }
  };

  for (let xv = 0; xv < xmax + 1; xv++) {
    for (let yv = -Math.abs(ymin) - 1; yv < Math.abs(ymin) + 1; yv++) {
      check({ xv, yv });
    }
  }

  console.log("Part1:", overall_max);
  console.log("Part2:", cnt.size);
})();
