const read = require("../../read-input");

(async () => {
  const input = await read((i) =>
    i.split("\n").map((l) => l.split``.map(Number))
  );

  let step = 0;
  let flashes = 0;

  while (++step < 1_000) {
    const q = [];
    for (let i = 0; i < input.length; i++) {
      for (let j = 0; j < input.length; j++) {
        input[i][j] += 1;
        if (input[i][j] === 10) q.push([i, j]);
      }
    }

    while (q.length) {
      const [r, c] = q.shift();

      flashes += 1;
      input[r][c] = -1;

      // prettier-ignore
      for (const [dr, dc] of [[1, 1], [1, -1], [1, 0], [-1, 0], [-1, 1], [0, 1], [0, -1], [-1, -1]]) {
        const [nr, nc] = [r + dr, c + dc];
        // prettier-ignore
        if (nr < 0 || nr >= input.length || nc < 0 || nc >= input[0].length || input[nr][nc] === -1)
          continue;
        input[nr][nc] += 1;
        if (input[nr][nc] === 10) q.push([nr, nc]);
      }
    }

    if (step === 100) console.log("Part1:", flashes);

    let cnt = 0;
    for (let i = 0; i < input.length; i++) {
      for (let j = 0; j < input.length; j++) {
        if (input[i][j] === -1) {
          input[i][j] = 0;
          cnt++;
        }
      }
    }

    if (cnt === input.length * input[0].length) {
      return console.log("Part2:", step);
    }
  }
})();
