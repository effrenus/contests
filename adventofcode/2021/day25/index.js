const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n").map((l) => l.split``));
  const m = input.length;
  const n = input[0].length;

  let steps = 0;

  const clear = () => {
    for (let r = 0; r < m; r++) {
      for (let c = 0; c < n; c++) {
        const [p1, p2] = input[r][c].split`|`;
        input[r][c] = p2 ? p2 : p1;
      }
    }
  };

  while (true) {
    steps++;

    let movedCnt = 0;

    // Move `>`
    for (let r = 0; r < m; r++) {
      for (let c = 0; c < n; c++) {
        if (input[r][c] !== ">") continue;

        const nc = (c + 1) % n;

        if (input[r][nc] !== ".") continue;

        movedCnt++;
        input[r][nc] = ".|>";
        input[r][c] += "|.";
      }
    }
    clear();

    // Move `v`
    for (let r = 0; r < m; r++) {
      for (let c = 0; c < n; c++) {
        if (input[r][c] !== "v") continue;

        const nr = (r + 1) % m;

        if (input[nr][c] !== ".") continue;

        movedCnt++;
        input[nr][c] = ".|v";
        input[r][c] += "|.";
      }
    }
    clear();

    if (movedCnt === 0) break;
  }

  console.log(steps);
})();
