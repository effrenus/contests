const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n").map((l) => l.split``));

  const scores = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137,
  };
  const m = {
    ")": "(",
    "]": "[",
    "}": "{",
    ">": "<",
  };

  let s = 0;
  for (let l of input) {
    const stack = [];
    for (const ch of l) {
      if (ch in m) {
        if (stack[stack.length - 1] === m[ch]) stack.pop();
        else {
          s += scores[ch];
          break;
        }
      } else stack.push(ch);
    }
  }

  console.log("Part1:", s);

  const scores2 = {
    "(": 1,
    "[": 2,
    "{": 3,
    "<": 4,
  };

  const ans = [];
  for (let l of input) {
    let s = 0;
    const stack = [];
    let isBroken = false;
    for (const ch of l) {
      if (ch in m) {
        if (stack[stack.length - 1] === m[ch]) stack.pop();
        else {
          isBroken = true;
          break;
        }
      } else stack.push(ch);
    }

    if (!isBroken && stack.length) {
      while (stack.length) {
        s = s * 5 + scores2[stack.pop()];
      }
      ans.push(s);
    }
  }
  console.log("Part2:", ans.sort((a, b) => a - b)[ans.length >> 1]);
})();
