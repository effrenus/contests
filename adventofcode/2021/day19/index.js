const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n"));

  const scanners = [];
  let data = [];

  for (const line of input) {
    if (line.startsWith("---")) continue;
    if (line.trim() === "") {
      scanners.push(data);
      data = [];
    } else data.push(line.split`,`.map(Number));
  }
  scanners.push(data);

  const getCoords = ([x, y, z]) => [
    [x, y, z],
    [y, x, z],
    [z, x, y],
    [x, z, y],
    [y, z, x],
    [z, y, x],
  ];

  const getDirs = ([x, y, z]) => [
    [x, y, z],
    [x, y, -z],
    [x, -y, z],
    [x, -y, -z],
    [-x, y, z],
    [-x, y, -z],
    [-x, -y, z],
    [-x, -y, -z],
  ];

  let beacons = [...scanners.shift().map((c) => getDirs(getCoords(c)[0])[0])];
  const scanDiff = [[0, 0, 0]];

  while (scanners.length) {
    for (const [idx, scanner] of scanners.entries()) {
      const overlap = {};

      for (const beacon of beacons) {
        for (const bb of scanner) {
          for (const [i, cc] of getCoords(bb).entries()) {
            for (const [j, ccc] of getDirs(cc).entries()) {
              if (ccc.some((coord) => Object.is(coord, -0))) continue;
              const diff = [
                beacon[0] - ccc[0],
                beacon[1] - ccc[1],
                beacon[2] - ccc[2],
              ];

              overlap[`${diff}|${i}|${j}`] =
                (overlap[`${diff}|${i}|${j}`] || 0) + 1;
            }
          }
        }
      }

      const sol = Object.entries(overlap).find(([_, cnt]) => cnt >= 12);
      if (sol) {
        scanners.splice(idx, 1);
        let [diff, i, j] = sol[0].split("|");
        diff = diff.split(",").map(Number);
        scanDiff.push(diff);
        for (const cc of scanner) {
          beacons.push(getDirs(getCoords(cc)[i])[j].map((v, i) => v + diff[i]));
        }
        beacons = [...new Set(beacons.map(String))].map((v) =>
          v.split`,`.map(Number)
        );
        break;
      }
    }
  }

  beacons = [...new Set(beacons.map(String))].map((v) =>
    v.split`,`.map(Number)
  );

  console.log("Part1:", beacons.length);

  let maxDist = -Infinity;
  for (const b1 of scanDiff) {
    for (const b2 of scanDiff) {
      maxDist = Math.max(
        maxDist,
        Math.abs(b1[0] - b2[0]) +
          Math.abs(b1[1] - b2[1]) +
          Math.abs(b1[2] - b2[2])
      );
    }
  }

  console.log("Part2:", maxDist);
})();
