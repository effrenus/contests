const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split(",").map(Number));

  const cnt = {};
  for (let i = 0; i < 9; i++) {
    cnt[i] = 0;
  }

  for (const v of input) {
    cnt[v] += 1;
  }

  let days = 0;
  while (days < 256) {
    let toAdd = 0;

    for (let i = 0; i < 9; i++) {
      if (i === 0) {
        toAdd = cnt[0];
      } else {
        cnt[i - 1] += cnt[i];
      }
      cnt[i] = 0;
    }

    cnt[8] += toAdd;
    cnt[6] += toAdd;

    days += 1;
  }

  console.log(
    Object.values(cnt)
      .filter((v) => v > 0)
      .reduce((acc, v) => acc + v)
  );
})();
