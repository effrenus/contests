const read = require("../../read-input");

// prettier-ignore
const nums = {
  '1110111': 0,
  '0010010': 1,
  '1011101': 2,
  '1011011': 3,
  "0111010": 4,
  '1101011': 5,
  '1101111': 6,
  '1010010': 7,
  '1111111': 8,
  '1111011': 9,
};

(async () => {
  const input = await read((i) =>
    i.split("\n").map((l) => {
      const [pattern, display] = l.split("|");

      return [pattern.trim().split(" "), display.trim().split(" ")];
    })
  );

  let ans = 0;
  for (const [_, dis] of input) {
    for (const d of dis) {
      const s = new Set(d);
      if (s.size === 2 || s.size === 4 || s.size === 3 || s.size === 7) ans++;
    }
  }

  console.log("Part1:", ans);

  let sum = 0;

  for (const [pattern, ns] of input) {
    const segs = Array(7).fill("");

    const stats = {};
    for (const p of pattern) {
      for (const ch of p) stats[ch] = (stats[ch] || 0) + 1;
    }

    const one = pattern.find((v) => v.length === 2);
    const four = pattern.find((v) => v.length === 4);
    const seven = pattern.find((v) => v.length === 3);
    const eight = pattern.find((v) => v.length === 7);

    segs[0] = seven.split``.find((v) => !one.includes(v));
    segs[1] = Object.entries(stats).find(([_, v]) => v === 6)[0];
    segs[4] = Object.entries(stats).find(([_, v]) => v === 4)[0];
    segs[5] = Object.entries(stats).find(([_, v]) => v === 9)[0];
    segs[2] = one.split``.find((v) => v !== segs[5]);
    segs[3] = four.split``.find((v) => !segs.includes(v));
    segs[6] = eight.split``.find((v) => !segs.includes(v));

    let n = "";
    for (const d of ns) {
      let dec = "";
      for (const c of segs) dec += d.includes(c) ? 1 : 0;
      n += nums[dec];
    }
    sum += +n;
  }

  console.log("Part2:", sum);
})();
