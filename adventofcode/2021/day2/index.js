const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split("\n").map((v) => v.split(" ")));

  const pos = [0, 0];

  for (const [move, val] of input) {
    switch (move) {
      case "forward":
        pos[0] += +val;
        break;
      case "down":
        pos[1] += +val;
        break;
      case "up":
        pos[1] -= +val;
        break;
      default:
        throw new Error("");
    }
  }

  console.log(pos[0] * pos[1]);
})();

(async () => {
  const input = await read((i) => i.split("\n").map((v) => v.split(" ")));

  let aim = 0;
  const pos = [0, 0];

  for (const [move, val] of input) {
    switch (move) {
      case "forward":
        pos[0] += +val;
        pos[1] += aim * val;
        break;
      case "down":
        aim += +val;
        break;
      case "up":
        aim -= +val;
        break;
      default:
        throw new Error("");
    }
  }

  console.log(pos[0] * pos[1]);
})();
