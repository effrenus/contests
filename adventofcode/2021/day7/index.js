const read = require("../../read-input");

(async () => {
  const input = await read((i) => i.split(",").map(Number));

  const sum = input.reduce((acc, v) => acc + v);
  const avg = Math.ceil(sum / input.length);

  let min = Number.MAX_SAFE_INTEGER;

  for (let i = avg; i >= 0; i--) {
    let s = 0;
    for (const v of input) {
      const n = Math.abs(v - i);
      // s += n;
      s += (n * (n + 1)) / 2;
    }
    if (s > min) break;
    if (s < min) min = s;
  }

  console.log(min);
})();
