const read = require("../../read-input");

function remove(mat, n) {
  for (let i = 0; i < mat.length; i++) {
    for (let j = 0; j < mat[0].length; j++) {
      if (mat[i][j] === n) mat[i][j] = -1;
    }
  }
}

function isWin(mat) {
  for (let c = 0; c < mat[0].length; c++) {
    let valid = true;
    for (let r = 0; r < mat.length; r++) {
      if (mat[r][c] !== -1) valid = false;
    }
    if (valid) return true;
  }

  return mat.some((r) => r.every((v) => v === -1));
}

function sum(mat) {
  return mat
    .map((r) => r.reduce((acc, v) => (v !== -1 ? acc + v : acc), 0))
    .reduce((acc, v) => acc + v, 0);
}

(async () => {
  const input = await read((i) => i.split("\n"));
  const nums = input[0].split(",").map(Number);

  const ps = [];
  let mat = [];

  for (let line of input.slice(2)) {
    if (!line.trim()) {
      ps.push(mat);
      mat = [];
    } else {
      mat.push(line.split(/\s+/).filter(Boolean).map(Number));
    }
  }

  let cnt = 0;
  const seen = {};

  for (const n of nums) {
    for (const [i, mat] of ps.entries()) {
      remove(mat, n);
      if (isWin(mat) && !seen[i]) {
        seen[i] = 1;
        cnt += 1;
        if (cnt === ps.length) {
          console.log(n * sum(mat));
        }
      }
    }
  }
})();
