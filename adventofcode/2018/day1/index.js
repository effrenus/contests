const fs = require("fs");
const path = require("path");

function readInput(filename) {
  const inputStr = fs.readFileSync(filename).toString();
  return inputStr
    .split("\n")
    .map(v => parseInt(v))
    .filter(v => !Number.isNaN(v));

}

function part2() {
  const vals = readInput(path.resolve(__dirname, "./input.txt"));
  
  let i = 0;
  let sum = 0;
  const occured = new Set;
  occured.add(sum);
  while (true) {
    i = i % vals.length;
    sum += vals[i++];
    if (occured.has(sum)) {
      console.log(sum);
      break;
    }
    occured.add(sum);
  }
}

part2();