const fs = require("fs");
const path = require("path");

function part1() {
  const inputStr = fs.readFileSync(path.resolve(__dirname, "./input.txt")).toString();
  const edges = {};
  const inVertCount = {};

  inputStr
    .split("\n")
    .forEach(s => {
      const [_, fromNode, toNode] = /Step (\w) must be finished before step (\w) can begin./.exec(s);
      if (!edges[fromNode]) edges[fromNode] = [];
      edges[fromNode].push(toNode);
      inVertCount[toNode] = inVertCount[toNode] ? inVertCount[toNode] + 1 : 1;
    });
  
  const queue = [];
  for (let key of Object.keys(edges))
    if (!inVertCount[key] || inVertCount[key] === 0)
      queue.push(key);
  
  let ans = "";
  while (queue.length) {
    queue.sort();
    const vert = queue.shift();
    ans += vert;
    if (!edges[vert]) continue;
    for (let v of edges[vert]) {
      inVertCount[v] -= 1;
      if (inVertCount[v] === 0)
        queue.push(v);
    }
  }

  return ans;
}

console.log(
  part1()
);