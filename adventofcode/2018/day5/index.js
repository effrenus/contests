const fs = require("fs");
const path = require("path");

function readInput(filename) {
  const inputStr = fs.readFileSync(filename).toString();
  return inputStr;
}

function part1(polymers) {
  if (!polymers) polymers = readInput(path.resolve(__dirname, "./input.txt"));
  const len = polymers.length;
  let i = 0;
  let j = -1;
  let stab = "";
  while (i < len - 1) {
    const a = polymers.charCodeAt(i);
    const b = polymers.charCodeAt(i + 1);
    
    if (Math.abs(a - b) !== 32) {
      stab += polymers[i];
      j++;
      i++;
    } else {
      i += 2;
      while (j >= 0 && i < len) {
        const a = stab.charCodeAt(j);
        const b = polymers.charCodeAt(i);
        if (Math.abs(a - b) === 32) {
          j--;
          i++;
        } else {
          break;
        }
      }
      stab = stab.substring(0, j + 1);
    }
  }
  while (i < len) {
    stab += polymers[i++];
  }

  return stab.length;
}

console.log(
  part1()
);

function part2() {
  const polymers = readInput(path.resolve(__dirname, "./input.txt"));
  let min = Infinity;
  "a b c d e f g h i j k l m n o p q r s t u v w x y z".split(" ").forEach(ch => {
    const filtered = polymers.replace(new RegExp(`${ch}|${ch.toUpperCase()}`, "g"), "");
    const afterReactionLen = part1(filtered);
    if (afterReactionLen < min) min = afterReactionLen;
  });

  return min;
}

console.log(
  part2()
);