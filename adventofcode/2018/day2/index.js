const fs = require("fs");
const path = require("path");

function readInput(filename) {
  const inputStr = fs.readFileSync(filename).toString();
  return inputStr
    .split("\n")
    .filter(v => Boolean(v.trim()));
}

function part1() {
  const ids = readInput(path.resolve(__dirname, "./input.txt"));

  let twoCounts = 0;
  let threeCounts = 0;
  ids.forEach(id => {
    const letterCounts = Array.from({ length: 26 }).map(_ => 0);
    id.split("").forEach(l => letterCounts[l.charCodeAt(0) - 97]++);
    letterCounts.some(v => v === 2) && twoCounts++;
    letterCounts.some(v => v === 3) && threeCounts++;
  });
  
  console.log(
    twoCounts * threeCounts
  );
}

part1();

class Trie {
  constructor() {
    this.letters = Array.from({ length: 26 });
  }
  add(word) {
    const code = word[0].charCodeAt(0) - 97;
    if (word.length ===  1) {
      this.letters[code] = true;
      return;
    }

    const tr = this.letters[code] ? this.letters[code] : new Trie;
    tr.add(word.substr(1));
    this.letters[code] = tr;
  }
  hasWord(word, index) {
    const code = word.charCodeAt(index) - 97;

    if (!this.letters[code]) return false;
    
    return index === word.length - 1 ? true : this.letters[code].hasWord(word, index + 1);
  }
  getOneLetterDiffWord(word, index) {
    const code = word.charCodeAt(index) - 97;
    let i = 0;
    const isLast = index === word.length - 1;

    for (let tr of this.letters) {
      if (!tr) {
        i++;
        continue;
      }
      
      if (code !== i) {
        if (isLast || tr.hasWord(word, index + 1)) {
          return word.substring(0, index) + word.substring(index + 1);
        }
      } else if (!isLast) {
        const w = tr.getOneLetterDiffWord(word, index + 1);
        if (w) return w;
      }
      i++;
    }
    return null;
  }
}

function part2() {
  const ids = readInput(path.resolve(__dirname, "./input.txt"));

  const root = new Trie();
  ids.forEach(id => root.add(id));
  ids.forEach(id => {
    const diff = root.getOneLetterDiffWord(id, 0);
    diff && console.log(diff);
  });
}

part2();