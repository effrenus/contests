const fs = require("fs");
const path = require("path");

function readInput(filename) {
  const inputStr = fs.readFileSync(filename).toString();
  return inputStr
    .split("\n")
    .filter(v => Boolean(v.trim()))
    .map(s => {
      //#1 @ 37,526: 17x23
      const [_, id, left, top, w, h] = /#(\d+) @ (\d+),(\d+): (\d+)x(\d+)/.exec(s);
      return {
        id: parseInt(id, 10),
        left: parseInt(left, 10),
        top: parseInt(top, 10),
        w: parseInt(w, 10),
        h: parseInt(h, 10),
        isIntact: true
      };
    });
}

function part1() {
  const cuts = readInput(path.resolve(__dirname, "./input.txt"));
  const collisions = {};
  let cells = 0;
  cuts.forEach(v => {
    for (let row = v.top+1; row <= v.top+v.h; row++) {
      for (let col = v.left+1; col <= v.left+v.w; col++) {
        let count = collisions[`${row}_${col}`] || 0;
        if (count == 2) continue;
        else if (count == 1) cells++;
        collisions[`${row}_${col}`] = ++count;
      }
    }
  });

  console.log(
    cells
  );
}

// part1();

function part2() {
  const cuts = readInput(path.resolve(__dirname, "./input.txt"));
  const claims = new Map;
  cuts.forEach(v => {
    for (let row = v.top+1; row <= v.top+v.h; row++) {
      for (let col = v.left+1; col <= v.left+v.w; col++) {
        const claim = claims.get(`${row}_${col}`);
        if (!claim) {
          claims.set(`${row}_${col}`, { prev: v, count: 1 });
        } else {
          const { prev } = claim;
          prev.isIntact = false;
          v.isIntact = false;
        }
      }
    }
  });

  Object.values(cuts).forEach(v => v.isIntact && console.log(v.id));
}

part2();