module.exports = (transformFn) => {
    let body = '';

    process.stdin.setEncoding('utf8');

    return new Promise((resolve) => {
        process.stdin.on('readable', () => {
            let chunk;
            while ((chunk = process.stdin.read()) !== null) {
                body += chunk;
            }
        });

        process.stdin.on('end', () => {
            resolve(transformFn(body));
        });
    });
}