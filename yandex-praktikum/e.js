function solve(elms) {
  const lifeSpan = new Map
  
  const g = Array.from({ length: 1001 }).map(_ => [])
  for (const [idx, v, l, r] of elms) {
    lifeSpan.set(idx, v)
    
    if (l != -1) g[idx].push(l)
    if (r != -1) g[idx].push(r)
  }
  
  const lvls = []
  const q = [[1, 0]]
  
  while (q.length) {
    const [idx, lvl] = q.shift()
    
    if (!lvls[lvl]) lvls[lvl] = []
    lvls[lvl].push(lifeSpan.get(idx))
    
    for (const n of g[idx]) {
      q.push([n, lvl+1])
    }
  }
  
  console.log(lvls.map(vs => (vs.reduce((acc,v)=>acc+v,0)/vs.length).toFixed(2)).join(' '))
}


const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin
});

const lines = []

rl.on('line', function(line){
    lines.push(line)
})

rl.on('close', () => {
    solve(lines.slice(1).map(r => r.split(' ').map(parseFloat)))
})

/*
1 1 2 3
2 2 4 5
3 3 6 -1
4 4 7 -1
5 5 -1 -1
6 6 -1 -1
7 7 -1 -1
*/
