const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin
});

const lines = []

rl.on('line', function(line){
    lines.push(line)
})

rl.on('close', () => {
  const target = +lines.shift()
  const nums = lines.pop().split(' ').map(Number)
  nums.sort((a, b) => a - b)
  
  let closestSum = nums[0]+nums[1]+nums[2]
  
  for (let first = 0; first < nums.length-2; first++) {
    let second = first+1
    let third = nums.length-1
    
    while (second < third) {
      const sum = nums[first]+nums[second]+nums[third]
      if (Math.abs(target - sum) < Math.abs(target - closestSum)) {
        closestSum = sum
      }
      
      if (sum > target) {
        third--
      } else {
        second++
      }
    }
  }
  
  console.log(closestSum)
})

