const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin
});

rl.on('line', function(num){
    const encodedNum = parseInt(num.split('').reverse().join(''))
    console.log((num.startsWith('-') ? '-' : '') + encodedNum)
})

