const readline = require('readline')
const rl = readline.createInterface({input: process.stdin})

const f = (n, [r,...rs], [e,...es], ans=[]) => !n ? ans : f(--n,rs,es,[...ans,r,e])

const lines = []
rl.on('line', line => lines.push(line))
rl.on('close', ([n,r,e] = lines) => 
	console.log(
    	+lines[0]
        ? f(+n, r.split` `, e.split` `).join` `
        : ''
     )
)

