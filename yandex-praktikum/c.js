const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin
});

const lines = []
rl.on('line', function(l){
	lines.push(l)
})

rl.on('close', function(){
    const N = +lines[0]
    const arr = Array(N+1).fill().map((_, i) => i)
    
    for (let i = 0; i < lines[1].length; ++i) {
    	if (lines[1][i] == ' ') continue
        let n = ''
        while (lines[1][i] && lines[1][i] != ' ') n += lines[1][i++]
        arr[n] = 0
    }
    
    console.log(arr.filter(v => Boolean(v)).join(' '))
})

