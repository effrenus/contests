function printSpiral(mat) {
  const len = mat.length
  const dirs = [[1,0],[0,1],[-1,0],[0,-1]]
  let dir = 0
  
  const nums = len*len
  const vals = Array.from({ length: nums }).fill(-1)

  let c = 0
  let pos = [0, 0]

  let i1 = 0
  while (c < nums) {
    const [i, j] = pos

    if (mat[i][j] != null) {
      c++
      vals[nums-c] = mat[i][j]
      mat[i][j] = null
    }

    const [ni, nj] = [i+dirs[dir][0], j+dirs[dir][1]]
    if (ni >= 0 && ni < len && nj >= 0 && nj < len && mat[ni][nj] !== null) {
      pos = [ni, nj]
    } else {
      dir = (dir+1) % dirs.length
    }
  }
  
  console.log(vals.join('\n'))
}

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin
});

const lines = []

rl.on('line', function(line){
    lines.push(line)
})

rl.on('close', () => {
    const mat = lines.slice(1).map(r => r.split(' ').map(parseFloat))
    
    printSpiral(mat)
})

