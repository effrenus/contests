function t(servers, check) {
    let l = 0
    let r = servers.length - 1

    const bisect = () => {
      const mid = (l + r) >> 1
      if (l >= r) return check(servers[l]).then(isOk => isOk ? servers[l+1] : servers[l])

      return check(servers[mid]).then(isOk => {
          if (isOk) {
            l = mid + 1
          } else {
            r = mid - 1
          }
          return bisect()
      })
    }
    
    return bisect()
}

// Test

const check = (name) => new Promise((res) => setTimeout(res, 100)).then(() => res[name])

const res = {
  'srv-a': true,
  'srv-b': true,
  'srv-c': true,
  'srv-d': true,
  'srv-e': true,
  'srv-f': false,
  'srv-g': false, 
  'srv-h': false,
}

t([
'srv-a',
'srv-b',
'srv-c',
'srv-d',
'srv-f',
'srv-g'], check).then(console.log).catch(console.error)