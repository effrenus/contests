function t(diffs) {
    let signX = -1
    let signY = -1
    
    const updated = diffs.map(([diffX, diffY]) => {
        if (diffX) {
          diffX *= signX
          signX *= -1
        }
        if (diffY) {
          diffY *= signY
          signY *= -1
        }
        return [diffX, diffY]
    })

    return updated.reduce((acc, [x, y]) => acc+x+y, 0) === 0 ? updated : null
}