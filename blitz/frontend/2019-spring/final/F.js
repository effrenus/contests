
// interface PlayerClient {  
//     on(event: ’move’, callback: (move: [number, number]) => void): void;  
//     nextTurn(rivalMove: [number, number]): void;  
//     retry(): void;  
//     end(result: ’win’ | ’lose’ | ’tie’): void;  
// }

// Tic-Tac-Toe

class Game {
    constructor(p1 /* : PlayerClient */, p2) {
        this.board = [
            ['z','z','z'],
            ['z','z','z'],
            ['z','z','z']
        ]
        
        this.p1 = {
            inst: p1,     
            char: 'x',
            tries: 0,
            rival: p2
        }

        this.p2 = {
            inst: p2,     
            char: 'o',
            tries: 0,
            rival: p1
        }

        p1.on('move', this.move.bind(this, this.p1))
        p2.on('move', this.move.bind(this, this.p2))
    }
    start() {
        this.playerTurn = this.p1
        this.p1.inst.nextTurn(null)
    }
    move(p, m) {
        if (p != this.playerTurn) return
        
        if (this.board[m[0]][m[1]] !== 'z') {
            p.tries += 1
            if (p.tries === 3) {
                this.end(p.rival)
                return
            }
            p.inst.retry()
            return
        }

        this.board[m[0]][m[1]] = p.char
        p.tries = 0

        const status = this.getBoardStatus(p)
        if (status == 'tie') this.end()
        else if (status == 'win') this.end(p)
        else if (status == 'lose') this.end(p.rival)
        else {
            this.playerTurn = p.rival
            p.rival.inst.nextTurn(move)
        }
    }
    getBoardStatus(p) {
        const lines = []
        this.board.forEach(l => lines.push(l.join('')))
        for(let i = 0; i < this.board.length; i++) {
            let l = ''
            for(let j = 0; j < this.board.length; j++) {
                l += this.board[j][i]
            } 
            lines.push(l)
        }
        lines.push(this.board[0][2]+this.board[1][1]+this.board[2][0])
        lines.push(this.board[0][0]+this.board[1][1]+this.board[2][2])

        if (lines.indexOf(p.char.repeat(3)) > -1) return 'win'
        if (lines.indexOf(p.rival.char.repeat(3)) > -1) return 'lose'
        if (lines.every(l => l.indexOf('z') === -1)) return 'tie'

        return 'next'
    }
    end(p) {
        if (!p) {
            this.p1.inst.end('tie')
            this.p2.inst.end('tie')
            return
        }
        
        p.inst.end('win')
        p.rival.inst.end('lose')
    }
}

function t(p1, p2) {
    const game = new Game(p1, p2)
    game.start()
}