module.exports = function findShortestMarkedPathLength(graph, markedVertices) {  
    const stack = [[0, 0, []]];
    const lastVertice = graph.length - 1;
    const visited = new Set();
    const vs = [];
    
    while (stack.length) {
    	const [step, v, p] = stack.shift();
        visited.add(v);
        
        if (step === 3 && (markedVertices.indexOf(v) === -1 && v !== lastVertice)) {
            visited.delete(v);
        	continue;
        }
        
        if (v === lastVertice) {
        	vs.push([...p, v]);
            continue;
        }
      
        const nextStep = markedVertices.indexOf(v) > -1 ? 1 : step + 1;
        
        graph[v].forEach(nv => {
        	if (!visited.has(nv)) {
            	stack.unshift([nextStep, nv, [...p, v]]);
           	}
        });
    }
    
    const min = vs.reduce((min, v) => v.length < min ? v.length : min, Infinity);
    
    return min === Infinity ? null : min - 1;
}