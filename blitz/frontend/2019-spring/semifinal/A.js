module.exports = function t(explorers) {  
	const places = {}
    explorers.forEach(([e, ...ps]) => {
    	ps.forEach(p => {
        	if (places[p]) {
                places[p].push(e)
            } else {
            	places[p] = [e]
            }
        })
    })
    
    return Object.keys(places).map(k => [k, ...places[k]])
}