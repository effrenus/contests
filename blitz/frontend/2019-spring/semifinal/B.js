/**  
 * @param {Country} startCountry  
 * @param {Function} getNeightbors  
 */  
module.exports = function (startCountry, getNeightbors) {  
	const visited = {}
    const unColored = []
    const cs = [startCountry]
    
    function visitNext() {
        const c = cs.shift()
        visited[c.code] = 1
        
        if (typeof c.color === "undefined") unColored.push(c.code)
        
        return getNeightbors(c.code)
        	.then(css => cs.push(...css.filter(c => !visited[c.code] ? (visited[c.code] = 1, true) : false )))
            .then(_ => cs.length > 0 ? visitNext() : Promise.resolve(unColored))
        
    }
    
    return visitNext()
}