const sortByCoordinates = (arr) => arr.sort((a, b) => a.geometry[0] - b.geometry[0])

const getMsg = (arr, prop) => {
    sortByCoordinates(arr)
    return arr.map(v => v[prop]).join(' ')
}

module.exports = function (data1, data2, maxLength) {
    const errorMsg = 'Message is too long'
    const [len1, len2] = [data1.length, data2.length]

    return [
        len1 > maxLength ? errorMsg : getMsg(data1, 'part1'),
        len2 > maxLength ? errorMsg : getMsg(data2, 'part1'),
        (len1 + len2) > maxLength ? errorMsg : getMsg(data1.concat(data2), 'part2')
    ]
}