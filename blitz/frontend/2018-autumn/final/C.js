const express = require('express');

const { BEEP_CODES } = require('@yandex-blitz/phone');

const createApp = ({ phone }) => {
    const app = express();

    // звонит по номеру записанному в "быстром наборе" под цифрой digit
    app.get("/speeddial/:digit", async (req, res) => {
        phone.connect()
            .then(() => {
                return phone.getData()
                    .then((value) => {
                        const speeddialDict = JSON.parse(value);
                        if (!speeddialDict[req.params.digit]) throw Error(`Record #${req.params.digit} doesn\'t exists.`);

                        return phone.dial(speeddialDict[req.params.digit])
                    })
                    .then(() => res.sendStatus(200))
                    .catch(() => {
                        phone.beep(BEEP_CODES.ERROR);

                        res.sendStatus(500);
                    });
            })
            .catch(() => {
                phone.beep(BEEP_CODES.FATAL);

                res.sendStatus(500);
            })
    });

    // записывает в "быстрый набор" под цифру digit номер phonenumber
    let writeLock = null;
    app.post("/speeddial/:digit/:phonenumber", async (req, res) => {
        if (writeLock) await writeLock;
        writeLock = new Promise(resolve => {
            return phone.getData()
                .then(value => {
                    const speeddialDict = JSON.parse(value);
                    speeddialDict[req.params.digit] = Number(req.params.phonenumber);

                    return phone.setData(JSON.stringify(speeddialDict))
                        .then(() => {
                            phone.beep(BEEP_CODES.SUCCESS);

                            res.sendStatus(200);
                        });
                })
                .catch(() => {
                    phone.beep(BEEP_CODES.ERROR);

                    res.sendStatus(500);
                })
                .then(resolve);
        });
        return writeLock;
    });

    return app;
};

exports.createApp = createApp;
