const solveCaptcha = require('./E');

console.log(
    solveCaptcha('TRABWARH\nTHSCAHAW\nWWBSCWAA\nCACACHCR')
);

console.log(
    solveCaptcha('CSRARHAR\nCWAHCBSW\nABWBSWBA\nRBSBTABH')
);

console.log(
    solveCaptcha('HSRSTBHC\nCAWTRTBT\nWBATSTRA\nTWRBRTRR\nRWTABSHB\nTWCBWBCA')
);

console.log(
    solveCaptcha('TSRSBWAC\nASCSWBTC\nTTAHTABC\nAHWTRWWA')
);

console.log(
    solveCaptcha('SSSSS')
);

console.log(
    solveCaptcha('TTST\nTTST\nSTTT')
);

function genTestString(rows, cols, onChar = (_row, _col) => 'T') {
    return Array.from({length: rows})
        .map((_, i) => Array.from({length: cols}).map((_, j) => onChar(i, j)).join(''))
        .join('\n');
}

console.assert(
    solveCaptcha(genTestString(100, 100, (row, col) => ( (row == 0 && col == 0) || (row == 99 && col == 99) ? 'S' : 'T'))).length === 2
);

console.assert(
    solveCaptcha(genTestString(1000, 1000, (row, col) => ( (row == 0 && col == 0) || (row == 999 && col == 999) ? 'S' : 'T'))).length === 2
);

console.assert(// FAIL =(
    solveCaptcha(genTestString(10000, 10000, (row, col) => ( (row == 0 && col == 0) || (row == 9999 && col == 9999) ? 'S' : 'T'))).length === 2
);