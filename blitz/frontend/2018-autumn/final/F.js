const conflictHash = new Map;

function maxFilesCompute(prs, index, excludePrs, includePrs) {
    let maxFiles = 0;
    const ids = [];
    for (let i = index; i < prs.length; i++) {
        const pr = prs[i];
        const conflictIds = conflictHash.get(pr.id).conflictWith;

        if (conflictIds.some(id => includePrs.indexOf(id) >= 0)) continue;
        
        if (
            (conflictIds.length && conflictIds.every(id => excludePrs.indexOf(id) >= 0))
            || !conflictIds.length
        ) {
            maxFiles += pr.files.length;
            ids.push(pr.id);
        } else {
            const r1 = maxFilesCompute(prs, i + 1, excludePrs.concat([pr.id]), includePrs);
            const r2 = maxFilesCompute(prs, i + 1, excludePrs, includePrs.concat([pr.id]));

            return r1.maxFiles > r2.maxFiles + pr.files.length ? {
                maxFiles: maxFiles + r1.maxFiles,
                ids: ids.concat(r1.ids)
            } : {
                maxFiles: maxFiles + r2.maxFiles + pr.files.length,
                ids: ids.concat([pr.id].concat(r2.ids))
            }
        }
    }

    return { maxFiles: maxFiles, ids: ids };
}

/**  
 * @param {PullRequest[]} pullRequests массив PR, отсортированных по времени создания  
 * @returns {string[]} идентификаторы реквестов в порядке мёржа  
 */  
module.exports = function(pullRequests) {
    conflictHash.clear();

    const files = new Map;
    pullRequests.forEach(pr => {
        conflictHash.set(pr.id, {
            conflictWith: []
        });

        pr.files.forEach(filename => {
            if (files.has(filename)) {
                const file = files.get(filename);
                file.conflict = true;
                file.prs.forEach(id => {
                    conflictHash[id].conflictWith.push(pr.id);
                    conflictHash[pr.id].conflictWith.push(id);
                });
                file.prs.push(pr.id);
            } else {
                files.set(filename, {
                    conflict: false,
                    prs: [pr.id]
                });
            }
        });
    });
    files.clear();

    return maxFilesCompute(pullRequests, 0, [], []).ids;
}