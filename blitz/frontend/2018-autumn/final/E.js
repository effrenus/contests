const INFINITY_MAX = 1e9;

const debug = (...args) => process.env.DEBUG && console.log(...args);

function calcPointArea (pointIndex, data, result) {
    const { occupiedArea, points, sizes, matrix } = data;
    const point = points[pointIndex];
    let infinityProtection = 0;
    if (pointIndex >= points.length) throw Error('point index out of bounds');

    for (let sizeIndex = 0; sizeIndex < sizes.length; sizeIndex++) {
        const size = sizes[sizeIndex];
        for (let sl = 0; sl < size[0]; sl++) {
            for (let st = 0; st < size[1]; st++) {
                if (++infinityProtection === INFINITY_MAX) throw Error('infinite loop in calcPointArea'); 
                const tl = [point[0]-sl, point[1]-st];
                if (
                    tl[0] < 0 || tl[1] < 0 // Compare matrix bounds.
                    || tl[0]+size[0] - 1 >= matrix[0].length
                    || tl[1]+size[1] - 1 >= matrix.length
                    || points.some(p => ( // Is there other sign points inside rectangle?
                        p !== point
                        && (tl[0] <= p[0] && p[0] <= tl[0]+size[0]-1)
                        && (tl[1] <= p[1] && p[1] <= tl[1]+size[1]-1)))
                    || occupiedArea.some(a => { // Is there rectangles that overlap?
                        if (tl[0]+size[0] <= a.tl[0] || a.tl[0]+a.size[0] <= tl[0])
                            return false;
                        if (tl[1]+size[1] <= a.tl[1] || a.tl[1]+a.size[1] <= tl[1])
                            return false;
                        return true;
                    })
                ) continue;

                occupiedArea.push({
                    tl: tl,
                    size: size
                });
                if (pointIndex !== points.length - 1) {
                    calcPointArea(pointIndex + 1, data, result);
                } else if (occupiedArea.length === points.length) {
                    debug('SUCCESS');
                    result.push(occupiedArea.slice());
                }
                occupiedArea.pop();
            }
        }
    }
}

function calcPossibleDims (square) {
    const sizes = new Set;
    for (let i = 1; i <= Math.sqrt(square); i++) {
        if (square%i) continue;
        sizes.add(i+'-'+(square/i));
        sizes.add((square/i)+'-'+i);
    }
    return Array.from(sizes).map(v => v.split('-').map(Number));
}

function filterResults (foundAreas, matrix) {
    const selectedArr = foundAreas
        .map(arr => {
            let dir = 'r';
            const sortedArr = [];
            let infinityProtection = 0;
            while (arr.length > 1) {
                if (++infinityProtection === INFINITY_MAX) throw Error('infinite loop'); 
                let minPoint = arr[0].tl;
                let index = 0;
                if (dir === 'r') {
                    arr.forEach((a, i) => {
                        if (a.tl[1] < minPoint[1]) { minPoint = a.tl; index = i; return; }
                        if (a.tl[1] === minPoint[1] && a.tl[0] < minPoint[0]) { minPoint = a.tl; index = i; return; }
                    });
                    dir = 'b';
                } else {
                    arr.forEach((a, i) => {
                        if (a.tl[0] < minPoint[0]) { minPoint = a.tl; index = i; return; }
                        if (a.tl[0] === minPoint[0] && a.tl[1] < minPoint[1]) { minPoint = a.tl; index = i; return; }
                    });
                    if (minPoint[1] + arr[index].size[1] >= matrix.length) dir = 'r';
                }
                sortedArr.push(arr.splice(index, 1)[0]);
            }
            if (arr.length) sortedArr.push(arr[0]);
            return sortedArr;
        })
        .reduce((selectedArr, curArr) => {
            for (let i = 0; i < selectedArr.length; i++ ) {
                if (curArr[i].size[0] > selectedArr[i].size[0]) return curArr;
            }
            return selectedArr;
        });

    return selectedArr.map(area =>{
        let row = area.tl[1];
        let col = area.tl[0];
        const rLimit = row + area.size[1], cLimit = col + area.size[0];
        let str = '';
        let infinityProtection = 0;
        for (;row < rLimit; row++) {
            for (col = area.tl[0]; col < cLimit; col++) {
                if (++infinityProtection === INFINITY_MAX) throw Error('infinite loop'); 
                str = str + matrix[row][col];
            }
            if (row !== rLimit - 1) str += '\n';
        }
        return str;
    });
}

module.exports = function solveCaptcha (captcha) {
    const signCount = captcha.split('S').length - 1;

    if (signCount === 1) return captcha;

    const matrix = captcha.split('\n').map(str => str.split(''));
    const matrixSquare = matrix.length * matrix[0].length;

    if (matrixSquare % signCount) return [];

    debug(matrixSquare);
    const points = [];
    const sizes = calcPossibleDims(matrixSquare / signCount);
    const occupiedArea = [];
    const foundAreas = [];

    sizes.sort((a, b) => b[0] - a[0]);

    // Find sign positions and populate `points` array.
    matrix.forEach((row, i) => row.forEach((_col, j) => matrix[i][j] === 'S' && points.push([j, i])));

    calcPointArea(0, {
        sizes, points, occupiedArea, matrix, matrixSquare
    }, foundAreas);

    return foundAreas.length
        ? filterResults(foundAreas, matrix)
        : foundAreas;
}