// Parse head.
function parseCode(str, pos, data) {
    const ch = str[pos];
    if (!/[G-U][3-8]{3}/.test(str) || ch === 'I' || ch === 'J' ) throw Error('wrong code');
    data.push(str.substring(pos, 4));
    return pos + 4;
}

function parseSector(str, pos, data) {
    const ch = str[pos];
    if (ch !== 'B' && ch !== 'T' ) throw Error('wrong sector code');
    data.push(ch);
    return pos + 1;
}

// Parse bio section.
function parseBioLifeForm(str, pos, data) {
    if (!/C|K|M|B/.test(str[pos])) throw Error('bio life form');
    data.push(str[pos]);
    return pos + 1;
}

function parseBioRealm(str, pos, data) {
    if (!/G|J|P/.test(str[pos])) throw Error('bio realm');
    data[data.length-1] = data[data.length-1] + str[pos];
    return pos + 1;
}

// Parse tech section.
function parseTechLifeForm(str, pos, data) {
    if (!/O|R|S/.test(str[pos])) throw Error('tech life form');
    data.push(str[pos]);
    return pos + 1;
}

function parseTechRealm(str, pos, data) {
    if (!/J|8|M|E/.test(str[pos])) throw Error('tech realm');
    data[data.length-1] = data[data.length-1] + str[pos];
    return pos + 1;
}

function parseNumber(str, pos, data) {
    const numLen = str.length - pos - 1;
    if (numLen < 1 || 24 < numLen) throw Error('illegal number length');
    if (!new RegExp(`^[0-9A-Y]{${numLen}}$`, '').test(str.substr(pos, numLen))) throw Error('wrong number');
    if (!/Z/.test(str[str.length - 1])) throw Error('wrong last letter');
    data.push(str.substr(pos, numLen));
    return str.length;
}

const BIO_SECTOR = 'B';
const TECH_SECTOR = 'T';

/** @returns Array<string>|null */  
module.exports = function parse(inputString) {
  try {
    if (!inputString.length) return null;
    let pos = 0, data = [];
    pos = parseCode(inputString, pos, data);
    pos = parseSector(inputString, pos, data);
    switch (data[data.length-1]) {
      case BIO_SECTOR:
        pos = parseBioLifeForm(inputString, pos, data);
        pos = parseBioRealm(inputString, pos, data);
        break;
      case TECH_SECTOR:
        pos = parseTechLifeForm(inputString, pos, data);
        pos = parseTechRealm(inputString, pos, data);
        break;
      default:
        return null;
    }
    parseNumber(inputString, pos, data); 
    
    return data;
  } catch (_err) { return null; }
}