const cmpS = (a, b) => (a.toUpperCase() > b.toUpperCase() ? 1 : (a.toUpperCase() === b.toUpperCase() ? 0 : -1));

const getBandKey = b => '__B__' + b.name;
const getGenreKey = g => '__G__' + g.name;

function getOrCreateGenre({elm, hash, parsedData}) {
    const key = getGenreKey(elm);
    
    if (hash.has(key)) return parsedData.genres[hash.get(key)];

    hash.set(key, parsedData.genres.length);
    const newGenre = {
        isRoot: elm.parent === null,
        name: elm.name,
        bands: new Set(),
        subgenres: new Set()
    };
    parsedData.genres.push(newGenre);
    return newGenre;
}

function getOrCreateBand({elm, hash, parsedData}) {
    const key = getBandKey(elm);
    
    if (hash.has(key)) return parsedData.bands[hash.get(key)];

    hash.set(key, parsedData.bands.length);
    const newBand = {
        name: elm.name,
        friends: new Set()
    };
    parsedData.bands.push(newBand);
    return newBand;
}

function formatGenre(g, { parsedData, hash }, pad = 0) {
    const prefix = '\n'+`${Array.from({length: pad}).map(_ => ' ').join('')}- ${g.name}`;
    const bandsStr = g.bands.size 
                        ? ': ' + Array.from(g.bands)
                                      .map(k => parsedData.bands[hash.get(k)].name)
                                      .sort(cmpS)
                                      .join(', ')
                        : '';
    const subgenresStr = Array.from(g.subgenres)
                            .sort((a, b) => cmpS(parsedData.genres[hash.get(a)].name, parsedData.genres[hash.get(b)].name))
                            .map(k => formatGenre(parsedData.genres[hash.get(k)], { parsedData, hash }, pad + 2))
                            .join('');

    return prefix + bandsStr + subgenresStr;
}

function formatBand(b, { parsedData, hash }) {
    const prefix = `\n- ${b.name}`;
    const friendStr = b.friends.size 
                            ? ', друзья: ' + 
                              Array.from(b.friends)
                                   .map(key => parsedData.bands[hash.get(key)].name)
                                   .sort(cmpS)
                                   .join(', ') 
                            : '';
    return prefix + friendStr;
}

function formatData({ parsedData, hash }) {
    const genresStr = parsedData.genres.length ? `## Жанры
${parsedData.genres
    .filter(g => g.isRoot)
    .sort((a, b) => cmpS(a.name, b.name))
    .map(g => formatGenre(g, { parsedData, hash }))
    .join('')}` : '';

const bandsStr = parsedData.bands.length ? `## Группы
${parsedData.bands
    .slice()
    .sort((a, b) => cmpS(a.name, b.name))
    .map(b => formatBand(b, { parsedData, hash }))
    .join('')}` : '';

    return genresStr + (genresStr && bandsStr ? '\n\n' : '') + bandsStr;
}

/**
 * @param {Band|Genre} data - ссылка на группу или жанр,
 * из которой нужно восстановить все возможные данные
 * @return {string}
 */
module.exports = function (data) {
    const hash = new Map();
    const parsedData = { genres: [], bands: [] };
    const queue = [data];
    data.processed = true;
    const queueAppend = item => !item.processed && (item.processed = true, queue.push(item));

    while (queue.length) {
        const elm = queue.shift();
        const elmKey = elm.type === 'genre' ? getGenreKey(elm) : getBandKey(elm);

        switch (elm.type) {
            case 'genre':
            const genreElm = getOrCreateGenre({elm, hash, parsedData});

            elm.subgenres.forEach(v => {
                const vKey = getGenreKey(v);
                genreElm.subgenres.add(vKey);
                queueAppend(v);
            });
            elm.bands.forEach(b => {
                const bKey = getBandKey(b);
                genreElm.bands.add(bKey);
                queueAppend(b);
            });
            if (elm.parent) {
                const parentKey = getGenreKey(elm.parent);
                const parentElm = getOrCreateGenre({elm: elm.parent, hash, parsedData});
                parentElm.subgenres.add(elmKey);
                queueAppend(elm.parent);
            }
            break;
            case 'band':
            const bandElm = getOrCreateBand({elm, hash, parsedData});

            elm.genres.forEach(v => {
                const vKey = getGenreKey(v);
                const genreElm = getOrCreateGenre({elm: v, hash, parsedData});
                genreElm.bands.add(elmKey);
                queueAppend(v);
            });
            elm.friends.forEach(v => {
                const vKey = getBandKey(v);
                bandElm.friends.add(vKey);
                queueAppend(v);
            });
            break;
            default:
            throw Error('unknown type');
        }
    }

    return formatData({ parsedData, hash });
}