const A = require('./A');

console.log(A('G333TR81Z'));
console.log(A('o464TR849BM182BDZ'));
// console.log(A('I464TR849BM182BDZ'));
// console.log(A('J464TR849BM182BDZ'));
console.log(A('U345BMG123456789ABCDEFZ'));
console.log(A('U345BMG123456789ABCDEF'));
console.assert(A('I345BMG123456789ABCDEFZ') === null, "1");
console.assert(A('U345BMG123456789ABCDEF') === null, "2");
console.assert(A('O35BMG123456789ABCDEFZ') === null, "3");
console.assert(A('O315BMG123456789ABCDEFZ') === null, "4");
console.assert(A('O3456BMG123456789ABCDEFZ') === null, "4");
console.assert(A('O346QMG123456789ABCDEFZ') === null, "4");
console.assert(A('O345BMG123456789ABCDZEF313Z') === null, "4");