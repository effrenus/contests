const sleep = (s = 1000) => new Promise(r => setTimeout(r, s));

/***  
 * @param container {Node} ссылка на DOM-node, в которую нужно вписать строку ‘str‘  
 * @param str {string} строка, которую необходимо вписать. Максимальная длина равняется 100 символам  
 * @param min {number} минимальное значение ‘font-variation-settings‘ (целое число)  
 * @param max {number} максимальное значение ‘font-variation-settings‘ (целое число)  
 * @return {number | null} искомое значение ‘font-variation-settings‘ (целое число) или null, если текст вписать нельзя  
 */  
function calcFontVariationSettings (container, str, min, max) {
  const rect = container.getBoundingClientRect();
  const containerWidth = rect.width;
  const elm = document.createElement('span');
  elm.textContent = str;
  elm.style.whiteSpace = 'nowrap';
  container.appendChild(elm);
  
  let probVal, variationWidth = -1;
  while (true) {
    probVal = (max + min) >> 1;
    elm.style.fontVariationSettings = `'wdth' ${probVal}`;
    const curRect = elm.getBoundingClientRect();
    if (curRect.width <= containerWidth) {
      variationWidth = probVal;
      min = probVal + 1;
    } else {
      max = probVal - 1;
    }
    if (max < min) break;
  }
  return variationWidth === -1 ? null : variationWidth;
}