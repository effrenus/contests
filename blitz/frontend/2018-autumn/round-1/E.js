(function() {
    const DELAY_STEP = 150; // ms
    const cardCounts = new Map();
    Array.from(document.querySelectorAll('.target .symbol')).forEach(p => {
        const sym = window.getComputedStyle(p, ':before').getPropertyValue('content');
        if (sym) cardCounts[sym] = !cardCounts[sym] ? 1 : cardCounts[sym] + 1;
    });

    let step = 0;
    Array.from(document.querySelectorAll('.keys .key')).forEach(p => {
        const sym = window.getComputedStyle(p, ':before').getPropertyValue('content');
        if (cardCounts[sym] && cardCounts[sym] >= 2) setTimeout(() => p.click(), DELAY_STEP * step++);
    });
})();