const parse = require('./F_iter');

// Жанры в памяти
const Genre1 = { type: 'genre', name: 'АРок', bands: [], subgenres: [], parent: null };
const Genre1Sub1 = { type: 'genre', name: '2Классик-рок', bands: [], subgenres: [], parent: null };
const Genre1Sub2 = { type: 'genre', name: '3Акустик-рок', bands: [], subgenres: [], parent: null };
const Genre1Sub3 = { type: 'genre', name: '1Полурок', bands: [], subgenres: [], parent: null };
const Genre1Sub3Sub1 = { type: 'genre', name: 'Полурок 2', bands: [], subgenres: [], parent: null };
const Genre1Sub3Sub1Sub1 = { type: 'genre', name: 'Полурок 3', bands: [], subgenres: [], parent: null };
const Genre2 = { type: 'genre', name: 'Нерок', bands: [], subgenres: [], parent: null };

// Разбираемся с роком
Genre1.subgenres.push(Genre1Sub1, Genre1Sub2, Genre1Sub3);
Genre1Sub1.parent = Genre1Sub2.parent = Genre1Sub3.parent = Genre1;
Genre1Sub3Sub1.parent = Genre1Sub3;
Genre1Sub3Sub1Sub1.parent = Genre1Sub3Sub1;
Genre1Sub3Sub1.subgenres.push(Genre1Sub3Sub1Sub1);
Genre1Sub3.subgenres.push(Genre1Sub3Sub1)

// Группы в памяти
const Band1 = { type: 'band', name: 'абсный слой', friends: [], genres: [] };
const Band2 = { type: 'band', name: 'Крбсный слой', friends: [], genres: [] };
const Band3 = { type: 'band', name: 'Красный слой', friends: [], genres: [] };

// И в жанрах
Band1.genres.push(Genre1Sub1);
Genre1Sub1.bands.push(Band1);

Band2.genres.push(Genre1Sub2);
Genre1Sub2.bands.push(Band2);

// А Бритый гриб лабает в двух жанрах
Band3.genres.push(Genre2);
Genre2.bands.push(Band3);
Band3.genres.push(Genre1Sub3);
Genre1Sub3.bands.push(Band3);

// Группы умеют дружить
// Band1.friends.push(Band2);
Band2.friends.push(Band1);

// С некоторыми — по 2 раза, но это не взаимно
Band1.friends.push(Band3);

console.log(parse(Band3));