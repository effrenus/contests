const KEY_GENRE_PREFIX = '___GENRE__';
const KEY_BAND_PREFIX = '___BAND__';

const getBandKey = (b) => `${KEY_BAND_PREFIX}${b.name}`;
const getGenreKey = (g) => `${KEY_GENRE_PREFIX}${g.name}`;
const bandExist = (band, data) => typeof data.hash[getBandKey(band)] !== 'undefined';
const genreExist = (genre, data) => typeof data.hash[getGenreKey(genre)] !== 'undefined';

function getOrCreateGenre(genre, data) {
    if (typeof data.hash[getGenreKey(genre)] === 'undefined') {
        data.hash[getGenreKey(genre)] = data.genres.length;
        const newGenre = {
            isSub: !!genre.parent,
            name: genre.name,
            bands: new Set(),
            subgenres: new Set()
        };
        data.genres.push(newGenre);
        return newGenre;
    }
    return data.genres[data.hash[getGenreKey(genre)]];
}

function getOrCreateBand(band, data) {
    if (typeof data.hash[getBandKey(band)] === 'undefined') {
        data.hash[getBandKey(band)] = data.bands.length;
        const newBand = {
            name: band.name,
            friends: new Set(band.friends.map(getBandKey))
        };
        data.bands.push(newBand);
        return newBand;
    }
    return data.bands[data.hash[getBandKey(band)]];
}

function parseGenre(genre, data) {
    if (genreExist(genre, data)) return;
    const pGenre = getOrCreateGenre(genre, data);
    genre.bands.forEach(b => pGenre.bands.add(getBandKey(b)));
    genre.subgenres.forEach(g => pGenre.subgenres.add(getGenreKey(g)));

    genre.bands.forEach(b => parseBand(b, data));
    genre.subgenres.forEach(g => parseGenre(g, data));
    if (genre.parent) {
        const parent = getOrCreateGenre(genre.parent, data);
        parent.subgenres.add(getGenreKey(genre));
    }
}

function parseBand(band, data) {
    if (bandExist(band, data)) return;
    const pBand = getOrCreateBand(band, data);
    band.friends.forEach(b => {
        parseBand(b, data);
        pBand.friends.add(getBandKey(b));
    });
    band.genres.forEach(g => {
        parseGenre(g, data);
        const genre = getOrCreateGenre(g, data);
        genre.bands.add(getBandKey(band));
    });
}

function formatGenre(g, data, pad = 0) {
    return '\n'+`${Array.from({length: pad}).map(_ => ' ').join('')}- ${g.name}${(g.bands.size ? ': '+Array.from(g.bands).sort().map(k => data.bands[data.hash[k]].name).join(', ') : '')}${Array.from(g.subgenres).sort().map(k => formatGenre(data.genres[data.hash[k]], data, pad+2)).join('')}`;
}

function formatBand(b, data) {
    return `\n- ${b.name}${(b.friends.size ? ', друзья: '+Array.from(b.friends).sort().map(k => data.bands[data.hash[k]].name).join(', ') : '')}`;
}

function formatData(data) {
    return `## Жанры
${data.genres.filter(g => !g.isSub).sort((a, b) => a > b ? -1 : (a === b ? 0 : 1)).map(g => formatGenre(g, data)).join('')}

## Группы
${data.bands.map(b => formatBand(b, data)).join('')}`
}

/**
 * @param {Band|Genre} data - ссылка на группу или жанр,
 * из которой нужно восстановить все возможные данные
 * @return {string}
 */
module.exports = function (data) {
    const parsedData = {
        hash: {}, genres: [], bands: []
    };
    switch (data.type.trim()) {
        case 'genre':
            parseGenre(data, parsedData);
            break;
        case 'band':
            parseBand(data, parsedData);
            break;
        default:
            throw Error('unknown type');
    }
    return formatData(parsedData);
}