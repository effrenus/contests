// https://www.codechef.com/COOK120B/problems/EVENTUAL

const readline = require('readline')
const rl = readline.createInterface({ input: process.stdin })

const lines = []
const res = []
const stats = Array(26)

function solve() {
  lines.shift()
  let s = lines.shift()
  stats.fill(0)
  
  for (const ch of s) {
    stats[ch.charCodeAt()-97]++
  }
  
  res.push(stats.every(v=>v%2==0) ? 'YES' : 'NO')
}

rl.on('line', function(line) {
  lines.push(line)
})

rl.on('close', function() {
  lines.shift()
  while (lines.length) solve()
  console.log(res.join('\n'))
})

