// https://www.codechef.com/JULY20B/problems/PTMSSNG

#include <bits/stdc++.h>
using namespace std;

bool sortbysec(const pair<int,int> &a, 
              const pair<int,int> &b) { 
    return (a.second < b.second); 
} 

int main() {
	int t;
	cin >> t;
	while (t--) {
		int n;
		cin >> n;
		vector<pair<int, int>> points;
		int ps = 4*n-1;
		while (ps--) {
			int x, y;
			cin >> x >> y;
			points.push_back(make_pair(x, y));
		}
		sort(points.begin(), points.end());
		
		int ansX, ansY;
		for (int i = 0; i < points.size(); ++i) {
			int cnt = 0;
			while(i < points.size()-1 && points[i].first == points[i+1].first) {
				cnt++;
				i++;
			}
			cnt++;
			if (cnt%2 == 1) {
				ansX = points[i].first;
				break;
			}
		}
		
		sort(points.begin(), points.end(), sortbysec);
		for (int i = 0; i < points.size(); ++i) {
			int cnt = 0;
			while(i < points.size()-1 && points[i].second == points[i+1].second) {
				cnt++;
				i++;
			}
			cnt++;
			if (cnt%2 == 1) {
				ansY = points[i].second;
				break;
			}
		}
		cout << ansX << " " << ansY << endl;
	}
	return 0;
}

