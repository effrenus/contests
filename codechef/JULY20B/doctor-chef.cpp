#include <iostream>
#include <bits/stdc++.h>

using namespace std;

void solve() {
  int n, x;
  cin >> n >> x;
  
  vector<int> v;
  while (n--) {
    int val;
    cin >> val;
    v.push_back(val);
  }
  
  sort(v.begin(), v.end());
  
  int i = 0;
  for (auto& n : v) {
    if (n < x) {
      i++;
      continue;
    }
    break;
  }
  
  vector<int> v2;
  for (int k = i; k < v.size(); ++k) {
    v2.push_back(v[k]);
  }
  for (int k = i-1; k >= 0; --k) {
    v2.push_back(v[k]);
  }
  
  int daysToCure = 0;
  for (int i = 0; i < v2.size(); ++i) {
    while (x < v2[i]) {
      daysToCure++;
      x = x*2;
    }
    
    daysToCure++;
    x = v2[i]*2;
  }
  
  cout << daysToCure << endl;
}

int main() {
	int t;
	cin >> t;
	while (t--) solve();
	return 0;
}

