// https://www.codechef.com/JULY20B/problems/ADAKING/

const readline = require('readline')
const rl = readline.createInterface({ input: process.stdin })

let firstInv = true

rl.on('line', function(K) {
  if (firstInv) return firstInv = false
  const board = Array(8).fill().map(_ => Array(8).fill(0))
  
  const q = [[0, 0]]
  while (q.length && K) {
    const [r, c] = q.shift()
    
    if (board[r][c]) continue
    board[r][c] = 1
    K--
    
    for (const [dr, dc] of [[-1,0],[-1,1],[0,1],[1,1],[1,0],[1,-1],[0,-1],[-1,-1]]) {
        const [nr, nc] = [r+dr, c+dc]
        if (nr < 0 || nr >= 8 || nc < 0 || nc >= 8 || board[nr][nc] == 1) continue
        q.push([nr, nc])
    }
  }
  
  for (let r = 0; r < 8; ++r) {
    for (let c = 0; c < 8; ++c) {
      if (
        board[r][c] == 0 && 
        [[-1,0],[-1,1],[0,1],[1,1],[1,0],[1,-1],[0,-1],[-1,-1]].some(([dr,dc]) => { const [nr,nc]=[r+dr,c+dc]; return nr >= 0 && nr < 8 && nc >= 0 && nc < 8 && board[nr][nc] == 1 })
      ) board[r][c] = 2
    }
  }
  
  const map = ['.', '.', 'X','O']
  board[0][0] = 3

  console.log(board.map(r => r.map(v => map[v]).join``).join`\n`)
})

rl.on('close', function() { process.exit(0) })
