#include <iostream>
#include <bits/stdc++.h>

using namespace std;
typedef long long ll;

void solve() {
  int n;
  cin >> n;
  
  vector<ll> v1(n);
  vector<ll> v2(n);
  
  map<ll, ll> m1;
  map<ll, ll> m2;
  ll mn = INT_MAX;
  
  for (int i = 0; i < n; ++i) {
    cin >> v1[i];
    m1[v1[i]]++;
    mn = min(mn, v1[i]);
  }
  
  for (int i = 0; i < n; ++i) {
    cin >> v2[i];
    m2[v2[i]]++;
    mn = min(mn, v2[i]);
  }
  
  vector<ll> diff;
  
  for(auto const& [key, val] : m1) {
    int diffVal = abs(val - m2[key]);
    if (diffVal%2 == 1) {
      cout << "-1" << endl;
      return;
    }
    for (int i = 0; i < diffVal/2; ++i) diff.push_back(key);
  }
  for(auto const& [key, val] : m2) {
    if (m1[key] != 0) continue;
    if (val%2 == 1) {
      cout << "-1" << endl;
      return;
    }
    for (int i = 0; i < val/2; ++i) diff.push_back(key);
  }
  
  sort(diff.begin(), diff.end());
  
  ll minCount = 0;
  int sz = diff.size()/2;
  for (int i = 0; i < sz; ++i) {
    minCount += min(diff[i], 2*mn);
  }
  cout << minCount << endl;
}

int main() {
  int t;
  cin >> t;
  while (t--) {
    solve();
  }
	return 0;
}

