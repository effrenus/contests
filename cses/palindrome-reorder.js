const readline = require('readline')
const rl = readline.createInterface({
  input: process.stdin
})

function solve(chars) {
  const arr = Array(26).fill(0)
  for (const ch of chars) arr[ch.charCodeAt()-65]++
  
  let l =''
  let r = ''
  let mid = ''
  
  for (let [i, cnt] of arr.entries()) {
    if (cnt%2) {
      if (mid) return 'NO SOLUTION'
      mid = String.fromCharCode(i+65)
      cnt--
    }
    l += String.fromCharCode(i+65).repeat(cnt/2)
    r = String.fromCharCode(i+65).repeat(cnt/2) + r
  }
  
  return l+mid+r
}

rl.on('line', line => {
  console.log(solve(line))
})

