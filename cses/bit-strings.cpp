#include <iostream>
using namespace std;

int main() {
	int ans = 1, n;
	cin >> n;
	while (n--) ans = (ans*2)%1000000007;
	cout << ans << endl;
	return 0;
}
