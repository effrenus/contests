#include <iostream>
using namespace std;

int main() {
	int ans = 0, n;
	cin >> n;
	
	int i = 5;
	while (i <= n) {
		ans += n/i;
		i *= 5;
	}
	cout << ans << endl;
	return 0;
}
