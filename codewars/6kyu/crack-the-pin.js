// https://www.codewars.com/kata/5efae11e2d12df00331f91a6/

/** Brute-force */ 

const crypto = require('crypto')
const assert = require('assert')

const md5 = str => crypto.createHash('md5').update(str).digest('hex')

function crack(pinHash) {
  let pin = 0

  while (
    md5(`0000${pin}`.slice(-5)) !== pinHash &&
    ++pin < 100000
  ) /* EMPTY */ ;
  
  assert.ok(pin < 100000, `Invalid PIN: ${pin}. Must be 5 digits.`)
  
  return `0000${pin}`.slice(-5)
}

