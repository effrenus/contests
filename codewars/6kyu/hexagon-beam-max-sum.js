// https://www.codewars.com/kata/5ecc1d68c6029000017d8aaf/

function maxHexagonBeam(n,seq){
  let rowSize = n
  let pos = 0
  
  const h = []
  while (h.length < 2*n-1) {
    const r = []
    for (let i = 0; i < rowSize; ++i) {
      r.push(seq[pos])
      pos = (pos+1)%seq.length
    }
    h.push(r)
    
    if (h.length < n) rowSize += 1
    else rowSize -= 1
  }
  
  let max = -Infinity
  
  for (const r of h) max = Math.max(max, r.reduce((acc,v) => acc+v))
  
  for (let c = 0; c < n; ++c) {
    let s = 0
    let cc = c
    for (let r = 0; r < h.length; ++r) {
      s += h[r][cc]
      if (r >= n-1) cc -= 1
      if (cc < 0) break
    }
    max = Math.max(max, s)
  }
  
  for (let r = 1; r < n; r++) {
    let c = h[r].length-1
    let s = 0
    for (let rr = r; rr < h.length; rr++) {
      s += h[rr][c]
      if (rr >= n-1) c -= 1
    }
    max = Math.max(max, s)
  }
  
  for (let c = 0; c < n; ++c) {
    let s = 0
    let cc =c
    for (let r = h.length-1; r >= 0; --r) {
      s += h[r][cc]
      if (r <= n-1) cc -= 1
      if (cc < 0) break
    }
    max = Math.max(max, s)
  }
  
  for (let r = h.length-2; r >= n-1; --r) {
    let c = h[r].length-1
    let s = 0
    for (let rr = r; rr >= 0; --rr) {
      s += h[rr][c]
      if (rr <= n-1) c -= 1
    }
    max = Math.max(max, s)
  }
  
  return max
}

