// https://www.codewars.com/kata/57738d9110a0a6f1d50000a7/

// Recursion, backtracking. Messy.

function linkUp(mat) {
  mat = mat.split`\n`.map((row, r) => [' ', ...row.split` `.map(c=>c.toLowerCase()), ' '])
  mat.push(Array(mat[0].length).fill(' '))
  mat.unshift(Array(mat[0].length).fill(' '))
   
  const rows = mat.length
  const cols = mat[0].length
  
  const dist = (mat, start, end) => {
    const q = [[start, 0, []]]
    let min = Infinity
    const ds = mat.map(r => r.map(_ => 20))
    while (q.length) {
      const [[r,c], d, dir] = q.shift()
      if (r == end[0] && c == end[1]) {
        min = Math.min(d, min)
        continue
      }
      for (const [dr,dc] of [[-1,0], [0,1], [1,0], [0,-1]]) {
        const [nr,nc] = [r+dr,c+dc]
        if (nr < 0 || nr >= rows || nc < 0 || nc >= cols || /[A-Z]/i.test(mat[nr][nc]) || (mat[nr][nc] == '_' && (nr != end[0] || nc != end[1]))) continue
        if (d == 0 || dir[0] != dr || dir[1] != dc) {
          if (d+1 >= ds[nr][nc]) continue
          ds[nr][nc] = d+1
          q.push([[nr,nc], d+1, [dr,dc]])
        } else {
          if (d >= ds[nr][nc]) continue
          ds[nr][nc] = d
          q.push([[nr,nc], d, [dr,dc]])
        }
      }
    }
    return min
  }
  
  const possibleActions = (mat, letters) => {
    const actions = []
    let k = 0
    
    for (const letterPos of letters) {
      for (let i = 0; i < letterPos.length; ++i) {
        for (let j = i+1; j < letterPos.length; ++j) {
          if (dist(mat, letterPos[i], letterPos[j]) > 3) continue
          actions.push([k, i, j])
        }
      }
      k++
    }
    return actions
  }
  

  const checked = new Map
  let ans = []
  
  const find = (mat, letters, acts) => {
    if (ans.length) return
    if (checked.has(String(acts)) || checked.has(String(mat))) return

    if (acts.length == 32) {
      ans = acts
      return
    }
    
    const nextActions = possibleActions(mat, letters)
    
    for (const [ch, i, j] of nextActions) {
      const nletters = copy(letters)
      const nacts = acts.slice()
      nacts.push([nletters[ch][i], nletters[ch][j]])
      nletters[ch].splice(j, 1)
      nletters[ch].splice(i, 1)
      const nmat = copy(mat)
      
      bfs([letters[ch][i], letters[ch][j]], nmat, nletters)
      find(nmat, nletters, nacts)
    }
    
    checked.set(String(acts), true)
    checked.set(String(mat), true)
  }
  
  function copy(arr) { return arr.map(v => v.slice()) }
  
  function bfs(pos, mat, letters) {
    const q = pos
    for (const [r, c] of pos) mat[r][c] = ' '
    
    while (q.length) {
      const [r, c] = q.shift()
      if (mat[r][c] == '.') continue
      mat[r][c] = '.'

      for (const [dr,dc] of [[-1,0], [0,1], [1,0], [0,-1]]) {
        const [nr,nc] = [r+dr,c+dc]
        if (nr < 0 || nr >= rows || nc < 0 || nc >= cols || mat[nr][nc] == '.' || mat[nr][nc] == '_') continue
        if (mat[nr][nc] == ' ') {
          q.push([nr,nc])
          continue
        }
        letters[mat[nr][nc].charCodeAt()-97].push([nr,nc])
        mat[nr][nc] = '_'
      }
    }
  }
  
  const letters = Array.from({ length:26 }).map(_ => [])
  bfs([[0,0]], mat, letters)
   
  find(mat, letters, [])
   
  return ans.map(([a,b]) => [[a[0]-1,a[1]-1], [b[0]-1,b[1]-1]])
}

