// https://www.codewars.com/kata/527fde8d24b9309d9b000c4e/

// TODO: Find more compact way to find/detect rectangle bounds. Now it's messy.

const inBounds = ([r, c], mat) => r >= 0 && r < mat.length && c >= 0 && c < mat[0].length

const addBoundsAndStringify = (ps) => {
  ps.sort((a, b) => a[0]-b[0])
  const [minRow, maxRow] = [ps[0][0], ps[ps.length-1][0]]
  
  ps.sort((a, b) => a[1]-b[1])
  const [minCol, maxCol] = [ps[0][1], ps[ps.length-1][1]]
  
  const arr = Array(maxRow-minRow+3).fill().map(_ => Array(maxCol-minCol+3).fill(' '))
  for (const [r,c] of ps) arr[r-minRow+1][c-minCol+1] = '#'
  
  const rows = arr.length
  const cols = arr[0].length
  
  for (let r = 0; r < rows; ++r) {
    for (let c = 0; c < cols; ++c) {
      if (arr[r][c] == '#') continue
    
      if (r < rows-1 && arr[r+1][c] == '#' && arr[r][c-1] != '#' && arr[r][c+1] != '#') arr[r][c] = '-'
      if (r > 1 && arr[r-1][c] == '#' && arr[r][c+1] != '#' && arr[r][c-1] != '#') arr[r][c] = '-'
      if (c < cols-1 && arr[r][c+1] == '#' && arr[r+1][c] != '#') arr[r][c] = '|'
      if (c > 1 && arr[r][c-1] == '#' && arr[r-1][c] != '#' && arr[r+1][c] != '#') arr[r][c] = '|'
    }
  }
  
  for (let r = 0; r < rows; ++r) {
    for (let c = 0; c < cols; ++c) {
      if (arr[r][c] != ' ') continue

      if (
        [[1,0], [-1,0]].some(([dr,dc]) => inBounds([r+dr, c+dc], arr) && arr[r+dr][c+dc] == '|') ||
        [[0,1], [0,-1]].some(([dr,dc]) => inBounds([r+dr, c+dc], arr) && arr[r+dr][c+dc] == '-')
      ) arr[r][c] = '+'
    }
  }
  
  for (let r = 0; r < rows; ++r) while (arr[r][arr[r].length-1] == ' ') arr[r].pop()
  
  return arr.map(r => r.map(v => v == '#' ? ' ' : v).join``).join`\n`
}

const expand = (pos, mat) => {
  const queue = [pos]
  const squareCells = []
  while (queue.length) {
    const [r, c] = queue.shift()
    
    if (r == 0 || r == mat.length-1 || c == 0 || c == mat[0].length-1) return false
    
    if (mat[r][c] != ' ') continue
    mat[r][c] = 1
    squareCells.push([r, c])
    
    for (const [dr, dc] of [[1,0], [-1,0], [0,1], [0,-1]]) {
      const [nr, nc] = [r+dr, c+dc]
      if (!inBounds([nr, nc], mat) || mat[nr][nc] != ' ') continue
      queue.push([nr, nc])
    }
  }
  return addBoundsAndStringify(squareCells)
}


function breakPieces(shape) {
  shape = shape.split('\n').map(row => row.split``)
  const rows = shape.length
  const cols = shape[0].length
  
  const ans = []
  for (let r = 0; r < shape.length; ++r) {
    for (let c = 0; c < shape[0].length; ++c) {
      if (shape[r][c] == '+' && r+1 < rows && c+1 < cols && shape[r+1][c+1] == ' ') {
        ans.push(expand([r+1, c+1], shape))
      }
    }
  }

  return ans.filter(Boolean)
}

