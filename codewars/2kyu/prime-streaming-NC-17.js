// https://www.codewars.com/kata/59122604e5bc240817000016/

const n = 500000000
const primes = new Uint32Array(n/64|0)

function isPrime(x) { 
    return !(primes[x/64|0] & (1 << ((x >> 1) & 31))); 
} 
  
function makeComposite(x) {
    primes[x/64|0] |= (1 << ((x >> 1) & 31)); 
}

for (let i = 3; i * i <= n; i += 2) { 
  if (isPrime(i)) 
      for (let j = i * i, k = i << 1; j < n; j += k) 
          makeComposite(j); 
}

class Primes {
  static * stream() {
      yield 2
      for (let i = 3; i <= n; i += 2) if (isPrime(i)) yield i
  }
}

