// https://www.codewars.com/kata/58e61f3d8ff24f774400002c/

const CMP = { EQ: 0, LT: -1, GT: 1 }
const HALT = 'end'

class Interpreter {
  constructor(intructions) {
    this.registers = {}
    this.flags = { cmp: CMP.EQ }
    this.callStack = []
    this.symTbl = new Map
    this.ip = 0
    this.intructions = intructions
    
    this.preprocessInstr()
  }
  
  preprocessInstr() {
    for (let i = 0; i < this.intructions.length; ++i) {
      if (/^[a-z0-9_]+:/.test(this.intructions[i][0]))
        this.symTbl.set(this.intructions[i][0].slice(0,-1), i+1)
    }
  }
  
  run() {
    while (this.ip < this.intructions.length) {
      const [cmd, ...params] = this.intructions[this.ip]
      
      if (cmd == HALT) return this.msg
      this.ip = this[cmd](...params)
    }
    return -1
  }
  
  getVal(v) {
    return /^\d+$/.test(v) ? +v : (this.registers[v]||0)
  }
  
  mov(x, y) {
    this.registers[x] = this.getVal(y)
    return this.ip+1
  }
  inc(x) {
    this.registers[x]++
    return this.ip+1
  }
  dec(x) {
    this.registers[x]--
    return this.ip+1
  }
  add(x, y) {
    this.registers[x] += this.getVal(y)
    return this.ip+1
  }
  sub(x, y) {
    this.registers[x] -= this.getVal(y)
    return this.ip+1
  }
  mul(x, y) {
    this.registers[x] *= this.getVal(y)
    return this.ip+1
  }
  div(x, y) {
    this.registers[x] = this.registers[x]/this.getVal(y)|0
    return this.ip+1
  }
  jmp(x) {
    return this.symTbl.get(x)
  }
  cmp(x, y) {
    [x, y] = [this.getVal(x), this.getVal(y)]
    this.flags.cmp = x==y ? CMP.EQ : (x < y ? CMP.LT : CMP.GT)
    return this.ip+1
  }
  jne(x) { return (this.flags.cmp != CMP.EQ) ? this.symTbl.get(x) : this.ip+1 }
  je(x) { return (this.flags.cmp == CMP.EQ) ? this.symTbl.get(x) : this.ip+1 }
  jge(x) { return (this.flags.cmp == CMP.EQ || this.flags.cmp == CMP.GT) ? this.symTbl.get(x) : this.ip+1 }
  jg(x) { return (this.flags.cmp == CMP.GT) ? this.symTbl.get(x) : this.ip+1 }
  jle(x) { return (this.flags.cmp == CMP.EQ || this.flags.cmp == CMP.LT) ? this.symTbl.get(x) : this.ip+1 }
  jl(x) { return (this.flags.cmp == CMP.LT) ? this.symTbl.get(x) : this.ip+1 }
  call(x) {
    if (!this.isTailCall()) this.callStack.push(this.ip+1)
    return this.symTbl.get(x)
  }
  ret() { return this.callStack.pop() }
  msg(...args) {
    this.msg = args.reduce((s,v) => s + (v[0]=="'"?v.slice(1,-1):this.registers[v]), '')
    return this.ip+1
  }
  isTailCall() { return this.ip < this.intructions.length-1 && /^[a-z0-9_]+:/.test(this.intructions[this.ip+1][0]) }
}

function split(s) {
  const ps = []
  for (let i = 0; i < s.length; i++) {
    if (s[i] == ' ' || s[i] == ',') continue
    ps.push(s.slice(i).match(/^'.*?'|[a-z0-9_]+/)[0])
    i += ps[ps.length-1].length-1
  }
  return ps
}

function parse(txt) {
  return txt
    .split('\n')
    .map(line => line.replace(/;.*/, '').trim())
    .filter(Boolean)
    .map(line => {
      const [,command, params] = line.match(/^([a-z0-9_:]+)(.*)/)
      return [command, ...split(params).map(v => v.trim()).filter(Boolean)]
    })
}

function assemblerInterpreter(program) {
  const commands = parse(program)
  return new Interpreter(commands).run()
}

