// https://www.codewars.com/kata/52dc4688eca89d0f820004c6/

/**

TODO:
 * Refactor.
 * Find another way to build table for labels before execution.

*/

const DEBUG = false
const log = (...args) => DEBUG && console.log(...args)

class Interpreter {
  constructor(code, input) {
    this.stack = []
    this.heap = new Map
    this.callStack = []
    this.ip = 0
    this.code = this.removeComments(code)
    this.output = []
    this.input = input
    this.sym = new Map
    this.constructSymTbl()
    this.dryRun = false
  }
  
  removeComments(code) {
    return code.replace(/[^\n\t ]/g, '')
  }
  
  constructSymTbl() {
    this.dryRun = true
    while (this.ip < this.code.length) {
      const cmd = this.matchCmdByIMP()
      this[cmd]()
    }
    this.ip = 0
  }
  
  run() {
    while (!this.programEnd) {
      if (this.ip >= this.code.length) throw new Error('Unexpected EOF')
      const cmd = this.matchCmdByIMP()
      this[cmd]()
    }
    
    return this.output.join``
  }
  
  matchCmdByIMP() {
    const codeSeq = this.code.substring(this.ip, this.ip+2)
    return [
        [/^ /, 'stackCmd'], [/^\t\n/, 'ioCmd'], [/^\t\t/, 'heapCmd'], [/^\t /, 'arithCmd'], [/^\n/, 'flowCmd']
      ].find(([re, cmd]) => re.test(codeSeq))[1]
  }
  
  readNumber() {
    const sign = this.code[this.ip++] == '\t' ? -1 : 1
    let bin = ''
    while (this.code[this.ip] != '\n') bin += this.code[this.ip++] == ' ' ? 0 : 1
    this.ip++
    
    log('num> ', sign, bin)
    
    return !bin ? 0 : sign*parseInt(bin, 2)
  }
  
  readLabel() {
    let l = '_'
    while (this.code[this.ip] != '\n') l += this.code[this.ip++]
    l = l.replace(/\s/g, 's').replace(/\t/g, 't')
    this.ip++
    return l
  }
  
  stackCmd() {
    this.ip += 1
    const codeSeq = this.code.substring(this.ip, this.ip+2)
    const op = [
      [/^ /, 'push'], [/^\t /, 'dup'], [/^\t\n/, 'discard'], [/^\n /, 'dup_top'], [/^\n\t/, 'swap'], [/^\n\n/, 'discard_top'] 
    ].find(([re, cmd]) => re.test(codeSeq))[1]
    this.ip += op == 'push' ? 1 : 2
    const stack = this.stack
    
    log('stack> ', op)
    
    switch (op) {
      case 'push': {
        const n = this.readNumber()
        if (this.dryRun) return
        stack.push(n)
        break
      }
      case 'dup': {
        const n = this.readNumber()
        if (this.dryRun) return
        if (!stack.length) throw new Error('Empty stack')
        const idx = stack.length-n-1
        if (idx < 0 || idx >= stack.length) throw new Error('Stack index out of bounds')
        stack.push(stack[idx])
        break
      }
      case 'discard': {
        let n = this.readNumber()
        if (this.dryRun) return
        if (!stack.length) throw new Error('Empty stack')
        const top = stack.pop()
        if (n < 0 || n >= stack.length) n = stack.length
        while (n--) stack.pop()
        stack.push(top)
        break
      }
      case 'dup_top': {
        if (this.dryRun) return
        if (!stack.length) throw new Error('Empty stack')
        stack.push(stack[stack.length-1])
        break
      }
      case 'swap': {
        if (this.dryRun) return
        if (!stack.length) throw new Error('Empty stack')
        const [a, b] = [stack.pop(), stack.pop()]
        stack.push(a, b)
        break
      }
      case 'discard_top': {
        if (this.dryRun) return
        if (!stack.length) throw new Error('Empty stack')
        stack.pop()
        break
      }
    }
  }
  
  arithCmd() {
    this.ip += 2
    const codeSeq = this.code.substring(this.ip, this.ip+2)
    const op = [
      [/^  /, 'sum'], [/^ \t/, 'sub'], [/^ \n/, 'mul'], [/^\t /, 'div'], [/^\t\t/, 'mod'] 
    ].find(([re, cmd]) => re.test(codeSeq))[1]
    this.ip += 2
    if (this.dryRun) return
    const stack = this.stack
    
    log('arith> ', op)
    
    if (stack.length < 2) throw new Error('Not enough values on stack')
    
    switch (op) {
      case 'sum': stack.push(stack.pop()+stack.pop()); break
      case 'sub': stack.push(-stack.pop()+stack.pop()); break
      case 'mul': stack.push(stack.pop()*stack.pop()); break
      case 'div': {
        const [a, b] = [stack.pop(), stack.pop()]
        if (a == 0) throw new Error('Div by zero')
        stack.push(Math.floor(b/a))
        break
      }
      case 'mod': {
        const [a, b] = [stack.pop(), stack.pop()]
        if (a == 0) throw new Error('Mod by zero')
        stack.push(((b%a)+a)%a)
        break
      }
    }
  }
  
  heapCmd() {
    this.ip += 2
    const codeSeq = this.code.substring(this.ip, this.ip+1)
    const op = [[/^ /, 'store'], [/^\t/, 'ret']].find(([re, cmd]) => re.test(codeSeq))[1]
    this.ip += 1
    if (this.dryRun) return
    
    log('heap> ', op)
    
    const stack = this.stack
    if (op == 'store') {
      if (stack.length < 2) throw new Error('Too small stack length')
      const [a, b] = [stack.pop(), stack.pop()]
      this.heap.set(b, a)
    }
    if (op == 'ret') {
      const address = stack.pop()
      if (!this.heap.has(address)) throw new Error('Heap address not exists')
      stack.push(this.heap.get(address))
    }
  }
  
  ioCmd() {
    this.ip += 2
    const codeSeq = this.code.substring(this.ip, this.ip+2)
    const op = [
      [/^  /, 'out_char'], [/^ \t/, 'out_num'], [/^\t /, 'in_char'], [/^\t\t/, 'in_num']
    ].find(([re, cmd]) => re.test(codeSeq))[1]
    this.ip += 2
    if (this.dryRun) return
    const stack = this.stack
    
    log('io> ', op)
    
    switch (op) {
      case 'out_char': {
        if (!stack.length) throw new Error('Empty stack')
        this.output.push(String.fromCharCode(stack.pop()))
        break
      }
      case 'out_num': {
        if (!stack.length) throw new Error('Empty stack')
        this.output.push(stack.pop())
        break
      }
      case 'in_char': {
        if (!this.input.length) throw new Error('Unexpected end of input')
        if (!this.stack.length) throw new Error('Empty stack')
        
        const [a, b] = [this.input[0][0], stack.pop()]
        this.heap.set(b, a.charCodeAt())
        this.input[0] = this.input[0].slice(1)
        break
      }
      case 'in_num': {
        if (!this.input.length) throw new Error('Unexpected end of input')
        if (!this.stack.length) throw new Error('Empty stack')
        
        const a = this.input.shift()
        const b = stack.pop()
        this.heap.set(b, +a)
        break
      }
    }
  }
  
  flowCmd() {
    this.ip += 1
    const codeSeq = this.code.substring(this.ip, this.ip+2)
    const op = [
      [/^  /, 'mark'], [/^ \t/, 'call'], [/^ \n/, 'jmp'], [/^\t /, 'jmpz'], [/^\t\t/, 'jmplz'], [/^\t\n/, 'ret'], [/^\n\n/, 'end']
    ].find(([re, cmd]) => re.test(codeSeq))[1]
    this.ip += 2
    
    log('flow> ', op)
    
    switch (op) {
      case 'mark': {
        const l = this.readLabel()
        if (this.sym.has(l) && this.dryRun) throw new Error('Duplicate label')
        this.sym.set(l, this.ip)
        break
      }
      case 'jmp': {
        const l = this.readLabel()
        if (this.dryRun) return
        if (!this.sym.has(l)) throw new Error('Unknown label')
        this.ip = this.sym.get(l)
        break
      }
      case 'jmpz': {
        const l = this.readLabel()
        if (this.dryRun) return
        if (!this.stack.length) throw new Error('Empty stack')
        const v = this.stack.pop()
        if (v == 0) this.ip = this.sym.get(l)
        break
      }
      case 'jmplz': {
        const l = this.readLabel()
        if (this.dryRun) return
        if (!this.stack.length) throw new Error('Empty stack')
        const v = this.stack.pop()
        if (v < 0) this.ip = this.sym.get(l)
        break
      }
      case 'call': {
        const l = this.readLabel()
        if (this.dryRun) return
        if (!this.sym.has(l)) throw new Error('Unknown label')
        this.callStack.push(this.ip)
        this.ip = this.sym.get(l)
        break
      }
      case 'ret': {
        if (this.dryRun) return
        this.ip = this.callStack.pop()
        break
      }
      case 'end': {
        if (this.dryRun) return
        this.programEnd = true
        break
      }
    }
  }
}

const whitespace = (code, input = '') => new Interpreter(code, input.split`\n`.filter(Boolean)).run()

