// https://www.codewars.com/kata/5a331ea7ee1aae8f24000175/

function triangle(row) {
  row = row.split``

  let len = row.length
  
  while (len > 1) {
    let n = 0
    while (3**(n+1) + 1 <= len) n++
    n = 3**n + 1
    
    let i = 0;
    while (i+n-1 < len) {
      const [l, r] = [row[i], row[i+n-1]]

      if (l == r) row[i] = l
      else if (l == 'R') row[i] = r == 'G' ? 'B' : 'G'
      else if (l == 'G') row[i] = r == 'R' ? 'B' : 'R'
      else if (l == 'B') row[i] = r == 'G' ? 'R' : 'G'
      i++
    }
    
    len = len - n + 1
  }
  
  return row[0]
}

