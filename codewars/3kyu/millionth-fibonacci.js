// https://www.codewars.com/kata/53d40c1e2f13e331fc000c26/

// Matrix binary exponentiation (https://cp-algorithms.com/algebra/binary-exp.html)

const mult = (a, b) => [
  [a[0][0]*b[0][0]+a[0][1]*b[1][0], a[0][0]*b[0][1]+a[0][1]*b[1][1]],
  [a[1][0]*b[0][0]+a[1][1]*b[1][0], a[1][0]*b[0][1]+a[1][1]*b[1][1]]
];

const matrix_pow = (matrix, n) => {
  if (n === 1) return matrix;
  
  const isOdd = n%2 === 1;
  
  let m = matrix_pow(matrix, isOdd ? (n-1)/2 : n/2);
 
  m = mult(m, m);
 
  if (isOdd) {
    m = mult(m, matrix);
  }
  
  return m;
}

function fib(n) {
  if (!n) return 0n

  return matrix_pow(
          n < 0
            ? [[ BigInt(-1), BigInt(1) ], [ BigInt(1), BigInt(0) ]]
            : [[ BigInt(1), BigInt(1) ], [ BigInt(1), BigInt(0) ]],
          Math.abs(n)
        )[0][1]
}

