// https://www.codewars.com/kata/5376b901424ed4f8c20002b7/

function closestPair(points) {
  points.sort((a, b) => a[0] - b[0])
  
  const dist = (a, b) => Math.sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2)
  
  const bruteForce = (l, r) => {
    let minDist = Infinity
    let minPoints = null

    for (let i = l; i <= r; ++i)
      for (let j = i + 1; j <= r; ++j)
        if (dist(points[i], points[j]) < minDist) {
          minDist = dist(points[i], points[j])
          minPoints = [points[i], points[j]]
        }

    return [minDist, minPoints]
  }
  
  const find = (l, r) => {
    if (r - l <= 3) return bruteForce(l, r)
    
    const mid = l + ((r - l) >> 1)
    
    const [dl, dlP] = find(l, mid-1)
    const [dr, drP] = find(mid, r)
    
    let [minDist, minPoints] = dl < dr ? [dl, dlP] : [dr, drP]

    const closest = []
    for (let i = l; i <= r; ++i) if (Math.abs(points[i][0] - points[mid][0]) < minDist)
      closest.push(points[i])
    
    closest.sort((a,b) => a[1] - b[1])
    
    for (let i = 0; i < closest.length; ++i)
        for (let j = i + 1; j < closest.length && (closest[j][1] - closest[i][1]) < minDist; ++j) if (dist(closest[i], closest[j]) < minDist) {
          minDist = dist(closest[i], closest[j])
          minPoints = [closest[i], closest[j]]
        }
    
    return [minDist, minPoints]
  }
  
  return find(0, points.length-1)[1]
}

