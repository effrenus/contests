// https://www.codewars.com/kata/52d4678038644497e900007c/

class Event {
  constructor() {
    const self = this
    Object.defineProperty(this, 'subs', { value: [], enumerable: false })
    Object.defineProperty(this, 'emit', { value: function(...args) { self._emit(this, args) } })
  }
  
  _emit(self, args) {
    this.subs.slice().forEach(fn => fn.apply(self, args))
  }
  
  subscribe(...fns) {
    this.subs.push(...fns.filter(fn => typeof fn == 'function'))
  }
  
  unsubscribe(...fns) {
    for (const fn of fns) {
      const idx = this.subs.lastIndexOf(fn)
      if (idx == -1) continue
      this.subs.splice(idx, 1)
    }
  }
}

