function maxMoney(prices, n, m) {
   prices.sort((a, b) => a - b);
   
   let sum = 0;
   let l = r = 0;
   
   while (n--) {
     sum += prices[r++];
   }
   
   if (sum > m) return 0;
   
   let max = sum;
   
   while (r < prices.length) {
     sum -= prices[l++];
     sum += prices[r++];
     
     if (sum > m) break;
     max = sum;
   }
   
   return max;
 }
