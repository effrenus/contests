function timeToEndFight(heroes, n) {
  let fs = heroes.splice(0, n);
  let time = 0;
  
  while (fs.length) {
    fs.sort((a, b) => a-b);
    const min = fs.shift();
    time += min;
    fs = fs.map(v => v - min);
    let j = 1;
    while (fs[0] === 0) j++;
    
    while (j && heroes.length) {
      fs.push(heroes.shift());
      j--;
    }
  }
  
  return time;
}
