function getNumberOfBoats(bs, limit) {
  bs.sort((a,b) => a-b);
  
  let count = 0;

  while (bs.length) {
    let i = 0;
    let w = 0;
    
    while (i < 2 && bs.length && w + bs[0] <= limit) {
      w += bs.shift();
      i += 1;
    }
    
    count += 1;
  }
  
  return count;
}
