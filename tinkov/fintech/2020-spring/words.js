const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin
});

function countWords(s) {
  const SPACE = /(\s|\t)+/
  const wordRe = /^[a-z][a-z]*((-[a-z]+)*|[a-z]*)[:?!;]?$/

  return s
    .split(SPACE)
    .map(w => w.trim())
    .filter(w => wordRe.test(w))
    .length
}

rl.on('line', function(line){
    console.log(countWords(line));
})
