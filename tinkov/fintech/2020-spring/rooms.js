const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin
});

function solution(max, rooms) {
  let i = 0;
  let toCleanRooms = 0;
  let needCommands = 0;

  while (rooms.length) {
    const needClean = rooms.shift();

    if (needClean) toCleanRooms++;

    i += 1;

    if (i !== max) continue

    if (toCleanRooms) needCommands++;

    toCleanRooms = 0;
    i = 0;
  }

  if (toCleanRooms) needCommands++;

  return needCommands;
}

const lines = []

rl.on('line', function(line){
    lines.push(parseInt(line))
})

rl.on('close', () => {
    const max = lines.shift()
    const rooms = lines.slice(1)

    console.log(solution(max, rooms))
})
