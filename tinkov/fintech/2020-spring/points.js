function doIntersect([a, b], ps) {
  const ccw = (a, b, c) => (c[1]-a[1]) * (b[0]-a[0]) > (b[1]-a[1]) * (c[0]-a[0])

  const check = (a, b, c, d) => ccw(a,c,d) != ccw(b,c,d) && ccw(a,b,c) != ccw(a,b,d)

  for (let i = 0; i < ps.length; i++) {
    if (check(a, b, ps[i], ps[(i+1)%ps.length])) return true;
  }

  return false;
}

function inPolygon (point, vs) {
    var x = point[0], y = point[1];

    var inside = false;
    for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
        var xi = vs[i][0], yi = vs[i][1];
        var xj = vs[j][0], yj = vs[j][1];

        var intersect = ((yi > y) != (yj > y))
            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
        if (intersect) inside = !inside;
    }

    return inside;
}

function cross(a, b, o) {
   return (a[0] - o[0]) * (b[1] - o[1]) - (a[1] - o[1]) * (b[0] - o[0])
}

function convexHull(points) {
   points.sort(function(a, b) {
      return a[0] == b[0] ? a[1] - b[1] : a[0] - b[0];
   });

   var lower = [];
   for (var i = 0; i < points.length; i++) {
      while (lower.length >= 2 && cross(lower[lower.length - 2], lower[lower.length - 1], points[i]) <= 0) {
         lower.pop();
      }
      lower.push(points[i]);
   }

   var upper = [];
   for (var i = points.length - 1; i >= 0; i--) {
      while (upper.length >= 2 && cross(upper[upper.length - 2], upper[upper.length - 1], points[i]) <= 0) {
         upper.pop();
      }
      upper.push(points[i]);
   }

   upper.pop();
   lower.pop();
   return lower.concat(upper);
}

function sol(points) {
  const p = convexHull(points)

  let minX = minY = Infinity
  let maxX = maxY = -Infinity

  for (const [x, y] of p) {
    minX = Math.min(minX, x)
    minY = Math.min(minY, y)
    maxX = Math.max(maxX, x)
    maxY = Math.max(maxY, y)
  }

  maxX += 1
  maxY += 1
  minX -= 1
  minY -= 1

  const dirs = [[0,1],[1,1],[-1,0],[-1,1],[0,-1],[-1,-1],[1,0],[1,-1]]
  const dists = [1,1.4142135623730951,1,1.4142135623730951,1,1.4142135623730951,1,1.4142135623730951]

  const usedPoints = new Map
  p.forEach(([x,y]) => usedPoints.set(`${x}-${y}`, true))

  const sortedP = p.slice().sort((a,b) => a[0] - b[0])
  const startPoint = sortedP[0]
  startPoint[0] -= 1

  const queue = [[startPoint, 0]]

  let first = true
  const ppp = []
  while(queue.length) {
    const [[x,y], d, fr] = queue.shift()
    ppp.push([x,y])

    usedPoints.set(`${x}-${y}`, true)

    for (const [i, [dx,dy]] of dirs.entries()) {
      if (first && i >= 2) continue

      const nx = x+dx
      const ny = y+dy

      if (nx === startPoint[0] && ny === startPoint[1] && d > 3) {
        console.log(d + dists[i])
        return
      }

      if (
        inPolygon([nx,ny], p) ||
        nx > maxX || nx < minX ||
        ny > maxY || ny < minY ||
        usedPoints.has(`${nx}-${ny}`) ||
        doIntersect([[x,y], [nx,ny]], p) ||
        usedPoints.has(`${nx}-${ny}`)
      ) continue

      usedPoints.set(`${nx}-${ny}`, true)
      queue.push([[nx,ny], d+dists[i], [x,y]])
    }

    if (first) first = false
  }
}

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin
});

const lines = []

rl.on('line', function(line){
    lines.push(line)
})

rl.on('close', () => {
    const points = lines.slice(1).map(p => p.split(' ').map(parseFloat))
    console.log(sol(points))
})
