const inherit_props = ["weather", "aggressiveness", "oreType", "color"];

function main(data) {
  const global_info = new Set();

  function walk(planet, parent_props = {}) {
    const planet_props = Object.keys(planet.info);
    const new_props = planet_props.filter(
      (k) => parent_props[k] !== planet.info[k]
    );

    const parent_props_ = { ...parent_props };

    for (const p of new_props) {
      global_info.add(`info_${p}_${planet.info[p]}`);

      if (inherit_props.includes(p)) {
        parent_props_[p] = planet.info[p];
      }
    }

    const visited_childs = planet.children.map((pl) => walk(pl, parent_props_));
    const stats = {};
    for (const { tree } of visited_childs) {
      for (const prop of tree.info) {
        const [_, name, val] = prop.split`_`;
        if (
          parent_props[name] ||
          planet_props.includes(name) ||
          !inherit_props.includes(name)
        )
          continue;
        if (!stats[name]) stats[name] = {};
        stats[name][val] = (stats[name][val] || 0) + 1;
      }
    }

    for (const [name, vals] of Object.entries(stats)) {
      if (
        Object.values(stats[name]).reduce((acc, v) => acc + v, 0) !==
        planet.children.length
      )
        continue;
      const [val] = Object.entries(vals).sort((a, b) => b[1] - a[1])[0];
      new_props.push(name);
      planet.info[name] = val;
    }
    visited_childs.forEach(({ update }) =>
      update(
        new_props
          .filter((p) => inherit_props.includes(p))
          .map((k) => `info_${k}_${planet.info[k]}`)
      )
    );

    const tree = {
      info: new_props.map((k) => `info_${k}_${planet.info[k]}`).sort(),
      children: visited_childs.map(({ tree }) => tree),
    };

    return {
      tree,
      update(props) {
        tree.info = tree.info.filter((p) => !props.includes(p));
      },
    };
  }

  const { tree } = walk(data);

  return {
    atomicInfo: [...global_info].sort(),
    tree,
  };
}

module.exports = main;
