class Solver {
  constructor() {
    this.sortX = [];
    this.sortY = [];
    this.minDist = Infinity;
  }

  dist() {
    if (this.sortX.length < 2) return null;

    for (let i = 0; i < this.sortX.length; i++) {
      let prev_dist = Infinity;
      for (let ii = i + 1; ii < this.sortX.length; ii++) {
        const dist = Math.sqrt(
          (this.sortX[i].x - this.sortX[ii].x) ** 2 +
            (this.sortX[i].y - this.sortX[ii].y) ** 2
        );
        if (dist > prev_dist) {
          break;
        }
        prev_dist = dist;
        this.minDist = Math.min(prev_dist, this.minDist);
      }
    }

    for (let i = 0; i < this.sortY.length; i++) {
      let prev_dist = Infinity;
      for (let ii = i + 1; ii < this.sortY.length; ii++) {
        const dist = Math.sqrt(
          (this.sortY[i].x - this.sortY[ii].x) ** 2 +
            (this.sortY[i].y - this.sortY[ii].y) ** 2
        );
        if (dist > prev_dist) {
          break;
        }
        prev_dist = dist;
        this.minDist = Math.min(prev_dist, this.minDist);
      }
    }

    return this.minDist;
  }

  insert(p) {
    this.sortX.push(p);
    this.sortX.sort((a, b) => a.x - b.x);
    this.sortY.push(p);
    this.sortY.sort((a, b) => a.y - b.y);

    return this.dist();
  }

  remove(p) {
    this.minDist = Infinity;
    const idX = this.sortX.findIndex((pp) => pp.x == p.x && pp.y == p.y);
    const idY = this.sortY.findIndex((pp) => pp.x == p.x && pp.y == p.y);

    this.sortX.splice(idX, 1);
    this.sortY.splice(idY, 1);

    return this.dist();
  }
}

module.exports = new Solver();

// const brute = require("./c-brute");

// const solver = new Solver();

// const points = [
//   // { x: 80, y: 54 },
//   // { x: 47, y: 89 },
//   // { x: 32, y: 62 },
//   // { x: 75, y: 99 },
//   // { x: 8, y: 18 },
//   // { x: 46, y: 14 },
//   // { x: 46, y: 27 },
//   // { x: 39, y: 99 },
// ];

// const k = (p) => `${p.x},${p.y}`;

// console.log(points.length);
// const ops = [
//   ...(points.length
//     ? points.map((p) => ["insert", p])
//     : Array(200)
//         .fill(0)
//         .map((_) => {
//           return [
//             "insert",
//             {
//               x: Math.round(Math.random() * 100),
//               y: Math.round(Math.random() * 100),
//             },
//           ];
//         })),
// ];

// const pp = {};
// for (const [op, p] of ops) {
//   if (pp[k(p)]) continue;

//   pp[k(p)] = p;
//   points.push(p);
//   const [a, b] = [solver[op](p), brute[op](p)];

//   if (a != b) {
//     console.log(a, b);
//     console.log(points);
//     break;
//   }
// }

// while (Object.keys(pp).length) {
//   const rk = Object.keys(pp).shift();
//   const p = pp[rk];
//   delete pp[rk];

//   const [a, b] = [solver.remove(p), brute.remove(p)];

//   if (a != b) {
//     console.log(a, b);
//     break;
//   }
// }
