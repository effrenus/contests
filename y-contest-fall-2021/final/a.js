function collect_routes(paths, node, parent, data, res, route = "") {
  if (!node) {
    return res;
  }

  const part = paths.shift();

  let title = node.title;
  const nextPart = node.redirectTo || part;
  const curRoute = route + nextPart + (nextPart != "/" ? "/" : "");
  route = route + part + (part != "/" ? "/" : "");

  if (node.redirectTo) {
    const rr = parent.children.find((v) => v.route === node.redirectTo);
    title = rr.title;
  }

  if (!node.title && !node.redirectTo) {
    title = data[node.route.slice(1).split(/[A-Z][a-z]+$/)[0]][part];
  }

  res.push({
    title,
    route: curRoute == "/" ? curRoute : curRoute.slice(0, curRoute.length - 1),
  });

  if (!paths.length) {
    return res;
  }

  let nextRoute = node.children.find((r) => r.route === paths[0]);
  if (!nextRoute) {
    nextRoute = node.children.find((r) => r.route.startsWith(":"));
  }

  return collect_routes(paths, nextRoute, node, data, res, route);
}

function main(routeTree, data, urls) {
  const out = [];

  for (const url of urls) {
    const paths = url.split("/").filter((s) => s.trim());
    paths.unshift("/");
    out.push(collect_routes(paths, routeTree, routeTree, data, []));
  }

  console.log(out.filter((a) => a.length > 1));

  return out.filter((a) => a.length > 1);
}

module.exports = main;
