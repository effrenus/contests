/*
interface Worker {  
  new (path: string); // для создания инстанса нужно передать путь до исполняемого кода,  
  // в нашем случае это всегда "./worker".  
  // Не забудьте добавить обработчик исключения, так как если достигнут лимит дронов,  
  // нужно дождаться, пока можно будет создавать новые.  
  onmessage?: (message: {data: string}) => void; // сюда нужно записать свою функцию-обработчик,  
  // чтобы принимать сообщения от дрона  
  postMessage: (text: string) => void; // эту функцию нужно вызвать для передачи строки боту,  
  // который её захеширует  
  terminate: () => void; // после одного использования, нужно удалять инстансы,  
  // таким образом освобождать дроны для следующих работ  
}
*/

// const Worker = require("./WorkerClass");

const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const getResp = async (t) => {
  if (t.startsWith("one")) return "2930";
  if (t.startsWith("sec")) {
    await sleep(5000);
    return "f653";
  }
  if (t.startsWith("third")) return "474f";
  if (t.startsWith("fourth")) return "3dda";
  if (t.startsWith("fifth")) return "ab09";
  if (t.startsWith("2930")) return "a03d";
  if (t.startsWith("474f")) return "66b0";
  if (t.startsWith("ab09")) return "8f9e";
  if (t.startsWith("a03d")) return "f8f4";
  if (t.startsWith("8f9e")) return "f72a";
  if (t.startsWith("f8f4")) return "ff35";
  return t.repeat(1);
};

const Worker = (() => {
  const MAX = 2;
  let curCnt = 0;

  return class {
    constructor(_path) {
      if (curCnt === MAX) {
        throw new Error("MAX_CAPACITY");
      }
      curCnt += 1;
    }

    terminate() {
      curCnt -= 1;
    }

    onmessage() {}

    postMessage(text) {
      setTimeout(
        async () => this.onmessage({ data: await getResp(text) }),
        Math.random() * 100
      );
    }
  };
})();

const main = (input) => {
  if (!input.length) {
    return Promise.resolve("0");
  }

  let finish;
  const p = new Promise((resolve) => (finish = resolve));

  const elmPerLvl = [];
  let c = input.length;
  while (true) {
    elmPerLvl.push(c);
    if (c === 1) break;
    c = (c % 2 ? c + 1 : c) / 2;
  }

  const hashes = new Map();
  const workQueue = input.map((s, i) => [-1, i, s]);

  const run = () => {
    if (hashes.has(`${elmPerLvl.length - 1},0`)) {
      return finish(hashes.get(`${elmPerLvl.length - 1},0`));
    }
    while (workQueue.length) {
      try {
        const worker = new Worker("./worker");
        doWork(worker);
      } catch (_err) {
        return;
      }
    }
  };

  const doWork = (worker) => {
    const [lvl, pos, text] = workQueue.shift();
    worker.onmessage = ({ data }) => {
      const nextPos = lvl === -1 ? pos : pos / 2;
      const k = `${lvl + 1},${nextPos}`;
      hashes.set(k, data);
      checkAndAppendNextWork(lvl + 1, nextPos);
      worker.terminate();
      run();
    };
    worker.postMessage(text);
  };

  const checkAndAppendNextWork = (lvl, pos) => {
    if (pos % 2 === 0 && pos === elmPerLvl[lvl] - 1) {
      const k = `${lvl},${pos}`;
      workQueue.push([lvl, pos, hashes.get(k).repeat(2)]);
      return;
    }
    pos = pos % 2 ? pos - 1 : pos;
    const k = `${lvl},${pos}`;
    const kk = `${lvl},${pos + 1}`;

    if (hashes.has(k) && hashes.has(kk)) {
      workQueue.push([lvl, pos, hashes.get(k) + hashes.get(kk)]);
    }
  };

  setTimeout(run, 10);

  return p;
};

module.exports = main;

// const tests = [
//   [["1", "2", "3", "4", "5", "6"], "12345656"],
//   [
//     ["1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c"],
//     "123456789abc9abc",
//   ],
//   [["1", "2"], "12"],
//   [["1", "2", "3", "4"], "1234"],
//   [["1", "2", "3", "4", "5", "6", "7"], "12345677"],
//   [["one", "sec", "third", "fourth", "fifth"], "ff35"],
//   [["one"], "2930"],
//   [["one", "sec"], "a03d"],
//   [["one", "sec", "third", "fourth"], "f8f4"],
// ];
// (async function test() {
//   while (tests.length) {
//     const [t, r] = tests.shift();
//     const res = await main(t);
//     console.log(res);

//     console.assert(res === r, `Must be equal ${res} == ${r}`);
//   }
// })();
