class Solver {
  constructor() {
    this.points = {};
  }

  key(p) {
    return `${p.x},${p.y}`;
  }

  dist() {
    const points = Object.values(this.points);

    if (points.length < 2) return null;

    let dist = Infinity;
    for (let i = 0; i < points.length; i++) {
      for (let ii = i + 1; ii < points.length; ii++) {
        dist = Math.min(
          dist,
          Math.sqrt(
            (points[i].x - points[ii].x) ** 2 +
              (points[i].y - points[ii].y) ** 2
          )
        );
      }
    }

    return dist;
  }

  insert(p) {
    this.points[this.key(p)] = p;
    return this.dist();
  }

  remove(p) {
    delete this.points[this.key(p)];
    return this.dist();
  }
}

module.exports = new Solver();
