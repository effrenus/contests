/*
type Video = {
    width: number;
    height: number; 
    x: number; // Положение левого верхнего угла видео по x относительно верхнего левого угла экрана
    y: number; // Положение левого верхнего угла видео по y относительно верхнего левого угла экрана
}
*/

/**
 * @param n - количество участников
 * @param width - ширина экрана каждого участника
 * @param height - высота экрана каждого участника
 */
function main(n, width, height) /*: Video[]*/ {
  const cols_count = Math.ceil(Math.sqrt(n));

  let video_width = Math.round(width / cols_count);
  let video_height = Math.round(video_width * (height / width));

  let rows_count = Math.ceil(n / cols_count);
  const widow_count = rows_count * cols_count - n;
  const res = [];

  let start_y = (height - rows_count * video_height) / 2;

  if (widow_count > 0) {
    const start_x = (width - video_width * (cols_count - widow_count)) / 2;
    for (let c = 0; c < cols_count - widow_count; c++) {
      res.push({
        width: video_width,
        height: video_height,
        x: start_x + c * video_width,
        y: start_y,
      });
    }
  }

  const start_r = widow_count > 0 ? 1 : 0;

  const start_x = (width - video_width * cols_count) / 2;
  for (let r = start_r; r < rows_count; r++) {
    for (let c = 0; c < cols_count; c++) {
      res.push({
        width: video_width,
        height: video_height,
        x: start_x + c * video_width,
        y: start_y + r * video_height,
      });
    }
  }

  return res;
}

module.exports = main;
