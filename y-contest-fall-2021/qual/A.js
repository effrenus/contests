const sleep = (ms = 10) => new Promise((resolve) => setTimeout(resolve, ms));

async function find_exit(game, start) {
  const moveToAction = {
    top: "up",
    bottom: "down",
    left: "left",
    right: "right",
  };

  const queue = [start];
  const visited = {};

  await game
    .right(start.x, start.y)
    .catch((_) => game.left(start.x, start.y))
    .catch((_) => game.up(start.x, start.y))
    .catch((_) => game.down(start.x, start.y));

  let ans;

  while (1) {
    if (ans) return ans;

    if (!queue.length) {
      await sleep(0);
      continue;
    }

    const p = queue.shift();

    game.state(p.x, p.y).then((gstate) => {
      if (gstate.finish) {
        ans = p;
        return;
      }

      for (const pos of ["top", "bottom", "left", "right"]) {
        if (!gstate[pos]) continue;

        const pp = {
          x: p.x + (pos === "right" ? 1 : pos === "left" ? -1 : 0),
          y: p.y + (pos === "top" ? -1 : pos === "bottom" ? 1 : 0),
        };
        const k = `${pp.x};${pp.y}`;

        if (visited[k]) continue;
        visited[k] = true;

        game[moveToAction[pos]](p.x, p.y).then(() => queue.push(pp));
      }
    });
  }
}

module.exports = find_exit;
