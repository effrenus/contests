function color_to_rgba({ r, g, b, a }) {
  r = (255 * r) >> 0;
  g = (255 * g) >> 0;
  b = (255 * b) >> 0;

  return `rgba(${r}, ${g}, ${b}, ${a})`;
}

function produce_strokes_styles(node) {
  if (!node.strokes || !node.strokes.length) return "";

  let styles = "";
  for (const st of node.strokes) {
    styles += `border: ${node.strokeWeight}px solid ${color_to_rgba(
      st.color
    )};`;
  }

  return styles;
}

function produce_effects_styles(effects) {
  if (!effects || !effects.length) return "";

  let styles = "";
  for (const eff of effects) {
    if (!eff.visible) continue;
    if (eff.type === "DROP_SHADOW")
      styles += `box-shadow:${eff.offset.x}px ${eff.offset.y}px ${
        eff.radius
      } ${color_to_rgba(eff.color)}`;
  }

  return styles;
}

function produce_styles(node, frame) {
  let styles = "";
  const originX = frame
    ? frame.absoluteBoundingBox.x
    : node.absoluteBoundingBox.x;
  const originY = frame
    ? frame.absoluteBoundingBox.y
    : node.absoluteBoundingBox.y;

  const { absoluteBoundingBox, fills, background } = node;

  if (absoluteBoundingBox) {
    styles += `position: absolute; top: ${
      absoluteBoundingBox.y - originY
    }px; left: ${absoluteBoundingBox.x - originX}px; width: ${
      absoluteBoundingBox.width
    }px; height: ${absoluteBoundingBox.height}px;`;
  }

  if (fills && fills.length && fills[0].color && fills[0].visible !== false) {
    styles +=
      node.type !== "RECTANGLE"
        ? `color: ${color_to_rgba(fills[0].color)};`
        : `background: ${color_to_rgba(fills[0].color)};`;
  }

  if (
    background &&
    background.length &&
    background[0].color &&
    background[0].visible !== false
  ) {
    styles += `background: ${color_to_rgba(background[0].color)};`;
  }

  const stMap = {
    fontFamily: (val) => `font-family: ${val};`,
    fontWeight: (val) => `font-weight: ${val};`,
    fontSize: (val) => `font-size: ${val}px;`,
    lineHeightPx: (val) => `line-height: ${val}px;`,
    letterSpacing: (val) => `letter-spacing: ${val}px;`,
    textAlignHorizontal: (val) => `text-align: ${val.toLowerCase()};`,
    paddingLeft: (val) => `padding-left: ${val}px;`,
    paddingRight: (val) => `padding-right: ${val}px;`,
    paddingTop: (val) => `padding-left: ${val}px;`,
    paddingBottom: (val) => `padding-left: ${val}px;`,
  };

  for (const name of Object.keys(stMap)) {
    if (node.style && typeof node.style[name] !== "undefined") {
      styles += stMap[name](node.style[name]);
    } else if (typeof node[name] !== "undefined") {
      styles += stMap[name](node[name]);
    }
  }

  if (node.clipsContent) {
    styles += "overflow: hidden;";
  }

  styles += produce_effects_styles(node.effects);

  styles += produce_strokes_styles(node);

  return styles;
}

function prepare_html(node, frame) {
  const compToHtml = {
    TEXT: (node) => {
      const styles = produce_styles(node, frame);
      return `<span style="${styles}">${node.characters}</span>`;
    },
    FRAME: (node) => {
      const styles = produce_styles(node, frame);
      return `<div data-name="${node.name || "unknown"}" data-type="${
        node.type || "unknown"
      }" style="${styles}">${(node.children || []).reduce(
        (acc, innode, i) => acc + prepare_html(innode, node, i),
        ""
      )}</div>`;
    },
    COMPONENT: () => "",
    INSTANCE: (node) => compToHtml["FRAME"](node),
    RECTANGLE: (node) => compToHtml["FRAME"](node),
    COMPONENT_SET: () => "",
    CANVAS: (node) => node.children.map((innode) => prepare_html(innode)),
    DOCUMENT: (node) => node.children.map((innode) => prepare_html(innode)),
  };

  return compToHtml[node.type] ? compToHtml[node.type](node) : "";
}

module.exports = function (json) {
  return `
  <style>html,body {margin:0;padding:0}</style>
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  ${prepare_html(json.document).flat()[0]}
  `;
};
