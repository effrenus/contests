// https://codingcompetitions.withgoogle.com/kickstart/round/000000000019ff43/00000000003379bb

const readline = require('readline')
const rl = readline.createInterface({
  input: process.stdin
})

function solution(n, arr) {
  const cnt = new Map
  arr.forEach(r => r.forEach(c => cnt.set(c, (cnt.get(c)||0)+1)))

  const rows = arr.length
  const cols = arr[0].length

  const possible = (row, ch) => {
    let cn = cnt.get(ch)

    const q = []
    for (let c = 0; c < cols; c++) {
      if ((row == rows-1 || arr[row+1][c] == null) && (arr[row][c] == ch || arr[row][c]== null)) q.push([row, c])
    }

    // console.log(row, ch, cn)

    while (q.length) {
      const [r, c] = q.shift()

      if (arr[r][c] == ch) cn--

      if (r-1 >= 0 && (arr[r-1][c] == null || arr[r-1][c] == ch)) q.push([r-1, c])
    }

    // console.log(cn)

    return cn == 0
  }

  const fill = (ch) => arr.forEach(row => row.forEach((_, i) => {
    if(row[i] == ch) row[i] = null
  }))

  const ans = []
  let uniqKCnt = [...cnt.keys()].length
  let old

  while (old != uniqKCnt && uniqKCnt > 0) {
    old = uniqKCnt
    let curRow = rows-1

    for (let r = rows-1; r >= 0; --r) {
      const checked = {}

      for (let c = 0; c < cols; ++c) {
        if (arr[r].every(v => v==null)) {
          break
        }

        if (checked[arr[r][c]]) continue
        checked[arr[r][c]] = true

        if (possible(r, arr[r][c])) {
          ans.push(arr[r][c])
          uniqKCnt--
          fill(arr[r][c])
          // console.log(arr)
        }
      }
    }

    // console.log(old, uniqKCnt)
  }

  console.log(`Case #${n}: ${!uniqKCnt ? ans.join('') : -1}`)
}

// solution([
//   [ 'X', 'X', 'O', 'O' ],
//   [ 'X', 'F', 'F', 'O' ],
//   [ 'X', 'F', 'X', 'O' ],
//   [ 'X', 'X', 'X', 'O' ]
// ])

const lines = []

rl.on('line', line => lines.push(line))

rl.on('close', () => {
  let N = +lines.shift()
  let n = 1

  while (N--) {
    let [r, c] = lines.shift().split` `.map(Number)

    const arr = []
    while (r--) arr.push(lines.shift().split``)

    solution(n++, arr)
  }
})
