const { readFileSync } = require('fs')
const input = readFileSync(process.argv[2]).toString().trim().split`\n`

for (let i=1, n=1; i<input.length; i+=2,n++) {
  let s = input[i+1]

  while (s.length > 1) {
    const ns = s.replace(/AB|BA/, '')
    if (ns == s) break
    s = ns
  }

  console.log(`Case #${n}: ${s.length==1?'Y':'N'}`)
}
